﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElementalKingdoms.core;

namespace ElementalKingdomsTests
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void TestLevelToHp()
        {
            User u = new User();
            u.Level = 1;
            Assert.AreEqual(1000, u.HP);
            Assert.AreEqual(1, u.Level);

            u.Level = 2;
            Assert.AreEqual(1070, u.HP);
            Assert.AreEqual(2, u.Level);

            u.Level = 9;
            Assert.AreEqual(1560, u.HP);
            Assert.AreEqual(9, u.Level);

            u.Level = 10;
            Assert.AreEqual(1630, u.HP);
            Assert.AreEqual(10, u.Level);

            u.Level = 11;
            Assert.AreEqual(1800, u.HP);
            Assert.AreEqual(11, u.Level);

            u.Level = 21;
            Assert.AreEqual(2800, u.HP);
            Assert.AreEqual(21, u.Level);

            u.Level = 101;
            Assert.AreEqual(17000, u.HP);
            Assert.AreEqual(101, u.Level);

            u.Level = 102;
            Assert.AreEqual(17160, u.HP);
            Assert.AreEqual(102, u.Level);

            u.Level = 107;
            Assert.AreEqual(17960, u.HP);
            Assert.AreEqual(107, u.Level);

            u.Level = 120;
            Assert.AreEqual(20040, u.HP);
            Assert.AreEqual(120, u.Level);

            //u.Level = 150;
            //Assert.AreEqual(32290, u.HP);
            //Assert.AreEqual(150, u.Level);
        }


        [TestMethod]
        public void ValidateRoundFiftyPlusDamage()
        {
            // From round 51, each player loses an increasing amount of life.
            Card.Cards = new System.Collections.Generic.List<Card>();
            Card.Cards.Add(
                new Card()
                {
                    Id = 1,
                    HpArray = new int[] { 1000 },
                    AttackArray = new int[] { 0 },
                    Wait = 0
                }
            );

            User attacker = new User();
            attacker.Level = 101;
            attacker.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            User defender = new User();
            defender.Level = 101;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = 1, Level = 0 });

            const int MAX_HP = 17000; // for level 101

            Battle b = new Battle(attacker, defender);
            b.Round();

            // 1st card on battlefield
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(0, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(2, b.Rounds.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(1000, b.DefendPlayer.Battlefield[0].CurrentHP);
            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            // Snipe kills defender. Opponent should receive 200 damage.
            b.Round();
            Assert.AreEqual(1, b.AttackPlayer.Battlefield.Count);
            Assert.AreEqual(1, b.DefendPlayer.Battlefield.Count);
            Assert.AreEqual(1000, b.AttackPlayer.Battlefield[0].CurrentHP);

            Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            for (int i = 4; i < 51; i++)
            {
                b.Round();
                Assert.AreEqual(i, b.Rounds.Count);
                Assert.AreEqual(MAX_HP, b.AttackPlayer.CurrentHP);
                Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);
            }

            b.Round();
            Assert.AreEqual(MAX_HP - 50, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(MAX_HP - 50, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 80, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(MAX_HP - 50 - 110, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 80, b.DefendPlayer.CurrentHP);

            b.Round();
            Assert.AreEqual(MAX_HP - 50 - 110, b.AttackPlayer.CurrentHP);
            Assert.AreEqual(MAX_HP - 80 - 140, b.DefendPlayer.CurrentHP);
        }
    }
}
