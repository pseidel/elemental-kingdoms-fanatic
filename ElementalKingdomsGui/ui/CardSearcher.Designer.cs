﻿namespace ElementalKingdoms.ui
{
    partial class CardSearcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CardSearcher));
            this.lbCards = new System.Windows.Forms.ListBox();
            this.tbFilter = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.numLevel = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblAttack = new System.Windows.Forms.Label();
            this.lblHP = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSkill1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbStars1 = new System.Windows.Forms.CheckBox();
            this.cbStars2 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbStars3 = new System.Windows.Forms.CheckBox();
            this.cbStars4 = new System.Windows.Forms.CheckBox();
            this.cbStars5 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbClassForest = new System.Windows.Forms.CheckBox();
            this.cbClassTundra = new System.Windows.Forms.CheckBox();
            this.cbClassMountain = new System.Windows.Forms.CheckBox();
            this.cbClassSwamp = new System.Windows.Forms.CheckBox();
            this.cbClassOther = new System.Windows.Forms.CheckBox();
            this.imageLoader = new System.ComponentModel.BackgroundWorker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbSkillFilter = new System.Windows.Forms.ComboBox();
            this.cbEvolveSkill = new System.Windows.Forms.ComboBox();
            this.cbDisplayOnlyOwnedCards = new System.Windows.Forms.CheckBox();
            this.btnClearEvolveSkill = new System.Windows.Forms.Button();
            this.pbEvolveLeft = new System.Windows.Forms.PictureBox();
            this.pbEvolveRight1 = new System.Windows.Forms.PictureBox();
            this.ttEvo = new System.Windows.Forms.ToolTip(this.components);
            this.lblLockSkill1 = new System.Windows.Forms.Label();
            this.lblLockSkill2 = new System.Windows.Forms.Label();
            this.lblLockSkillDemon = new System.Windows.Forms.Label();
            this.cbAllEvoSkills = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.numTimesEvolved = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.lblCost = new System.Windows.Forms.Label();
            this.lblLockSkill1b = new System.Windows.Forms.Label();
            this.pbEvolveRight2 = new System.Windows.Forms.PictureBox();
            this.pbEvolveRight3 = new System.Windows.Forms.PictureBox();
            this.pbEvolveRight4 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lblWait = new System.Windows.Forms.Label();
            this.pbFavourite = new System.Windows.Forms.PictureBox();
            this.cbOnlyFavourites = new System.Windows.Forms.CheckBox();
            this.lblSkill2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimesEvolved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFavourite)).BeginInit();
            this.SuspendLayout();
            // 
            // lbCards
            // 
            this.lbCards.FormattingEnabled = true;
            this.lbCards.ItemHeight = 16;
            this.lbCards.Location = new System.Drawing.Point(12, 124);
            this.lbCards.Name = "lbCards";
            this.lbCards.Size = new System.Drawing.Size(274, 116);
            this.lbCards.TabIndex = 0;
            // 
            // tbFilter
            // 
            this.tbFilter.Location = new System.Drawing.Point(54, 12);
            this.tbFilter.Name = "tbFilter";
            this.tbFilter.Size = new System.Drawing.Size(232, 22);
            this.tbFilter.TabIndex = 2;
            this.tbFilter.TextChanged += new System.EventHandler(this.tbFilter_TextChanged);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(268, 408);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // numLevel
            // 
            this.numLevel.Location = new System.Drawing.Point(79, 246);
            this.numLevel.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numLevel.Name = "numLevel";
            this.numLevel.Size = new System.Drawing.Size(48, 22);
            this.numLevel.TabIndex = 4;
            this.numLevel.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numLevel.ValueChanged += new System.EventHandler(this.numLevel_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 248);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Level";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(217, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "HP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(132, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Attack";
            // 
            // lblAttack
            // 
            this.lblAttack.AutoSize = true;
            this.lblAttack.Location = new System.Drawing.Point(178, 248);
            this.lblAttack.Name = "lblAttack";
            this.lblAttack.Size = new System.Drawing.Size(16, 17);
            this.lblAttack.TabIndex = 8;
            this.lblAttack.Text = "0";
            // 
            // lblHP
            // 
            this.lblHP.AutoSize = true;
            this.lblHP.Location = new System.Drawing.Point(243, 248);
            this.lblHP.Name = "lblHP";
            this.lblHP.Size = new System.Drawing.Size(16, 17);
            this.lblHP.TabIndex = 9;
            this.lblHP.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 313);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Skills";
            // 
            // lblSkill1
            // 
            this.lblSkill1.AutoSize = true;
            this.lblSkill1.Location = new System.Drawing.Point(78, 313);
            this.lblSkill1.Name = "lblSkill1";
            this.lblSkill1.Size = new System.Drawing.Size(13, 17);
            this.lblSkill1.TabIndex = 11;
            this.lblSkill1.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Name";
            // 
            // cbStars1
            // 
            this.cbStars1.AutoSize = true;
            this.cbStars1.Checked = true;
            this.cbStars1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars1.Location = new System.Drawing.Point(54, 40);
            this.cbStars1.Name = "cbStars1";
            this.cbStars1.Size = new System.Drawing.Size(38, 21);
            this.cbStars1.TabIndex = 13;
            this.cbStars1.Text = "1";
            this.cbStars1.UseVisualStyleBackColor = true;
            this.cbStars1.CheckedChanged += new System.EventHandler(this.cbStars_CheckedChanged);
            // 
            // cbStars2
            // 
            this.cbStars2.AutoSize = true;
            this.cbStars2.Checked = true;
            this.cbStars2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars2.Location = new System.Drawing.Point(98, 40);
            this.cbStars2.Name = "cbStars2";
            this.cbStars2.Size = new System.Drawing.Size(38, 21);
            this.cbStars2.TabIndex = 14;
            this.cbStars2.Text = "2";
            this.cbStars2.UseVisualStyleBackColor = true;
            this.cbStars2.CheckedChanged += new System.EventHandler(this.cbStars_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Stars";
            // 
            // cbStars3
            // 
            this.cbStars3.AutoSize = true;
            this.cbStars3.Checked = true;
            this.cbStars3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars3.Location = new System.Drawing.Point(142, 40);
            this.cbStars3.Name = "cbStars3";
            this.cbStars3.Size = new System.Drawing.Size(38, 21);
            this.cbStars3.TabIndex = 16;
            this.cbStars3.Text = "3";
            this.cbStars3.UseVisualStyleBackColor = true;
            this.cbStars3.CheckedChanged += new System.EventHandler(this.cbStars_CheckedChanged);
            // 
            // cbStars4
            // 
            this.cbStars4.AutoSize = true;
            this.cbStars4.Checked = true;
            this.cbStars4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars4.Location = new System.Drawing.Point(186, 40);
            this.cbStars4.Name = "cbStars4";
            this.cbStars4.Size = new System.Drawing.Size(38, 21);
            this.cbStars4.TabIndex = 17;
            this.cbStars4.Text = "4";
            this.cbStars4.UseVisualStyleBackColor = true;
            this.cbStars4.CheckedChanged += new System.EventHandler(this.cbStars_CheckedChanged);
            // 
            // cbStars5
            // 
            this.cbStars5.AutoSize = true;
            this.cbStars5.Checked = true;
            this.cbStars5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStars5.Location = new System.Drawing.Point(230, 40);
            this.cbStars5.Name = "cbStars5";
            this.cbStars5.Size = new System.Drawing.Size(38, 21);
            this.cbStars5.TabIndex = 18;
            this.cbStars5.Text = "5";
            this.cbStars5.UseVisualStyleBackColor = true;
            this.cbStars5.CheckedChanged += new System.EventHandler(this.cbStars_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 17);
            this.label7.TabIndex = 19;
            this.label7.Text = "Class";
            // 
            // cbClassForest
            // 
            this.cbClassForest.AutoSize = true;
            this.cbClassForest.BackColor = System.Drawing.SystemColors.Control;
            this.cbClassForest.Checked = true;
            this.cbClassForest.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbClassForest.Location = new System.Drawing.Point(54, 67);
            this.cbClassForest.Name = "cbClassForest";
            this.cbClassForest.Size = new System.Drawing.Size(70, 21);
            this.cbClassForest.TabIndex = 20;
            this.cbClassForest.Text = "Forest";
            this.cbClassForest.UseVisualStyleBackColor = false;
            this.cbClassForest.CheckedChanged += new System.EventHandler(this.cbClass_CheckedChanged);
            // 
            // cbClassTundra
            // 
            this.cbClassTundra.AutoSize = true;
            this.cbClassTundra.Checked = true;
            this.cbClassTundra.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbClassTundra.Location = new System.Drawing.Point(130, 67);
            this.cbClassTundra.Name = "cbClassTundra";
            this.cbClassTundra.Size = new System.Drawing.Size(76, 21);
            this.cbClassTundra.TabIndex = 21;
            this.cbClassTundra.Text = "Tundra";
            this.cbClassTundra.UseVisualStyleBackColor = true;
            this.cbClassTundra.CheckedChanged += new System.EventHandler(this.cbClass_CheckedChanged);
            // 
            // cbClassMountain
            // 
            this.cbClassMountain.AutoSize = true;
            this.cbClassMountain.Checked = true;
            this.cbClassMountain.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbClassMountain.Location = new System.Drawing.Point(206, 67);
            this.cbClassMountain.Name = "cbClassMountain";
            this.cbClassMountain.Size = new System.Drawing.Size(88, 21);
            this.cbClassMountain.TabIndex = 22;
            this.cbClassMountain.Text = "Mountain";
            this.cbClassMountain.UseVisualStyleBackColor = true;
            this.cbClassMountain.CheckedChanged += new System.EventHandler(this.cbClass_CheckedChanged);
            // 
            // cbClassSwamp
            // 
            this.cbClassSwamp.AutoSize = true;
            this.cbClassSwamp.Checked = true;
            this.cbClassSwamp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbClassSwamp.Location = new System.Drawing.Point(292, 67);
            this.cbClassSwamp.Name = "cbClassSwamp";
            this.cbClassSwamp.Size = new System.Drawing.Size(75, 21);
            this.cbClassSwamp.TabIndex = 23;
            this.cbClassSwamp.Text = "Swamp";
            this.cbClassSwamp.UseVisualStyleBackColor = true;
            this.cbClassSwamp.CheckedChanged += new System.EventHandler(this.cbClass_CheckedChanged);
            // 
            // cbClassOther
            // 
            this.cbClassOther.AutoSize = true;
            this.cbClassOther.Location = new System.Drawing.Point(373, 67);
            this.cbClassOther.Name = "cbClassOther";
            this.cbClassOther.Size = new System.Drawing.Size(66, 21);
            this.cbClassOther.TabIndex = 24;
            this.cbClassOther.Text = "Other";
            this.cbClassOther.UseVisualStyleBackColor = true;
            this.cbClassOther.CheckedChanged += new System.EventHandler(this.cbClass_CheckedChanged);
            // 
            // imageLoader
            // 
            this.imageLoader.WorkerSupportsCancellation = true;
            this.imageLoader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.imageLoader_DoWork);
            this.imageLoader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.imageLoader_RunWorkerCompleted);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(390, 124);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(127, 254);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 17);
            this.label8.TabIndex = 26;
            this.label8.Text = "Skill";
            // 
            // cbSkillFilter
            // 
            this.cbSkillFilter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbSkillFilter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSkillFilter.FormattingEnabled = true;
            this.cbSkillFilter.Location = new System.Drawing.Point(54, 94);
            this.cbSkillFilter.Name = "cbSkillFilter";
            this.cbSkillFilter.Size = new System.Drawing.Size(232, 24);
            this.cbSkillFilter.Sorted = true;
            this.cbSkillFilter.TabIndex = 27;
            this.cbSkillFilter.DropDown += new System.EventHandler(this.comboBox_DropDown);
            // 
            // cbEvolveSkill
            // 
            this.cbEvolveSkill.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbEvolveSkill.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbEvolveSkill.FormattingEnabled = true;
            this.cbEvolveSkill.Location = new System.Drawing.Point(15, 379);
            this.cbEvolveSkill.Name = "cbEvolveSkill";
            this.cbEvolveSkill.Size = new System.Drawing.Size(271, 24);
            this.cbEvolveSkill.TabIndex = 28;
            this.cbEvolveSkill.DropDown += new System.EventHandler(this.comboBox_DropDown);
            this.cbEvolveSkill.SelectedIndexChanged += new System.EventHandler(this.cbEvolveSkill_SelectedIndexChanged);
            // 
            // cbDisplayOnlyOwnedCards
            // 
            this.cbDisplayOnlyOwnedCards.AutoSize = true;
            this.cbDisplayOnlyOwnedCards.Checked = true;
            this.cbDisplayOnlyOwnedCards.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDisplayOnlyOwnedCards.Location = new System.Drawing.Point(292, 15);
            this.cbDisplayOnlyOwnedCards.Name = "cbDisplayOnlyOwnedCards";
            this.cbDisplayOnlyOwnedCards.Size = new System.Drawing.Size(160, 21);
            this.cbDisplayOnlyOwnedCards.TabIndex = 29;
            this.cbDisplayOnlyOwnedCards.Text = "Only cards you have";
            this.cbDisplayOnlyOwnedCards.UseVisualStyleBackColor = true;
            // 
            // btnClearEvolveSkill
            // 
            this.btnClearEvolveSkill.Location = new System.Drawing.Point(299, 380);
            this.btnClearEvolveSkill.Name = "btnClearEvolveSkill";
            this.btnClearEvolveSkill.Size = new System.Drawing.Size(23, 23);
            this.btnClearEvolveSkill.TabIndex = 30;
            this.btnClearEvolveSkill.Text = "x";
            this.btnClearEvolveSkill.UseVisualStyleBackColor = true;
            this.btnClearEvolveSkill.Click += new System.EventHandler(this.btnClearEvolveSkill_Click);
            // 
            // pbEvolveLeft
            // 
            this.pbEvolveLeft.BackColor = System.Drawing.SystemColors.Control;
            this.pbEvolveLeft.Location = new System.Drawing.Point(312, 185);
            this.pbEvolveLeft.Name = "pbEvolveLeft";
            this.pbEvolveLeft.Size = new System.Drawing.Size(55, 55);
            this.pbEvolveLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEvolveLeft.TabIndex = 31;
            this.pbEvolveLeft.TabStop = false;
            this.pbEvolveLeft.Click += new System.EventHandler(this.pbEvolve_Click);
            // 
            // pbEvolveRight1
            // 
            this.pbEvolveRight1.BackColor = System.Drawing.SystemColors.Control;
            this.pbEvolveRight1.Location = new System.Drawing.Point(532, 323);
            this.pbEvolveRight1.Name = "pbEvolveRight1";
            this.pbEvolveRight1.Size = new System.Drawing.Size(55, 55);
            this.pbEvolveRight1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEvolveRight1.TabIndex = 32;
            this.pbEvolveRight1.TabStop = false;
            this.pbEvolveRight1.Click += new System.EventHandler(this.pbEvolve_Click);
            // 
            // lblLockSkill1
            // 
            this.lblLockSkill1.AutoSize = true;
            this.lblLockSkill1.Location = new System.Drawing.Point(79, 330);
            this.lblLockSkill1.Name = "lblLockSkill1";
            this.lblLockSkill1.Size = new System.Drawing.Size(13, 17);
            this.lblLockSkill1.TabIndex = 33;
            this.lblLockSkill1.Text = "-";
            // 
            // lblLockSkill2
            // 
            this.lblLockSkill2.AutoSize = true;
            this.lblLockSkill2.Location = new System.Drawing.Point(79, 347);
            this.lblLockSkill2.Name = "lblLockSkill2";
            this.lblLockSkill2.Size = new System.Drawing.Size(13, 17);
            this.lblLockSkill2.TabIndex = 34;
            this.lblLockSkill2.Text = "-";
            // 
            // lblLockSkillDemon
            // 
            this.lblLockSkillDemon.AutoSize = true;
            this.lblLockSkillDemon.Location = new System.Drawing.Point(232, 347);
            this.lblLockSkillDemon.Name = "lblLockSkillDemon";
            this.lblLockSkillDemon.Size = new System.Drawing.Size(13, 17);
            this.lblLockSkillDemon.TabIndex = 35;
            this.lblLockSkillDemon.Text = "-";
            // 
            // cbAllEvoSkills
            // 
            this.cbAllEvoSkills.AutoSize = true;
            this.cbAllEvoSkills.Location = new System.Drawing.Point(328, 382);
            this.cbAllEvoSkills.Name = "cbAllEvoSkills";
            this.cbAllEvoSkills.Size = new System.Drawing.Size(116, 21);
            this.cbAllEvoSkills.TabIndex = 36;
            this.cbAllEvoSkills.Text = "Show all skills";
            this.cbAllEvoSkills.UseVisualStyleBackColor = true;
            this.cbAllEvoSkills.CheckedChanged += new System.EventHandler(this.cbAllEvoSkills_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 274);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 34);
            this.label9.TabIndex = 37;
            this.label9.Text = "Times \r\nevolved";
            // 
            // numTimesEvolved
            // 
            this.numTimesEvolved.Location = new System.Drawing.Point(79, 274);
            this.numTimesEvolved.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numTimesEvolved.Name = "numTimesEvolved";
            this.numTimesEvolved.Size = new System.Drawing.Size(48, 22);
            this.numTimesEvolved.TabIndex = 38;
            this.numTimesEvolved.ValueChanged += new System.EventHandler(this.numTimesEvolved_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(132, 274);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 17);
            this.label10.TabIndex = 39;
            this.label10.Text = "Cost";
            // 
            // lblCost
            // 
            this.lblCost.AutoSize = true;
            this.lblCost.Location = new System.Drawing.Point(178, 274);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(16, 17);
            this.lblCost.TabIndex = 40;
            this.lblCost.Text = "0";
            // 
            // lblLockSkill1b
            // 
            this.lblLockSkill1b.AutoSize = true;
            this.lblLockSkill1b.Location = new System.Drawing.Point(232, 330);
            this.lblLockSkill1b.Name = "lblLockSkill1b";
            this.lblLockSkill1b.Size = new System.Drawing.Size(13, 17);
            this.lblLockSkill1b.TabIndex = 41;
            this.lblLockSkill1b.Text = "-";
            // 
            // pbEvolveRight2
            // 
            this.pbEvolveRight2.BackColor = System.Drawing.SystemColors.Control;
            this.pbEvolveRight2.Location = new System.Drawing.Point(532, 262);
            this.pbEvolveRight2.Name = "pbEvolveRight2";
            this.pbEvolveRight2.Size = new System.Drawing.Size(55, 55);
            this.pbEvolveRight2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEvolveRight2.TabIndex = 42;
            this.pbEvolveRight2.TabStop = false;
            this.pbEvolveRight2.Click += new System.EventHandler(this.pbEvolve_Click);
            // 
            // pbEvolveRight3
            // 
            this.pbEvolveRight3.BackColor = System.Drawing.SystemColors.Control;
            this.pbEvolveRight3.Location = new System.Drawing.Point(532, 201);
            this.pbEvolveRight3.Name = "pbEvolveRight3";
            this.pbEvolveRight3.Size = new System.Drawing.Size(55, 55);
            this.pbEvolveRight3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEvolveRight3.TabIndex = 43;
            this.pbEvolveRight3.TabStop = false;
            this.pbEvolveRight3.Click += new System.EventHandler(this.pbEvolve_Click);
            // 
            // pbEvolveRight4
            // 
            this.pbEvolveRight4.BackColor = System.Drawing.SystemColors.Control;
            this.pbEvolveRight4.Location = new System.Drawing.Point(532, 140);
            this.pbEvolveRight4.Name = "pbEvolveRight4";
            this.pbEvolveRight4.Size = new System.Drawing.Size(55, 55);
            this.pbEvolveRight4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEvolveRight4.TabIndex = 44;
            this.pbEvolveRight4.TabStop = false;
            this.pbEvolveRight4.Click += new System.EventHandler(this.pbEvolve_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(217, 274);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 17);
            this.label11.TabIndex = 45;
            this.label11.Text = "Wait";
            // 
            // lblWait
            // 
            this.lblWait.AutoSize = true;
            this.lblWait.Location = new System.Drawing.Point(263, 274);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(16, 17);
            this.lblWait.TabIndex = 46;
            this.lblWait.Text = "0";
            // 
            // pbFavourite
            // 
            this.pbFavourite.Image = global::ElementalKingdomsGui.Properties.Resources.unstar;
            this.pbFavourite.Location = new System.Drawing.Point(548, 41);
            this.pbFavourite.Name = "pbFavourite";
            this.pbFavourite.Size = new System.Drawing.Size(39, 33);
            this.pbFavourite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFavourite.TabIndex = 45;
            this.pbFavourite.TabStop = false;
            this.pbFavourite.Click += new System.EventHandler(this.pbFavourite_Click);
            // 
            // cbOnlyFavourites
            // 
            this.cbOnlyFavourites.AutoSize = true;
            this.cbOnlyFavourites.Location = new System.Drawing.Point(449, 14);
            this.cbOnlyFavourites.Name = "cbOnlyFavourites";
            this.cbOnlyFavourites.Size = new System.Drawing.Size(157, 21);
            this.cbOnlyFavourites.TabIndex = 46;
            this.cbOnlyFavourites.Text = "Only favourite cards";
            this.cbOnlyFavourites.UseVisualStyleBackColor = true;
            this.cbOnlyFavourites.Visible = false;
            // 
            // lblSkill2
            // 
            this.lblSkill2.AutoSize = true;
            this.lblSkill2.Location = new System.Drawing.Point(232, 313);
            this.lblSkill2.Name = "lblSkill2";
            this.lblSkill2.Size = new System.Drawing.Size(13, 17);
            this.lblSkill2.TabIndex = 47;
            this.lblSkill2.Text = "-";
            // 
            // CardSearcher
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 439);
            this.Controls.Add(this.lblSkill2);
            this.Controls.Add(this.lblWait);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbOnlyFavourites);
            this.Controls.Add(this.pbFavourite);
            this.Controls.Add(this.pbEvolveRight4);
            this.Controls.Add(this.pbEvolveRight3);
            this.Controls.Add(this.pbEvolveRight2);
            this.Controls.Add(this.lblLockSkill1b);
            this.Controls.Add(this.lblCost);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.numTimesEvolved);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cbAllEvoSkills);
            this.Controls.Add(this.lblLockSkillDemon);
            this.Controls.Add(this.lblLockSkill2);
            this.Controls.Add(this.lblLockSkill1);
            this.Controls.Add(this.pbEvolveRight1);
            this.Controls.Add(this.pbEvolveLeft);
            this.Controls.Add(this.btnClearEvolveSkill);
            this.Controls.Add(this.cbDisplayOnlyOwnedCards);
            this.Controls.Add(this.cbEvolveSkill);
            this.Controls.Add(this.cbSkillFilter);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbClassOther);
            this.Controls.Add(this.cbClassSwamp);
            this.Controls.Add(this.cbClassMountain);
            this.Controls.Add(this.cbClassTundra);
            this.Controls.Add(this.cbClassForest);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbStars5);
            this.Controls.Add(this.cbStars4);
            this.Controls.Add(this.cbStars3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbStars2);
            this.Controls.Add(this.cbStars1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblSkill1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblHP);
            this.Controls.Add(this.lblAttack);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numLevel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.tbFilter);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbCards);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CardSearcher";
            this.Text = "CardSearcher";
            this.Shown += new System.EventHandler(this.CardSearcher_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTimesEvolved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEvolveRight4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFavourite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbCards;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox tbFilter;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.NumericUpDown numLevel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblAttack;
        private System.Windows.Forms.Label lblHP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSkill1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbStars1;
        private System.Windows.Forms.CheckBox cbStars2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbStars3;
        private System.Windows.Forms.CheckBox cbStars4;
        private System.Windows.Forms.CheckBox cbStars5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbClassForest;
        private System.Windows.Forms.CheckBox cbClassTundra;
        private System.Windows.Forms.CheckBox cbClassMountain;
        private System.Windows.Forms.CheckBox cbClassSwamp;
        private System.Windows.Forms.CheckBox cbClassOther;
        private System.ComponentModel.BackgroundWorker imageLoader;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbSkillFilter;
        private System.Windows.Forms.ComboBox cbEvolveSkill;
        private System.Windows.Forms.CheckBox cbDisplayOnlyOwnedCards;
        private System.Windows.Forms.Button btnClearEvolveSkill;
        private System.Windows.Forms.PictureBox pbEvolveLeft;
        private System.Windows.Forms.PictureBox pbEvolveRight1;
        private System.Windows.Forms.ToolTip ttEvo;
        private System.Windows.Forms.Label lblLockSkill1;
        private System.Windows.Forms.Label lblLockSkill2;
        private System.Windows.Forms.Label lblLockSkillDemon;
        private System.Windows.Forms.CheckBox cbAllEvoSkills;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numTimesEvolved;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Label lblLockSkill1b;
        private System.Windows.Forms.PictureBox pbEvolveRight2;
        private System.Windows.Forms.PictureBox pbEvolveRight3;
        private System.Windows.Forms.PictureBox pbEvolveRight4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblWait;
        private System.Windows.Forms.PictureBox pbFavourite;
        private System.Windows.Forms.CheckBox cbOnlyFavourites;
        private System.Windows.Forms.Label lblSkill2;
    }
}