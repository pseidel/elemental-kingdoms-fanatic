﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.util;
using ElementalKingdomsGui.Properties;

namespace ElementalKingdoms.ui
{
    public partial class LeaguePlayer : UserControl
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private League.LeagueRoundResult player;

        private int WinChanceAttack { get; set; }
        private int WinChanceDefense { get; set; }

        public LeaguePlayer()
        {
            InitializeComponent();
            WinChanceAttack = -1;
            WinChanceDefense = -1;
            lblName.Text = "";
            lblOddsGame.Text = "";
            lblOddsSim.Text = "";
        }

        public void SetLeagueRoundResult(League.LeagueRoundResult result)
        {
            player = result;
            log.InfoFormat("Name  : {0}", player.BattleInfo.User.NickName);
            log.InfoFormat("Avatar: {0}", player.BattleInfo.User.Avatar);
            log.InfoFormat("Total : {0}", player.BetTotal);
            log.InfoFormat("Total : {0}", player.BetOdds);

            lblName.Text = player.BattleInfo.User.NickName;
            if (player.BattleInfo.User.Avatar != 0)
            {
                Card c = Card.Get(player.BattleInfo.User.Avatar);
                if (c == null)
                {
                    log.ErrorFormat("Avatar using an unknown card (id={0})", player.BattleInfo.User.Avatar);
                }
                else
                {
                    pbAvatar.Image = ImageLoader.GetSmallImageByName(c.Name);
                }
            }
            else
            {
                if (player.BattleInfo.User.Sex == Gender.Female)
                {
                    pbAvatar.Image = Resources.female;
                }
                else
                {
                    pbAvatar.Image = Resources.male;
                }
            }
            if (player.BetOdds != 0)
            {
                lblOddsGame.Text = player.BetOdds.ToString();
                toolTip.SetToolTip(pbAvatar, String.Format("{0} lvl {1}\r\nHonor bet: {2}", player.BattleInfo.User.NickName, player.BattleInfo.User.Level, player.BetTotal));
            }
        }

        public void SetAttackWinChance(int chance)
        {
            WinChanceAttack = chance;
            UpdateSimOddsLabel();
        }

        public void SetDefendWinChance(int chance)
        {
            WinChanceDefense = chance;
            UpdateSimOddsLabel();
        }

        private void UpdateSimOddsLabel()
        {
            int min = Math.Min(WinChanceAttack, WinChanceDefense);
            int max = Math.Max(WinChanceAttack, WinChanceDefense);
            if (min == -1)
            {
                lblOddsSim.Text = String.Format("{0}%", max);
            }
            else
            {
                lblOddsSim.Text = String.Format("{0} - {1}%", min, max);
            }
        }
    }
}
