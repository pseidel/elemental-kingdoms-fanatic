﻿namespace ElementalKingdomsGui.ui
{
    partial class QuickEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuickEdit));
            this.label2 = new System.Windows.Forms.Label();
            this.rbCrystark = new System.Windows.Forms.RadioButton();
            this.rbMitzi = new System.Windows.Forms.RadioButton();
            this.rbPeppa = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.rbEKU = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.timerFireUpdateEvent = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 386);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Deck format";
            // 
            // rbCrystark
            // 
            this.rbCrystark.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbCrystark.AutoSize = true;
            this.rbCrystark.Checked = true;
            this.rbCrystark.Location = new System.Drawing.Point(111, 359);
            this.rbCrystark.Name = "rbCrystark";
            this.rbCrystark.Size = new System.Drawing.Size(81, 21);
            this.rbCrystark.TabIndex = 9;
            this.rbCrystark.TabStop = true;
            this.rbCrystark.Text = "Crystark";
            this.rbCrystark.UseVisualStyleBackColor = true;
            this.rbCrystark.CheckedChanged += new System.EventHandler(this.rbCrystark_CheckedChanged);
            // 
            // rbMitzi
            // 
            this.rbMitzi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbMitzi.AutoSize = true;
            this.rbMitzi.Location = new System.Drawing.Point(111, 386);
            this.rbMitzi.Name = "rbMitzi";
            this.rbMitzi.Size = new System.Drawing.Size(57, 21);
            this.rbMitzi.TabIndex = 8;
            this.rbMitzi.Text = "Mitzi";
            this.rbMitzi.UseVisualStyleBackColor = true;
            this.rbMitzi.CheckedChanged += new System.EventHandler(this.rbMitzi_CheckedChanged);
            // 
            // rbPeppa
            // 
            this.rbPeppa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbPeppa.AutoSize = true;
            this.rbPeppa.Location = new System.Drawing.Point(111, 413);
            this.rbPeppa.Name = "rbPeppa";
            this.rbPeppa.Size = new System.Drawing.Size(70, 21);
            this.rbPeppa.TabIndex = 7;
            this.rbPeppa.Text = "Peppa";
            this.rbPeppa.UseVisualStyleBackColor = true;
            this.rbPeppa.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(11, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(464, 338);
            this.textBox1.TabIndex = 11;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // rbEKU
            // 
            this.rbEKU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbEKU.AutoSize = true;
            this.rbEKU.Location = new System.Drawing.Point(259, 384);
            this.rbEKU.Name = "rbEKU";
            this.rbEKU.Size = new System.Drawing.Size(57, 21);
            this.rbEKU.TabIndex = 12;
            this.rbEKU.Text = "EKU";
            this.rbEKU.UseVisualStyleBackColor = true;
            this.rbEKU.Visible = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 437);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(464, 24);
            this.label1.TabIndex = 13;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timerFireUpdateEvent
            // 
            this.timerFireUpdateEvent.Interval = 200;
            // 
            // QuickEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 470);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbEKU);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbCrystark);
            this.Controls.Add(this.rbMitzi);
            this.Controls.Add(this.rbPeppa);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "QuickEdit";
            this.Text = "QuickEdit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.QuickEdit_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbCrystark;
        private System.Windows.Forms.RadioButton rbMitzi;
        private System.Windows.Forms.RadioButton rbPeppa;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RadioButton rbEKU;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerFireUpdateEvent;
    }
}