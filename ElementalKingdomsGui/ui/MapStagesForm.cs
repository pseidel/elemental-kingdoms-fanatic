﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.server;
using ElementalKingdoms.tools;
using Newtonsoft.Json.Linq;

namespace ElementalKingdoms.ui
{
    public partial class MapStagesForm : Form
    {
        User user;
        MapStageDetail currentMap;
        Dictionary<int, Button> mapStages = new Dictionary<int, Button>();
        ServerCommunicator com;
        public MapStagesForm()
        {
            InitializeComponent();

            com = ServerCommunicator.Create(ElementalKingdomsGui.Properties.Settings.Default.DeviceId, true);
            
            foreach (MapStage map in MapStage.AllMapStages)
            {
                Label lblMap = new Label()
                {
                    Text = String.Format("{0} {1}", map.Id, map.Name),
                    Location = new Point(20, 30 + map.Id * 40)
                };
                this.Controls.Add(lblMap);

                foreach (MapStageDetail level in map.MapStageDetails)
                {
                    Button btnLevel = new Button()
                    {
                        Text = level.Name,
                        Location = new Point(150 + 80 * level.Rank, 30 + map.Id * 40),
                        Tag = level
                    };
                    btnLevel.Click += btnLevel_Click;
                    btnLevel.Enabled = false;
                    btnLevel.MouseMove += btnLevel_MouseMove;
                    mapStages.Add(level.MapStageDetailId, btnLevel);
                    this.Controls.Add(btnLevel);
                }
            }
        }

        void btnLevel_MouseMove(object sender, MouseEventArgs e)
        {
            if (toolTip.Tag == sender)
            {
                return;
            }
            Button btnLevel = sender as Button;
            MapStageDetail stage = btnLevel.Tag as MapStageDetail;

            StringBuilder sb = new StringBuilder();

            int finishedStage = 0;

            if (user.UserMapStages.ContainsKey(stage.MapStageDetailId))
            {
                var userInfo = user.UserMapStages[stage.MapStageDetailId];
                finishedStage = userInfo.FinishedStage;
            }


            if (stage.IsTower)
            {
                sb.AppendFormat("Tower");
            }
            else if (finishedStage < 3)
            {
                var level = stage.Levels[finishedStage];

                User u = new User();
                u.Level = level.HeroLevel;
                u.UserDeck.Cards = level.Cards;
                MultiBattleExecutor mbi = new MultiBattleExecutor(user, u);
                var battleResult = mbi.Execute(100);

                sb.AppendFormat("{0} {1}\n{2}", stage.Rank, stage.FightName, level.AchievementText);
                sb.AppendLine();
                sb.AppendFormat("{0}% win chance", (battleResult.RoundsWon * 100 / battleResult.Iterations));

            }
            else
            {
                sb.AppendFormat("Stage completed!");
            }

            String tooltipText = sb.ToString();
            toolTip.SetToolTip(btnLevel, tooltipText);
            toolTip.Tag = btnLevel;
        }

        void btnLevel_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            MapStageDetail mapStage = btn.Tag as MapStageDetail;

            currentMap = mapStage;

            if (mapStage.IsTower)
            {
                //throw new NotImplementedException();
                // TODO: Should move this to a Maze / Tower class in main project
                JObject currentTowerState = com.SelectTower(mapStage.MapStageId);

                if (currentTowerState["data"]["Clear"].Value<int>() != 0)
                {
                    Console.WriteLine("Tower seems to be cleared...");
                    if (currentTowerState["data"]["FreeReset"].Value<int>() == 1)
                    {
                        com.ResetTower(mapStage.MapStageId); 
                        currentTowerState = com.SelectTower(mapStage.MapStageId);
                    }
                    else
                    {
                        // TODO: Gem reset?
                        return;
                    }
                }

                int currentLayer = currentTowerState["data"]["Layer"].Value<int>();
                int totalLayers = MapStage.AllMapStages.First(x => x.Id == mapStage.MapStageId).MazeCount;

                for (int i = currentLayer; i <= totalLayers; i++)
                {
                    JObject levelInfo = com.EnterTowerFloor(mapStage.MapStageId, i);
                    int[] items = levelInfo["data"]["Map"]["Items"].ToObject<int[]>();
                    // 1 => empty
                    // 2 => Chest
                    // 3 => Monster
                    // 4 => Stairs up / start location
                    // 5 => Stairs down

                    for (int n = 0; n < items.Length; n++)
                    {
                        if (items[n] == 2 || items[n] == 3)
                        {
                            JObject battleResult = com.FightTowerBattle(mapStage.MapStageId, i, n);
                            //Console.WriteLine(battleResult.ToString());
                            Console.WriteLine("Battle failed: {0}", battleResult["message"]);
                            return;
                        }
                    }

                    if (!levelInfo["data"]["Map"]["IsFinish"].Value<bool>())
                    {

                        int stairsIndex = Array.IndexOf(items, 5);
                        JObject battleResult = com.FightTowerBattle(mapStage.MapStageId, i, stairsIndex);
                        if (battleResult["status"].Value<int>() == 0)
                        {
                            Console.WriteLine("Battle failed: {0}", battleResult["message"]);
                            return;
                        }
                        //Console.WriteLine(battleResult.ToString());
                    }

                    Console.WriteLine(" test" );
                }
                //*/
            }
            else
            {
                var battleResult = com.UserMapBattle(mapStage.MapStageDetailId, false);

                if (battleResult.Value<int>("status") == 1)
                {

                    if (battleResult["data"].Value<int>("Win") == 1)
                    {
                        mapStages[mapStage.Next].Enabled = true;
                    }

                    com.GetUserMapStages();
                }
            }
        }

        public void SetUserMapStages(User u)
        {
            user = u;
            int currentStage = 0;
            int currentLevel = 0;
            foreach (var map in u.UserMapStages.Values)
            {
                Console.WriteLine(map.MapStageDetailId);
                mapStages[map.MapStageDetailId].Enabled = true;
                var detail = mapStages[map.MapStageDetailId].Tag as MapStageDetail;
                mapStages[detail.Next].Enabled = true;
                currentStage = Math.Max(currentStage, map.MapStageId);
                currentLevel = Math.Max(currentLevel, detail.Next);
            }

            foreach (var map in MapStage.AllMapStages.Where(x => x.Id <= currentStage))
            {
                foreach (var mapStage in map.MapStageDetails.Where(x => x.IsTower && x.MapStageDetailId < currentLevel))
                {
                    Console.WriteLine("Tower {0} is unlocked", mapStage.Name);
                    mapStages[mapStage.MapStageDetailId].Enabled = true;
                }
            }
        }
    }
}
