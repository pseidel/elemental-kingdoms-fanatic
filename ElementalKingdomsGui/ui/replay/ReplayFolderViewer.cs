﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ElementalKingdoms.core.history;

namespace ElementalKingdomsGui.ui.replay
{
    public partial class ReplayFolderViewer : UserControl
    {
        public String Folder { get; set; }
        public ReplayFolderViewer()
        {
            InitializeComponent();
            Folder = @"cache\battle";
            //System.IO.Directory.CreateDirectory(Folder); // create if it doesn't exist
            FillTree();
        }

        private void FillTree()
        {
            DirectoryInfo di = new DirectoryInfo(Folder);
            if (di.Exists)
            {
                AddDirectory(treeFiles.Nodes, di);
                treeFiles.ExpandAll();
            }
        }

        private void AddDirectory(TreeNodeCollection parent, DirectoryInfo directory)
        {
            TreeNode root = new TreeNode(directory.Name);
            parent.Add(root);

            foreach (var dir in directory.GetDirectories())
            {
                AddDirectory(root.Nodes, dir);
            }

            foreach (var file in directory.GetFiles().OrderBy(x => x.CreationTime))
            {
                TreeNode fileNode = new TreeNode(file.Name);
                fileNode.Tag = file.FullName;
                root.Nodes.Add(fileNode);

            }
        }

        private void treeFiles_ItemDrag(object sender, ItemDragEventArgs e)
        {
            var i = e.Item as TreeNode;

            if (i != null && i.Tag != null)
            {
                //Console.WriteLine("Dragging... {0}", i.Tag);
                //DoDragDrop(e.Item, DragDropEffects.Link);

                Battle b = ElementalKingdoms.util.JsonLoader.LoadBattle(i.Tag.ToString());
                //if (first)
                //{
                //    loadBattle(b);
                //    first = false;
                //}
            }
        }

        public delegate void ReplaySelectionChangedEventHandler(object sender, ReplaySelectionChangedEventArgs e);
        public event ReplaySelectionChangedEventHandler ReplaySelectionChanged;

        protected void OnSelectedItemChangedChanged(Battle b)
        {
            if (ReplaySelectionChanged != null && b != null)
            {
                ReplaySelectionChanged(this, new ReplaySelectionChangedEventArgs(b));
            }
        }

        public class ReplaySelectionChangedEventArgs
        {
            public Battle Battle { get; set; }

            public ReplaySelectionChangedEventArgs(Battle b)
            {
                this.Battle = b;
            }
        }

        private void treeFiles_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != null && e.Node.Tag != null)
            {
                //Console.WriteLine("Dragging... {0}", i.Tag);
                //DoDragDrop(e.Item, DragDropEffects.Link);

                Battle b = ElementalKingdoms.util.JsonLoader.LoadBattle(e.Node.Tag.ToString());
                OnSelectedItemChangedChanged(b);
                //if (first)
                //{
                //    loadBattle(b);
                //    first = false;
                //}
            }
        }
    }
}
