﻿namespace ElementalKingdoms.ui
{
    partial class BattleRuneViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbRune4 = new System.Windows.Forms.PictureBox();
            this.pbRune3 = new System.Windows.Forms.PictureBox();
            this.pbRune2 = new System.Windows.Forms.PictureBox();
            this.pbRune1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbRune4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRune3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRune2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRune1)).BeginInit();
            this.SuspendLayout();
            // 
            // pbRune4
            // 
            this.pbRune4.Location = new System.Drawing.Point(70, 66);
            this.pbRune4.Name = "pbRune4";
            this.pbRune4.Size = new System.Drawing.Size(55, 55);
            this.pbRune4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRune4.TabIndex = 3;
            this.pbRune4.TabStop = false;
            // 
            // pbRune3
            // 
            this.pbRune3.Location = new System.Drawing.Point(10, 66);
            this.pbRune3.Name = "pbRune3";
            this.pbRune3.Size = new System.Drawing.Size(55, 55);
            this.pbRune3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRune3.TabIndex = 2;
            this.pbRune3.TabStop = false;
            // 
            // pbRune2
            // 
            this.pbRune2.Location = new System.Drawing.Point(70, 10);
            this.pbRune2.Name = "pbRune2";
            this.pbRune2.Size = new System.Drawing.Size(55, 55);
            this.pbRune2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRune2.TabIndex = 1;
            this.pbRune2.TabStop = false;
            // 
            // pbRune1
            // 
            this.pbRune1.Location = new System.Drawing.Point(10, 10);
            this.pbRune1.Name = "pbRune1";
            this.pbRune1.Size = new System.Drawing.Size(55, 55);
            this.pbRune1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRune1.TabIndex = 0;
            this.pbRune1.TabStop = false;
            // 
            // BattleRuneViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pbRune4);
            this.Controls.Add(this.pbRune3);
            this.Controls.Add(this.pbRune2);
            this.Controls.Add(this.pbRune1);
            this.Name = "BattleRuneViewer";
            this.Size = new System.Drawing.Size(130, 130);
            ((System.ComponentModel.ISupportInitialize)(this.pbRune4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRune3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRune2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRune1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbRune1;
        private System.Windows.Forms.PictureBox pbRune2;
        private System.Windows.Forms.PictureBox pbRune4;
        private System.Windows.Forms.PictureBox pbRune3;
    }
}
