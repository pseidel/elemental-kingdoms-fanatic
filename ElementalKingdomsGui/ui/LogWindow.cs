﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net.Appender;

namespace ElementalKingdoms.ui
{
    public partial class LogWindow : Form, IAppender
    {
        public LogWindow()
        {
            InitializeComponent();
        }

        public void DoAppend(log4net.Core.LoggingEvent loggingEvent)
        {
            AppendText(String.Format("{0} {1}: {2}\r\n", loggingEvent.Level.Name, loggingEvent.LoggerName, loggingEvent.MessageObject.ToString()));
        }

        delegate void SetTextCallback(string text);

        private void AppendText(string text)
        {
            if (textBox1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(AppendText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                var x = this.Handle; 
                textBox1.AppendText(text);
            }
        }

        private void LogWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }
    }
}
