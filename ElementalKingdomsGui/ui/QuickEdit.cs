﻿using ElementalKingdoms.core;
using ElementalKingdoms.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ElementalKingdomsGui.ui
{
    public partial class QuickEdit : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IDeckConverter deckConverter;

        private User user;
        
        public delegate void DeckChangedHandler(User u);

        public event DeckChangedHandler DeckChanged;

        protected virtual void OnDeckChanged()
        {
            DeckChangedHandler handler = DeckChanged;
            if (handler != null)
            {
                handler(user);
            }
        }

        public QuickEdit()
        {
            InitializeComponent();
            deckConverter = new CrystarkLoader();
            timerFireUpdateEvent.Tick += TimerFireUpdateEvent_Tick;
        }

        private void TimerFireUpdateEvent_Tick(object sender, EventArgs e)
        {
            log.Debug("Timer fired");
            ParseDeck();
            timerFireUpdateEvent.Stop();
        }

        private void FireDeckChangedUpdate()
        {
            log.Debug("Starting timer...");
            if (timerFireUpdateEvent.Enabled)
            {
                log.Debug("Timer was running");
                timerFireUpdateEvent.Stop();
            }
            timerFireUpdateEvent.Start();
        }

        public void SetUser(User u)
        {
            user = u;
            UpdateEdit();
        }

        private void UpdateEdit()
        {
            textBox1.Text = deckConverter.GetDeckString(user);
        }

        private void UpdateDeck(object sender, EventArgs e)
        {
            if (log.IsDebugEnabled) log.DebugFormat("Deck 1 changed.");
            
        }

        private void rbCrystark_CheckedChanged(object sender, EventArgs e)
        {
            deckConverter = new CrystarkLoader();
            UpdateEdit();
        }

        private void rbMitzi_CheckedChanged(object sender, EventArgs e)
        {
            deckConverter = new MitziLoader();
            UpdateEdit();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            FireDeckChangedUpdate();
        }

        private void ParseDeck()
        {
            log.Debug("Parsing deck");
            User u = null;
            try
            {
                var oldLevel = ElementalKingdoms.Util.SetLoglevel(log4net.Core.Level.Error);
                u = deckConverter.GetUserFromString(textBox1.Text);
                ElementalKingdoms.Util.SetLoglevel(oldLevel);
            }
            catch (Exception ex)
            {
                log.DebugFormat("Exception: {0}", ex.ToString());
            }

            if (u != null)
            {
                label1.Text = "Deck OK";
                label1.BackColor = Color.Green;
                label1.ForeColor = Color.White;
                user = u;
                OnDeckChanged();
            }
            else
            {
                label1.Text = "Deck has errors";
                label1.BackColor = Color.Red;
                label1.ForeColor = Color.White;
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            FireDeckChangedUpdate();
        }

        private void QuickEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }
    }
}
