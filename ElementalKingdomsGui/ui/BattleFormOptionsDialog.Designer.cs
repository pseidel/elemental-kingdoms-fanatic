﻿namespace ElementalKingdoms.ui
{
    partial class BattleFormOptionsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BattleFormOptionsDialog));
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.editHydraMeritCardButton1 = new ElementalKingdomsGui.ui.controls.EditCardButton();
            this.editHydraMeritCardButton3 = new ElementalKingdomsGui.ui.controls.EditCardButton();
            this.editHydraMeritCardButton2 = new ElementalKingdomsGui.ui.controls.EditCardButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.editEwMeritCardButton1 = new ElementalKingdomsGui.ui.controls.EditCardButton();
            this.editEwMeritCardButton2 = new ElementalKingdomsGui.ui.controls.EditCardButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.editDIMeritCardButton1 = new ElementalKingdomsGui.ui.controls.EditCardButton();
            this.editDIMeritCardButton2 = new ElementalKingdomsGui.ui.controls.EditCardButton();
            this.cbAlwaysPerformBattle = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.Location = new System.Drawing.Point(196, 510);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.editHydraMeritCardButton1);
            this.groupBox1.Controls.Add(this.editHydraMeritCardButton3);
            this.groupBox1.Controls.Add(this.editHydraMeritCardButton2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(424, 147);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hydra merit cards";
            // 
            // editHydraMeritCardButton1
            // 
            this.editHydraMeritCardButton1.DefaultText = null;
            this.editHydraMeritCardButton1.EditCard = null;
            this.editHydraMeritCardButton1.Location = new System.Drawing.Point(13, 23);
            this.editHydraMeritCardButton1.Name = "editHydraMeritCardButton1";
            this.editHydraMeritCardButton1.Size = new System.Drawing.Size(100, 100);
            this.editHydraMeritCardButton1.TabIndex = 3;
            // 
            // editHydraMeritCardButton3
            // 
            this.editHydraMeritCardButton3.DefaultText = null;
            this.editHydraMeritCardButton3.EditCard = null;
            this.editHydraMeritCardButton3.Location = new System.Drawing.Point(304, 23);
            this.editHydraMeritCardButton3.Name = "editHydraMeritCardButton3";
            this.editHydraMeritCardButton3.Size = new System.Drawing.Size(100, 100);
            this.editHydraMeritCardButton3.TabIndex = 5;
            // 
            // editHydraMeritCardButton2
            // 
            this.editHydraMeritCardButton2.DefaultText = null;
            this.editHydraMeritCardButton2.EditCard = null;
            this.editHydraMeritCardButton2.Location = new System.Drawing.Point(159, 23);
            this.editHydraMeritCardButton2.Name = "editHydraMeritCardButton2";
            this.editHydraMeritCardButton2.Size = new System.Drawing.Size(100, 100);
            this.editHydraMeritCardButton2.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.editEwMeritCardButton1);
            this.groupBox2.Controls.Add(this.editEwMeritCardButton2);
            this.groupBox2.Location = new System.Drawing.Point(12, 165);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(424, 147);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Elemental War merit cards";
            // 
            // editEwMeritCardButton1
            // 
            this.editEwMeritCardButton1.DefaultText = null;
            this.editEwMeritCardButton1.EditCard = null;
            this.editEwMeritCardButton1.Location = new System.Drawing.Point(13, 23);
            this.editEwMeritCardButton1.Name = "editEwMeritCardButton1";
            this.editEwMeritCardButton1.Size = new System.Drawing.Size(100, 100);
            this.editEwMeritCardButton1.TabIndex = 3;
            // 
            // editEwMeritCardButton2
            // 
            this.editEwMeritCardButton2.DefaultText = null;
            this.editEwMeritCardButton2.EditCard = null;
            this.editEwMeritCardButton2.Location = new System.Drawing.Point(159, 23);
            this.editEwMeritCardButton2.Name = "editEwMeritCardButton2";
            this.editEwMeritCardButton2.Size = new System.Drawing.Size(100, 100);
            this.editEwMeritCardButton2.TabIndex = 4;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.editDIMeritCardButton1);
            this.groupBox3.Controls.Add(this.editDIMeritCardButton2);
            this.groupBox3.Location = new System.Drawing.Point(12, 318);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(424, 147);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Demon Invasion merit cards";
            // 
            // editDIMeritCardButton1
            // 
            this.editDIMeritCardButton1.DefaultText = null;
            this.editDIMeritCardButton1.EditCard = null;
            this.editDIMeritCardButton1.Location = new System.Drawing.Point(13, 23);
            this.editDIMeritCardButton1.Name = "editDIMeritCardButton1";
            this.editDIMeritCardButton1.Size = new System.Drawing.Size(100, 100);
            this.editDIMeritCardButton1.TabIndex = 3;
            // 
            // editDIMeritCardButton2
            // 
            this.editDIMeritCardButton2.DefaultText = null;
            this.editDIMeritCardButton2.EditCard = null;
            this.editDIMeritCardButton2.Location = new System.Drawing.Point(159, 23);
            this.editDIMeritCardButton2.Name = "editDIMeritCardButton2";
            this.editDIMeritCardButton2.Size = new System.Drawing.Size(100, 100);
            this.editDIMeritCardButton2.TabIndex = 4;
            // 
            // cbAlwaysPerformBattle
            // 
            this.cbAlwaysPerformBattle.AutoSize = true;
            this.cbAlwaysPerformBattle.Checked = global::ElementalKingdomsGui.Properties.Settings.Default.autoRecalculateResults;
            this.cbAlwaysPerformBattle.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::ElementalKingdomsGui.Properties.Settings.Default, "autoRecalculateResults", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbAlwaysPerformBattle.Location = new System.Drawing.Point(25, 483);
            this.cbAlwaysPerformBattle.Name = "cbAlwaysPerformBattle";
            this.cbAlwaysPerformBattle.Size = new System.Drawing.Size(287, 21);
            this.cbAlwaysPerformBattle.TabIndex = 0;
            this.cbAlwaysPerformBattle.Text = "Automatically calculate new battle results";
            this.cbAlwaysPerformBattle.UseVisualStyleBackColor = true;
            this.cbAlwaysPerformBattle.Visible = false;
            // 
            // BattleFormOptionsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 549);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cbAlwaysPerformBattle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BattleFormOptionsDialog";
            this.Text = "Options";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbAlwaysPerformBattle;
        private System.Windows.Forms.Button btnSave;
        private ElementalKingdomsGui.ui.controls.EditCardButton editHydraMeritCardButton1;
        private ElementalKingdomsGui.ui.controls.EditCardButton editHydraMeritCardButton2;
        private ElementalKingdomsGui.ui.controls.EditCardButton editHydraMeritCardButton3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private ElementalKingdomsGui.ui.controls.EditCardButton editEwMeritCardButton1;
        private ElementalKingdomsGui.ui.controls.EditCardButton editEwMeritCardButton2;
        private System.Windows.Forms.GroupBox groupBox3;
        private ElementalKingdomsGui.ui.controls.EditCardButton editDIMeritCardButton1;
        private ElementalKingdomsGui.ui.controls.EditCardButton editDIMeritCardButton2;
    }
}