﻿using ElementalKingdoms.core;
using ElementalKingdoms.tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElementalKingdoms.ui
{
    public partial class CounterdeckGeneratorForm : Form
    {
        public CounterdeckGeneratorForm()
        {
            InitializeComponent();
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            User attacker = new User();
            attacker.Level = 81;

            User defender = new User();
            defender.Level = 0;
            //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Dark Titan"), Level = 10 });
            //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Mars"), Level = 10 });
            //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Deucalion"), Level = 10 });

            for (int i = 0; i < 10; i++)
            {
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
            }

            CounterdeckGenerator generator = new CounterdeckGenerator(attacker, defender);
        }
    }
}
