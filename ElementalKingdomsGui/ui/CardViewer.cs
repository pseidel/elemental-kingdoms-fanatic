﻿using ElementalKingdoms.core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElementalKingdoms
{
    public partial class CardViewer : Form
    {
        private Game game;
        public CardViewer(Game g)
        {
            InitializeComponent();
            game = g;

            dataGridView1.DataSource = Card.Cards;


            //making sure all columns are sortable
            foreach (DataGridViewColumn col in this.dataGridView1.Columns)
            {
                col.SortMode = DataGridViewColumnSortMode.Automatic;
            }    
        }
    }
}
