﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.util;

namespace ElementalKingdoms.ui
{
    public partial class BattleFieldCard : UserControl
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        BattleCard card;
        core.history.BattleCard histCard;

        public BattleFieldCard()
        {
            InitializeComponent();
            InitializeTooltip();

            Reset();
        }

        private void InitializeTooltip()
        {
            toolTip1.SetToolTip(lblBurned, "Burned");
            toolTip1.SetToolTip(lblPoisoned, "Poisoned");
            toolTip1.SetToolTip(lblParalyzed, "Paralyzed");
            toolTip1.SetToolTip(lblFrozen, "Frozen");
            toolTip1.SetToolTip(lblConfused, "Confused");
            toolTip1.SetToolTip(lblLaceration, "Lacerated");
            toolTip1.SetToolTip(lblTrapped, "Trapped");
            toolTip1.SetToolTip(lblStunned, "Stunned");
            toolTip1.SetToolTip(lblSilenced, "Silenced");
            toolTip1.SetToolTip(lblDeathMark, "Death Marked");
            toolTip1.SetToolTip(lblBlinded, "Blinded");
        }

        private void Reset()
        {
            pbCard.Image = null;
            lblHp.Visible = false;
            lblAttack.Visible = false;

            lblBurned.Visible = false;
            lblPoisoned.Visible = false;
            lblParalyzed.Visible = false;
            lblFrozen.Visible = false;
            lblConfused.Visible = false;
            lblLaceration.Visible = false;
            lblTrapped.Visible = false;
            lblStunned.Visible = false;
            lblSilenced.Visible = false;
            lblDeathMark.Visible = false;
            lblBlinded.Visible = false;
        }

        public void SetCard(BattleCard bc)
        {
            if (bc == null)
            {
                Reset();
            }
            else
            {
                if (card == null || card.Name != bc.Name)
                {
                    pbCard.Image = ImageLoader.GetImageByName(bc.Name);
                    lblHp.Visible = true;
                    lblAttack.Visible = true;
                }
                StringBuilder tooltipText = new StringBuilder();
                tooltipText.AppendFormat("{0} level {1}", bc.Name, bc.Card.Level).AppendLine();
                tooltipText.AppendFormat("ATK: {0}", bc.CurrentTotalAttack).AppendLine();
                tooltipText.AppendFormat("HP : {0}/{1}", bc.CurrentHP, bc.BaseHP + bc.BonusSkillHP).AppendLine();
                tooltipText.AppendLine();
                foreach (var skill in bc.Card.ActiveSkills())
                {
                    tooltipText.AppendFormat("{0}: {1}", skill.Name, skill.Desc).AppendLine();
                }

                toolTip1.SetToolTip(pbCard, tooltipText.ToString());

                lblHp.Text = bc.CurrentHP.ToString();
                lblAttack.Text = bc.CurrentTotalAttack.ToString();

                if (bc.Burned)
                {
                    lblBurned.Text = bc.Burns.Values.Sum().ToString();
                }
                lblBurned.Visible = bc.Burned;

                if (bc.Poisoned)
                {
                    lblPoisoned.Text = bc.Poisons.Sum().ToString();
                }
                lblPoisoned.Visible = bc.Poisoned;

                if (bc.DeathMark > 0)
                {
                    lblDeathMark.Text = bc.DeathMark.ToString();
                }
                lblDeathMark.Visible = bc.DeathMark > 0;

                lblParalyzed.Visible = bc.Paralyzed;
                lblFrozen.Visible = bc.Frozen;
                lblConfused.Visible = bc.Confused;
                lblLaceration.Visible = bc.Laceration;
                lblTrapped.Visible = bc.Trapped;
                lblStunned.Visible = bc.Stunned;
                lblSilenced.Visible = bc.Silenced;
                lblBlinded.Visible = bc.Blinded;
            }
            card = bc;
        }

        public void SetCard(core.history.BattleCard bc)
        {
            if (bc == null)
            {
                Reset();
            }
            else
            {
                if (histCard == null || histCard.Name != bc.Name)
                {
                    pbCard.Image = ImageLoader.GetImageByName(bc.Name);
                    lblHp.Visible = true;
                    lblAttack.Visible = true;
                }
                StringBuilder tooltipText = new StringBuilder();
                tooltipText.AppendFormat("{0} level {1}", bc.Name, bc.Card.Level).AppendLine();
                tooltipText.AppendFormat("ATK: {0}", bc.CurrentTotalAttack).AppendLine();
                tooltipText.AppendFormat("HP : {0}/{1}", bc.CurrentHP, bc.Card.HP).AppendLine();
                tooltipText.AppendLine();
                foreach (var skill in bc.Card.ActiveSkills())
                {
                    tooltipText.AppendFormat("{0}: {1}", skill.Name, skill.Desc).AppendLine();
                }

                toolTip1.SetToolTip(pbCard, tooltipText.ToString());

                lblHp.Text = bc.CurrentHP.ToString();
                lblAttack.Text = bc.CurrentTotalAttack.ToString();

                if (bc.Burned)
                {
                    lblBurned.Text = bc.Burns.Values.Sum().ToString();
                }
                lblBurned.Visible = bc.Burned;

                if (bc.Poisoned)
                {
                    lblPoisoned.Text = bc.Poisons.Sum().ToString();
                }
                lblPoisoned.Visible = bc.Poisoned;

                lblParalyzed.Visible = bc.Paralyzed;
                lblFrozen.Visible = bc.Frozen;
                lblConfused.Visible = bc.Confused;
                lblLaceration.Visible = bc.Laceration;
                lblTrapped.Visible = bc.Trapped;
                lblStunned.Visible = bc.Stunned;
                lblSilenced.Visible = bc.Silenced;
                lblBlinded.Visible = bc.Blinded;
            }
            histCard = bc;
        }
    }
}
