﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.util;
using System.Threading;

namespace ElementalKingdoms.ui
{
    public partial class HandViewer : UserControl
    {
        PictureBox[] pbsHandCards;
        Label[] lblsWaitTime;
        BattleCardInHand[] state;

        List<BattleCardInHand> hand;

        public HandViewer()
        {
            InitializeComponent();

            pbsHandCards = new PictureBox[] { pbHand1, pbHand2, pbHand3, pbHand4, pbHand5 };
            lblsWaitTime = new Label[] { lblWait1, lblWait2, lblWait3, lblWait4, lblWait5 };
            state = new BattleCardInHand[pbsHandCards.Length];
            if (pbsHandCards.Length != lblsWaitTime.Length)
            {
                throw new Exception("ERROR! Number of pictureboxes is different then the number of wait time labels!");
            }
            hand = null;
        }

        public void SetHand(List<core.history.BattleCard> cards)
        {
            hand = new List<BattleCardInHand>();
            foreach (var card in cards.OrderBy(x => x.LocationIndex))
            {
                BattleCardInHand bc = new BattleCardInHand();
                bc.Position = card.LocationIndex;
                bc.CurrentWaitTime = card.CurrentWaitTime;
                bc.Name = card.Name;
                bc.CardId = card.Card.CardId;

                hand.Add(bc);
            }
            Refresh();
        }

        public void SetHand(List<BattleCard> cards)
        {
            hand = new List<BattleCardInHand>();
            int i = 0;
            foreach (var card in cards)
            {
                BattleCardInHand bc = new BattleCardInHand();
                bc.Position = i++;
                bc.CurrentWaitTime = card.CurrentWaitTime;
                bc.Name = card.Name;
                bc.CardId = card.Card.CardId;

                hand.Add(bc);
            }
            Refresh();
        }

        public override void Refresh()
        {            
            for (int i = 0; i < pbsHandCards.Length; i++)
            {
                if (hand.Count <= i)
                {
                    lblsWaitTime[i].Visible = false;
                    pbsHandCards[i].Image = null;
                    state[i] = null;
                }
                else
                {
                    BattleCardInHand c = hand[i];
                    lblsWaitTime[i].Text = c.CurrentWaitTime.ToString();
                    lblsWaitTime[i].Visible = true;

                    if (state[i] != c)
                    {
                        ThreadPool.QueueUserWorkItem(
                            delegate(object param)
                            {
                                BattleCardInHand td = (BattleCardInHand)param;
                                pbsHandCards[td.Position].Image = ImageLoader.GetSmallImageByName(td.Name);
                                state[td.Position] = td;
                            }
                            , c
                        );
                    }
                }
            }

            base.Refresh();
        }

        class BattleCardInHand
        {
            public int CardId;
            public int CurrentWaitTime;
            public string Name;
            public int Position;
        }
    }
}
