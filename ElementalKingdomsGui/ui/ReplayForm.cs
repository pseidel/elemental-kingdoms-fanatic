﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core.history;

namespace ElementalKingdoms.ui
{
    public partial class ReplayForm : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Battle b;

        private bool restartWorker;

        public ReplayForm()
        {
            InitializeComponent();
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.DragDrop += new DragEventHandler(Form1_DragDrop);

            replayFolderViewer1.ReplaySelectionChanged += replayFolderViewer1_ReplaySelectionChanged;

            lbAttackerDeck.DisplayMember = "Name";
            lbAttackerCemetary.DisplayMember = "Name";
            lbDefenderDeck.DisplayMember = "Name";
            lbDefenderCemetary.DisplayMember = "Name";
            lblDropFileHint.Visible = false;
        }

        void replayFolderViewer1_ReplaySelectionChanged(object sender, ElementalKingdomsGui.ui.replay.ReplayFolderViewer.ReplaySelectionChangedEventArgs e)
        {
            b = e.Battle;
            loadBattle(e.Battle);
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            textBox1.Clear();
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            ElementalKingdoms.util.JsonLoader l = new util.JsonLoader();
            bool first = true;
            foreach (string file in files)
            {
                Console.WriteLine(file);

                //string battleJsonPath = @"D:\projects\Games\ElementalKingdoms\replies\20\EditUserMapStages.json";
                //string battleJsonPath = @"D:\projects\Games\ElementalKingdoms\replies\22\Battle.json";
                //string battleJsonPath = @"D:\projects\Games\ElementalKingdoms\replies\23\Battle.json";
                //string battleJsonPath = @"D:\projects\Games\ElementalKingdoms\replies\Ranked battle\04 Fight.json";
                //string battleJsonPath = @"D:\projects\Games\ElementalKingdoms\replies\Battles\01.json";
                //string battleJsonPath = @"D:\projects\Games\ElementalKingdoms\replies\Battles\di_mars_01.json";
                //string battleJsonPath = @"D:\projects\Games\ElementalKingdoms\replies\Battles\di_mars_02.json";
                b = util.JsonLoader.LoadBattle(file);
                if (first)
                {
                    loadBattle(b);
                    first = false;
                }
                textBox1.AppendText(file);
                textBox1.AppendText(Environment.NewLine);
                textBox1.AppendText(b.GetLog());
                textBox1.AppendText(Environment.NewLine);
                break;// just 1 for now.
            }
        }

        public void SetBattle(core.history.Battle battle)
        {
            b = battle;
            loadBattle(b);
        }

        private void loadBattle(core.history.Battle b)
        {
            lblAttackerName.Text = b.AttackPlayer.NickName;
            lblAttackerHP.Text = b.AttackPlayer.HP;

            lblDefenderName.Text = b.DefendPlayer.NickName;
            lblDefenderHP.Text = b.DefendPlayer.HP;
            
            tbRounds.Maximum = 1;
            lblRounds.Text = "0 / " + b.Rounds.Count.ToString();

            showBattleState(b.GetState(1));

            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
                restartWorker = true;
            }
            else
            {
                restartWorker = false;
                backgroundWorker1.RunWorkerAsync(b);
            }
        }

        private void showBattleState(BattleState round)
        {
            if (log.IsDebugEnabled) log.DebugFormat("Showing round {0}", round.Round);
            
            if (tbRounds.Value != round.Round)
            {
                tbRounds.Value = round.Round;
            }

            if (round.IsAttack)
            {

                lblAttackerName.Font = new Font(lblAttackerName.Font, FontStyle.Bold);
                lblDefenderName.Font = new Font(lblDefenderName.Font, FontStyle.Regular);
            }
            else
            {
                lblAttackerName.Font = new Font(lblAttackerName.Font, FontStyle.Regular);
                lblDefenderName.Font = new Font(lblDefenderName.Font, FontStyle.Bold);
            }

            lblRound.Text = round.Round.ToString();
            
            lblAttackerHP.Text = round.Attacker.HP.ToString();
            lblDefenderHP.Text = round.Defender.HP.ToString();

            lbAttackerDeck.DataSource = round.Attacker.Cards.FindAll(x => x.Location == core.BattleLocation.Deck);
            handViewerAttacker.SetHand(round.Attacker.Cards.FindAll(x => x.Location == core.BattleLocation.Hand));
            battleFieldAttacker.SetBattlefield(round.Attacker.Cards.FindAll(x => x.Location == core.BattleLocation.Battlefield));
            lbAttackerCemetary.DataSource = round.Attacker.Cards.FindAll(x => x.Location == core.BattleLocation.Cemetary);

            lbDefenderDeck.DataSource = round.Defender.Cards.FindAll(x => x.Location == core.BattleLocation.Deck);
            handViewerDefender.SetHand(round.Defender.Cards.FindAll(x => x.Location == core.BattleLocation.Hand));
            battleFieldDefender.SetBattlefield(round.Defender.Cards.FindAll(x => x.Location == core.BattleLocation.Battlefield));
            lbDefenderCemetary.DataSource = round.Defender.Cards.FindAll(x => x.Location == core.BattleLocation.Cemetary);

            tbOps.Text = String.Join(Environment.NewLine, round.Ops);
        }

        private void tbRounds_Scroll(object sender, EventArgs e)
        {
            if (b != null)
            {
                showBattleState(b.GetState(tbRounds.Value));
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            Battle b = e.Argument as Battle;

            int numRounds = b.Rounds.Count;

            foreach (var round in b.Rounds)
            {
                if (backgroundWorker1.CancellationPending)
                {
                    break;
                }
                b.GetState(round.Number);
                backgroundWorker1.ReportProgress(round.Number * 100 / numRounds, round.Number);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int currentRound = (int)e.UserState;
            int maxRounds = b.Rounds.Count;
            if (log.IsDebugEnabled) log.DebugFormat("{0}% : Loaded round {1} of {2}", e.ProgressPercentage, currentRound, maxRounds);

            tbRounds.Maximum = currentRound;
            if (currentRound < b.Rounds.Count)
            {
                lblRounds.Text = string.Format("Loading round {0} / {1}", currentRound, maxRounds);
            }
            else
            {
                lblRounds.Text = maxRounds.ToString();
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (restartWorker)
            {
                restartWorker = false;
                backgroundWorker1.RunWorkerAsync(b);
            }
        }
    }
}
