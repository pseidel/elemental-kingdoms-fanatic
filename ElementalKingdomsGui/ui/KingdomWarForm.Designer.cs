﻿namespace ElementalKingdomsGui.ui
{
    partial class KingdomWarForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KingdomWarForm));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblMountain = new System.Windows.Forms.Label();
            this.lblTundra = new System.Windows.Forms.Label();
            this.lblForest = new System.Windows.Forms.Label();
            this.lblSwamp = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(344, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(494, 463);
            this.textBox1.TabIndex = 0;
            // 
            // lblMountain
            // 
            this.lblMountain.AutoSize = true;
            this.lblMountain.Location = new System.Drawing.Point(24, 26);
            this.lblMountain.Name = "lblMountain";
            this.lblMountain.Size = new System.Drawing.Size(46, 17);
            this.lblMountain.TabIndex = 1;
            this.lblMountain.Text = "label1";
            // 
            // lblTundra
            // 
            this.lblTundra.AutoSize = true;
            this.lblTundra.Location = new System.Drawing.Point(109, 26);
            this.lblTundra.Name = "lblTundra";
            this.lblTundra.Size = new System.Drawing.Size(46, 17);
            this.lblTundra.TabIndex = 2;
            this.lblTundra.Text = "label2";
            // 
            // lblForest
            // 
            this.lblForest.AutoSize = true;
            this.lblForest.Location = new System.Drawing.Point(24, 63);
            this.lblForest.Name = "lblForest";
            this.lblForest.Size = new System.Drawing.Size(46, 17);
            this.lblForest.TabIndex = 3;
            this.lblForest.Text = "label3";
            // 
            // lblSwamp
            // 
            this.lblSwamp.AutoSize = true;
            this.lblSwamp.Location = new System.Drawing.Point(109, 63);
            this.lblSwamp.Name = "lblSwamp";
            this.lblSwamp.Size = new System.Drawing.Size(46, 17);
            this.lblSwamp.TabIndex = 4;
            this.lblSwamp.Text = "label4";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(27, 163);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 196);
            this.listBox1.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Available targets";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // KingdomWarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 487);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.lblSwamp);
            this.Controls.Add(this.lblForest);
            this.Controls.Add(this.lblTundra);
            this.Controls.Add(this.lblMountain);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KingdomWarForm";
            this.Text = "Kingdom War";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblMountain;
        private System.Windows.Forms.Label lblTundra;
        private System.Windows.Forms.Label lblForest;
        private System.Windows.Forms.Label lblSwamp;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label5;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}