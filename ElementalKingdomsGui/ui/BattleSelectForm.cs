﻿using ElementalKingdoms;
using ElementalKingdoms.core;
using ElementalKingdoms.tools;
using ElementalKingdoms.ui;
using ElementalKingdoms.util;
using ElementalKingdomsGui.Properties;
using ElementalKingdomsGui.util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElementalKingdomsGui.ui
{
    public partial class BattleSelectForm : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string CUSTOM_DECK_FILE = Path.Combine(Settings.Default.DataPath, "custom_decks.json");

        private BattleFormOptionsDialog settingsWindow;
        private LeagueForm leagueWindow;
        private LogWindow logWindow;
        private ResultsForm resultsForm;

        private User loggedInUser;
        
        private User attacker;
        private User defender;
        private User editPlayer = null;

        private Dictionary<string, User> customDecks;

        public BattleSelectForm()
        {
            InitializeComponent();


            settingsWindow = new BattleFormOptionsDialog();
            logWindow = new LogWindow();

            ((log4net.Repository.Hierarchy.Hierarchy)log4net.LogManager.GetRepository()).Root.AddAppender(logWindow);

            offToolStripMenuItem.Tag = log4net.Core.Level.Off;
            errorsToolStripMenuItem.Tag = log4net.Core.Level.Error;
            warningsToolStripMenuItem.Tag = log4net.Core.Level.Warn;
            infoToolStripMenuItem.Tag = log4net.Core.Level.Info;
            debugToolStripMenuItem.Tag = log4net.Core.Level.Debug;

            deckEditor1.DeckChanged += deckEditor1_DeckChanged;

            AddMapStagesMenu();
            AddDemonMenu();
            AddHydraMenu();
            AddLeagueMenu();
            AddElementalWarMenu();

            tbResults.Text = "";
            chart1.Series.Clear();

            toolTip1.SetToolTip(rbDefault, "Default - play against the bottom deck until a player runs out of life or cards.\r\nSuitable for arena, maps, thieves, demons, elemental war.\r\nDisplays chance to win, or damage done (for demons)");
            toolTip1.SetToolTip(rbKingdomWar, "Kingdom war - play against predefined decks until you lose. Damage done to your hero and cards is carried over to the next fight.\r\nSuitable for kingdom war.\r\nDisplays number of fights until you lose.");

            if (File.Exists(CUSTOM_DECK_FILE))
            {
                var data = File.ReadAllText(CUSTOM_DECK_FILE);

                // Check if the data file was an old file (containing only Decks)
                // Do this by checking if the AllDecks array is present, this is an element of the User object
                if (data.IndexOf("\"AllDecks\": [") == -1)
                {
                    //It was an old deck file which had decks saved instead of users; try to load it like it was a deck and wrap it in a user
                    customDecks = new Dictionary<string, User>();

                    try
                    {
                        Dictionary<string, Deck> decks = JObject.Parse(data).ToObject<Dictionary<string, Deck>>();

                        foreach (var item in decks)
                        {
                            User u = new User() { Level = Settings.Default.playerLevel, NickName = item.Key };
                            u.AllDecks.Add(item.Value);
                            customDecks.Add(item.Key, u);
                        }
                    }
                    catch (JsonException ex)
                    {
                        log.Warn("Cough, Jason", ex);
                    }
                    // Save the new file after conversion
                    WriteCustomDecks();
                }
                else
                {
                    try
                    {
                        customDecks = JObject.Parse(data).ToObject<Dictionary<string, User>>();
                    }
                    catch (JsonException ex)
                    {
                        log.Warn("Cough, Jason", ex);
                    }
                }
                lbCustomDecks.Items.AddRange(customDecks.Keys.ToArray());
            }
            else
            {
                customDecks = new Dictionary<string, User>();
            }
            
            RecentData recentData = ElementalKingdomsGui.util.JsonLoader.LoadRecentData();
            attacker = recentData.Attacker;
            defender = recentData.Defender;
            SetLoggedInUser(recentData.LoggedInUser);

            //User attacker = g.User;
            if (attacker == null)
            {
                //attacker = new User();
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Lava Destroyer"), Level = 10 });
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Lava Destroyer"), Level = 10 });
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Treant Healer"), Level = 10 });
                attacker = new User();
                attacker.Level = Settings.Default.playerLevel;
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Wood Elf Archer"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Wood Elf Archer"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Wood Elf Archer"), Level = 10 });

                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Lore"), Level = 4 });
                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Revival"), Level = 4 });
                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Leaf"), Level = 4 });
                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Fire Forge"), Level = 4 });
                //[FH]Lethal
                /*
                attacker.Level = 81;

                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Frost Bite"), Level = 4 });
                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Arctic Freeze"), Level = 4 });
                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Clear Spring"), Level = 4 });
                attacker.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Permafrost"), Level = 4 });

                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 0, Level = 10 }); //

                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Jack Frost"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Jack Frost"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Ice Dragon"), Level = 13, SkillNew = 191 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Taiga Cleric"), Level = 15, SkillNew = 688 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Taiga General"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Taiga General"), Level = 15, SkillNew = 688 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Ancient One"), Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Ancient One"), Level = 15, SkillNew= 688 });
                */
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Headless Horseman"), Level = 10 });

                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 166, Level = 10 }); // Crystal Emperor
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 162, Level = 10 }); // Bite: Fallen Angel
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 310, Level = 10 }); // Feast of Blood, Reincarnation: Celestial Touchstone
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 47, Level = 10 }); // Fireball, Firewall: Barbarian Shaman
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 24, Level = 10 }); // Ice Bolt, Blizzard: Water Elemental
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 170, Level = 10 }); // Thunderbolt, Chain Lightning, Forest Force: Harpy
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 42, Level = 10 }); // Healing, Regeneration: Arctic Priest
                // TODO: Need new json for this card...
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 0, Level = 10 }); // Healing mist, dexterity: Moon Ranger

                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 59, Level = 10 }); // Magic Shield, Exile, Regeneration: Taiga General
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 173, Level = 10 }); // Resistance: Armored Sumatran
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 178, Level = 10 }); // Sacrifice, Infiltrator, Resistance: Northern Gladiator
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 261, Level = 10 }); // Marsh Barrier: Orc Commander
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 171, Level = 10 }); // counterattack, rejuvenation, retal: Cacti Guardian
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 34, Level = 10 }); // trap: Tundra Hunter
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 304, Level = 10 }); // trap: Jack Frost
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 9002, Level = 10 }); // Devil's Curse: Mars
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 140, Level = 10 }); // Curse / Seal: Azula
                /*
                 * Before damage skillz
                 */
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 198, UserCardId = 0, Level = 5 }); // Arctic Polution / Swamp Purity: Enticing Elpaca
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 163, UserCardId = 0, Level = 5 }); // Forest Fire: Giant Mollusc
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 147, UserCardId = 0, Level = 5 }); // Mountain Glacier: Arctic Hellhound

                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 228, Level = 10 }); // backstab, vendetta, warpath: Rogue Knight

                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 58, Level = 10 }); // concentration, backstab, dodge: pbb
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 106, Level = 10 }); // backstab, concentration, dodge: hh

                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 129, Level = 0 }); // Venom: Goblin Archer
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 169, Level = 10 }); // blitz (+ status effect): Lord of the Mire
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 191, Level = 10 }); // Toxic Clouds: Omniscient Dragon
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 292, Level = 10 }); // hot chase: Flame Empress

                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 120, Level = 0 }); // Clean Sweep: DST
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 188, Level = 5 }); // Chain Attack: Moor Ripper


                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 175, Level = 10 }); // Slayer : Ice Knight



                /*
                 * After damage skillz */
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 69, UserCardId = 0, Level = 0 }); // Bloodsucker: Vulture
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 35, UserCardId = 1, Level = 0 }); // Weaken: Ice elemental
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 187, UserCardId = 3, Level = 0 }); // Blight: Wild Duron
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 128, UserCardId = 4, Level = 5 }); // Laceration: Latite Golem (no-one with this as first skill)
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 305, UserCardId = 2, Level = 0 }); // Bloodthirsty: Christmas Treant
                //attacker.UserDeck.Cards.Add(new UserCard() { CardId = 32, UserCardId = 5, Level = 0 }); // Puncture: Tundra Barbarian

                /*
                 * Snipers: Winged Tiger, Wood Elf Archer, Sea King
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = 174, UserCardId = 0, Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = 9, UserCardId = 1, Level = 10 });
                attacker.UserDeck.Cards.Add(new UserCard() { CardId = 9005, UserCardId = 2, Level = 10 });
                 */
            }

            if (defender == null)
            {
                defender = new User();
                defender.Level = 0;
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Pandarus"), Level = 10 });

                //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Fire-Cursed Treant"), Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Fire-Cursed Treant"), Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Fire-Cursed Treant"), Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Fire-Cursed Treant"), Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Fire-Cursed Treant"), Level = 10 });
                //MCGoonSquad
                /*
                defender.Level = 90;

                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Frost Bite"), Level = 4 });
                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Permafrost"), Level = 4 });
                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Arctic Freeze"), Level = 4 });
                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Clear Spring"), Level = 4 });

                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Ice Dragon"), Level = 15, SkillNew = 102 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Ancient One"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Ancient One"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Ancient One"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Jack Frost"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Jack Frost"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Sea Piercer"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Dharmanian"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Dharmanian"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Dharmanian"), Level = 10 });
                */
                //rebecca000
                /*
                defender.Level = 81;

                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Spring Breeze"), Level = 4 });
                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Burning Soul"), Level = 4 });
                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Ghost Step"), Level = 4 });
                defender.UserDeck.Runes.Add(new UserRune() { RuneId = Rune.GetIdFromName("Thunder Shield"), Level = 4 });

                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Taiga General"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Taiga General"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Aranyani"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Treant Healer"), Level = 10, SkillNew = 688 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Dire Snapping Turtle"), Level = 10, SkillNew = 751 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Flame Empress"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Fire Kirin"), Level = 10, SkillNew = 241 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Moss Dragon"), Level = 10, SkillNew = 81 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Azula"), Level = 10 });
                defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Dire Snapping Turtle"), Level = 10 });
                */

                //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Lava Destroyer"), Level = 5 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Mars"), Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 9002, Level = 15 }); // Mars
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 59, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 59, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 59, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 173, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 173, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 173, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 119, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 119, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 119, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 42, Level = 10 }); // Arctic Priest
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 8030, UserCardId = 2, Level = 0 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 8030, UserCardId = 2, Level = 0 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 8030, UserCardId = 2, Level = 0 });

                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 8030, UserCardId = 0, Level = 10 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 8030, UserCardId = 1, Level = 5 });
                //defender.UserDeck.Cards.Add(new UserCard() { CardId = 8030, UserCardId = 2, Level = 0 });
            }
            deckEditor1.SetUser(attacker);
            deckEditor2.SetUser(defender);


            if (Game.TheGame.KingdomWarDecks.Count() == 0)
            {
                rbKingdomWar.Enabled = false;
                toolTip1.SetToolTip(rbKingdomWar, "Could not find any Kingdom War decks!");
            }
        }
		
        void ResetBattle()
        {
            //btnAuto.Text = "Auto";
            //timerAutoBattle.Enabled = false;
            //lblAttackerHP.DataBindings.Clear();
            //lblDefenderHP.DataBindings.Clear();
            //battle = InitializeBattle();
            //DataBind();
            //RefreshDatabindings();

            //lblQuickStats.Text = "";

            //MultiBattleExecutor battles = new MultiBattleExecutor(attacker, defender);
            //MultiBattleExecutor.Result r = battles.Execute(20);

            //lblQuickStats.Text = r.ToString();
        }

        private void deckEditor1_DeckChanged(object sender, EventArgs e)
        {
            editPlayer = attacker;
            if (log.IsDebugEnabled) log.DebugFormat("Deck 1 changed.");
            if (attacker == null) { log.Error("Editing finished, but no edit player found."); }

            attacker.Level = deckEditor1.User.Level;
            attacker.UserDeck = deckEditor1.Deck;
            if (log.IsDebugEnabled) log.DebugFormat("Current number of decks: {0}", attacker.AllDecks.Count);

            if (qeTop != null)
            {
                qeTop.SetUser(attacker);
            }
            ResetBattle();
        }
		
        private void deckEditor2_DeckChanged(object sender, EventArgs e)
        {
            editPlayer = defender;
            if (log.IsDebugEnabled) log.DebugFormat("Deck 2 changed.");
            if (defender == null) { log.Error("Editing finished, but no edit player found."); }
        
            defender.Level = deckEditor2.User.Level;
            defender.UserDeck = deckEditor2.Deck;
            if (log.IsDebugEnabled) log.DebugFormat("Current number of decks: {0}", defender.AllDecks.Count);

            if (qeBottom != null)
            {
                qeBottom.SetUser(defender);
            }
            ResetBattle();
        }
		
        private void AddDemonMenu()
        {
            foreach (var demon in Card.Cards.FindAll(x => x.Race == Race.Demon))
            {
                var demonItem = new ToolStripMenuItem()
                {
                    Text = demon.Name,
                    Tag = demon
                };
                demonItem.Click += demonToolStripMenuItem_Click;
                demonToolStripMenuItem.DropDownItems.Add(demonItem);
            }
        }

        void demonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            Card demon = item.Tag as Card;

            User defender = new User();
            defender.Level = 0;
            defender.UserDeck.Cards.Add(new UserCard() { CardId = demon.Id, Level = 15 });

            SetDefender(defender);

            if (log.IsDebugEnabled) log.DebugFormat("Selected demon {0}", demon.Name);
            ResetBattle();
        }
		
        void AddHydraMenu()
        {
            hydraToolStripMenuItem.DropDownItems.Clear();

            hydraToolStripMenuItem.Enabled = Game.TheGame.HydraDecks.Count > 0;

            foreach (var user in Game.TheGame.HydraDecks)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Found hydra {0}", user.NickName);

                var hydraToolStripMenuItem = new ToolStripMenuItem()
                {
                    Text = user.NickName,
                    Tag = user
                };

                hydraToolStripMenuItem.Click += hydraToolStripMenuItem_Click;
                this.hydraToolStripMenuItem.DropDownItems.Add(hydraToolStripMenuItem);
            }
        }

        private void hydraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            User user = item.Tag as User;

            SetDefender(user);

            if (log.IsDebugEnabled) log.DebugFormat("Selected Hydra opponent {0}", user.NickName);
            ResetBattle();
        }

        private void AddMapStagesMenu()
        {
            mapToolStripMenuItem.DropDownItems.Clear();
            foreach (var mapStage in MapStage.AllMapStages)
            {
                var mapStageToolStripMenuItem = new ToolStripMenuItem()
                {
                    Text = mapStage.Id.ToString(),
                };

                mapToolStripMenuItem.DropDownItems.Add(mapStageToolStripMenuItem);

                foreach (var mapStageDetail in mapStage.MapStageDetails.OrderBy(x => x.Rank))
                {
                    if (mapStageDetail.Levels == null)
                    {
                        // Tower in 2-8, unimplemented levels in 9-11, unimplemented towers in 12-14
                        continue;
                    }
                    var mapStageDetailsToolStripMenuItem = new ToolStripMenuItem()
                    {
                        Text = String.Format("{0}-{1}  {2}", mapStage.Id, mapStageDetail.Rank + 1, mapStageDetail.Name)
                    };

                    mapStageToolStripMenuItem.DropDownItems.Add(mapStageDetailsToolStripMenuItem);

                    foreach (var level in mapStageDetail.Levels)
                    {
                        string difficulty = "???";
                        if (level.Level == 1)
                        {
                            difficulty = "Easy";
                        }
                        else if (level.Level == 2)
                        {
                            difficulty = "Medium";
                        }
                        else if (level.Level == 3)
                        {
                            difficulty = "Hard";
                        }
                        var mapStageLevelToolStripMenuItem = new ToolStripMenuItem()
                        {
                            Text = String.Format("{0}", difficulty),
                            Tag = level
                        };

                        mapStageLevelToolStripMenuItem.Click += mapStageLevelToolStripMenuItem_Click;
                        mapStageDetailsToolStripMenuItem.DropDownItems.Add(mapStageLevelToolStripMenuItem);
                    }
                }
            }
        }

        void mapStageLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            MapStageLevel level = item.Tag as MapStageLevel;

            User defender = new User();
            defender.Level = level.HeroLevel;
            defender.UserDeck.Cards = level.Cards;
            defender.UserDeck.Runes = level.Runes;

            SetDefender(defender);

            if (log.IsDebugEnabled) log.DebugFormat("Selected opponent from level {0} (difficulty: {1})", level.MapStageDetailId, level.Level);
            ResetBattle();
        }

        private void AddLeagueMenu()
        {
            leagueToolStripMenuItem.DropDownItems.Clear();

            var leagueFoHToolStripMenuItem = new ToolStripMenuItem()
            {
                Text = "FoH simulator"
            };
            leagueFoHToolStripMenuItem.Click += leagueToolStripMenuItem_Click;
            leagueToolStripMenuItem.DropDownItems.Add(leagueFoHToolStripMenuItem);

            leagueToolStripMenuItem.DropDownItems.Add(new ToolStripSeparator());

            if (League.CurrentLeague != null)
            {
                foreach (var result in League.CurrentLeague.RoundResult.First())
                {
                    if (log.IsDebugEnabled) log.DebugFormat("Found player {0}", result.BattleInfo.User.NickName);

                    var leagueBattlePlayerToolStripMenuItem = new ToolStripMenuItem()
                    {
                        Text = String.Format("{0}", result.BattleInfo.User.NickName),
                        Tag = result
                    };

                    leagueBattlePlayerToolStripMenuItem.Click += leagueBattlePlayerToolStripMenuItem_Click;
                    leagueToolStripMenuItem.DropDownItems.Add(leagueBattlePlayerToolStripMenuItem);
                }
            }
        }

        void leagueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (leagueWindow == null)
            {
                leagueWindow = new LeagueForm();
            }

            leagueWindow.ShowDialog();
            AddLeagueMenu(); // League might have been reloaded from this form
            AddMapStagesMenu();
        }

        void leagueBattlePlayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            League.LeagueRoundResult result = item.Tag as League.LeagueRoundResult;
            if (result == null) return;

            if (log.IsDebugEnabled) log.DebugFormat("Handling league click for player {0} with odds {1} ({2})", result.BattleInfo.User.NickName, result.BetOdds, result.BetTotal);
            DialogResult dialogResult = MessageBox.Show("Yes = defender, No = attacker, Cancel = cancel", "Defender?", MessageBoxButtons.YesNoCancel);
            User u = result.BattleInfo.User;
            User user = new User() { Level = u.Level, Avatar = u.Avatar, NickName = u.NickName, Sex = u.Sex, Uid = u.Uid };
            user.UserDeck.Name = u.NickName;
            user.UserDeck.Cards = result.BattleInfo.Cards.ToList();
            user.UserDeck.Runes = result.BattleInfo.Runes.ToList();
            if (dialogResult == DialogResult.Yes)
            {
                SetDefender(user);
            }
            else if (dialogResult == DialogResult.No)
            {
                SetAttacker(user);
            }

            if (dialogResult != DialogResult.Cancel)
            {
                ResetBattle();
            }
        }

        void AddElementalWarMenu()
        {
            elementalWarToolStripMenuItem.DropDownItems.Clear();

            foreach (var user in Game.TheGame.ElementalWarsDecks.OrderBy(x => x.NickName))
            {
                if (log.IsDebugEnabled) log.DebugFormat("Found player {0}", user.NickName);

                var ewOpponentToolStripMenuItem = new ToolStripMenuItem()
                {
                    Text = String.Format("{0}", user.NickName.Replace("EW ", "")),
                    Tag = user
                };

                ewOpponentToolStripMenuItem.Click += EwOpponentToolStripMenuItem_Click;
                elementalWarToolStripMenuItem.DropDownItems.Add(ewOpponentToolStripMenuItem);
            }

            // Also add the goblin deck 
            if (Game.TheGame.ElementalWarsDecks.Count > 0)
            {
                elementalWarToolStripMenuItem.DropDownItems.Add(new ToolStripSeparator());
            }

            try
            {

                User goblin = new User();
                goblin.Level = 0;
                goblin.NickName = "Treasure Goblin";
                goblin.UserDeck.Cards.Add(new UserCard() { CardId = Card.GetIdFromName("Treasure Goblin"), Level = 15 });
                goblin.UserDeck.Name = goblin.NickName;

                var goblinToolStripMenuItem = new ToolStripMenuItem()
                {
                    Text = goblin.NickName,
                    Tag = goblin
                };
                goblinToolStripMenuItem.Click += EwOpponentToolStripMenuItem_Click;

                elementalWarToolStripMenuItem.DropDownItems.Add(goblinToolStripMenuItem);
            }
            catch (Exception ex)
            {
                log.WarnFormat("Unable to add goblin to toolstrip: {0}", ex.Message);
            }
        }

        private void EwOpponentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            User user = item.Tag as User;

            SetDefender(user);

            if (log.IsDebugEnabled) log.DebugFormat("Selected EW opponent {0}", user.NickName);
            ResetBattle();
        }

        private void setDebugLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var itm in ((ToolStripMenuItem)sender).GetCurrentParent().Items)
            {
                ToolStripMenuItem item = itm as ToolStripMenuItem;
                if (item == null) continue;

                if (item == sender)
                {
                    item.Checked = true;
                    var logLevel = item.Tag as log4net.Core.Level;
                    Util.SetLoglevel(logLevel);
                }
                if ((item != null) && (item != sender))
                {
                    item.Checked = false;
                }
            }
        }

        private void resultsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (resultsForm == null)
            {
                resultsForm = new ResultsForm();
            }
            resultsForm.Show();
        }

        private void level100ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetThief(100, false);
        }

        private void level100LegendaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetThief(100, true);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }

        private void showLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logWindow.Show();
        }
		
        private void replayViewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new ReplayForm().Show();
        }

        private void foHSimulatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            leagueToolStripMenuItem_Click(sender, e);
        }

        private void dataLoaderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ServerConnectorForm serverConnectForm = new ServerConnectorForm();
            serverConnectForm.ShowDialog();

            SetLoggedInUser(serverConnectForm.RegisteredUser);

            // League might have been reloaded from this form
            AddLeagueMenu();
            AddMapStagesMenu();
        }

        private void SetLoggedInUser(User user)
        {
            loggedInUser = user;

            if (loggedInUser != null)
            {
                deckEditor1.SetUserCards(loggedInUser.UserCards);
                deckEditor2.SetUserCards(loggedInUser.UserCards);

                int i = 0;
                foreach (var deck in loggedInUser.AllDecks)
                {
                    string deckName = loggedInUser.NickName + "_" + i;
                    i++;
                    User u = new User()
                    {
                        NickName = loggedInUser.NickName,
                        Level = loggedInUser.Level,
                        Sex = loggedInUser.Sex,
                        Avatar = loggedInUser.Avatar,
                    };
                    u.AllDecks.Add(new Deck() { Cards = deck.Cards.ToList(), Runes = deck.Runes.ToList(), Name = deckName });
                    if (!customDecks.Remove(deckName))
                    {
                        lbCustomDecks.Items.Add(deckName);
                    }
                    customDecks.Add(deckName, u);
                }
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settingsWindow.ShowDialog();
        }


        private void SetThief(int level, bool legendary)
        {
            User defender = new User();
            defender.Level = 0;
            defender.NickName = String.Format("Level {0} [{1}thief]", level, legendary ? "legendary " : "");
            List<UserCard> thiefCards = new List<UserCard>();
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Fire Demon"), Level = 10 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Ice Dragon"), Level = 10 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Water Elemental"), Level = 10 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Water Elemental"), Level = 10 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Kitsune"), Level = 10 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Behemoth"), Level = 10 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Demonic Imp"), Level = 9 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Demonic Imp"), Level = 9 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Swamp Golem"), Level = 8 });
            thiefCards.Add(new UserCard() { CardId = Card.GetIdFromName("Polar Bearborn"), Level = 5 });
            defender.UserDeck.Cards = thiefCards;

            SetDefender(defender);

            if (log.IsDebugEnabled) log.DebugFormat("Selected opponent thief level {0} (Legendary: {1})", level, legendary);
            ResetBattle();
        }


        #region From DeckEditor

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (deckEditor1.Deck.Name.Trim().Length == 0)
            {
                MessageBox.Show("Please enter a deck name");
                return;
            }

            Deck d = deckEditor1.Deck;
            User u = new User() { Level = deckEditor1.User.Level, NickName = deckEditor1.User.NickName };
            Deck copy = new Deck() { Cards = d.Cards.ToList(), GroupId = d.GroupId, Name = d.Name, Runes = d.Runes.ToList(), Uid = d.Uid };
            u.AllDecks.Add(copy);
            if (customDecks.ContainsKey(d.Name))
            {
                // ask if you want to overwrite
                // maybe check if it is the same or different first
                // Maybe just overwrite it.
                customDecks[d.Name] = u;
            }
            else
            {
                customDecks.Add(d.Name, u);
                lbCustomDecks.Items.Add(d.Name);
            }
            WriteCustomDecks();
        }

        private void WriteCustomDecks()
        {
            string json = JsonConvert.SerializeObject(customDecks, Formatting.Indented);
            // Make a backup of an existing custom decks file
            if (File.Exists(CUSTOM_DECK_FILE))
            {
                File.Move(CUSTOM_DECK_FILE, Path.Combine(Settings.Default.DataPath, String.Format("custom_decks_backup_{0}.json", Util.GetFilenameFriendlyCurrentTime())));
            }

            File.WriteAllText(CUSTOM_DECK_FILE, json);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (lbCustomDecks.SelectedItem != null)
            {
                User u = customDecks[lbCustomDecks.SelectedItem.ToString()];
                SetAttacker(u);
            }
        }


        private void btnSaveBottom_Click(object sender, EventArgs e)
        {
            if (deckEditor2.Deck.Name.Trim().Length == 0)
            {
                MessageBox.Show("Please enter a deck name");
                return;
            }

            Deck d = deckEditor2.Deck;
            User u = new User() { Level = deckEditor2.User.Level, NickName = deckEditor2.User.NickName };
            Deck copy = new Deck() { Cards = d.Cards.ToList(), GroupId = d.GroupId, Name = d.Name, Runes = d.Runes.ToList(), Uid = d.Uid };
            u.AllDecks.Add(copy);
            if (customDecks.ContainsKey(d.Name))
            {
                // ask if you want to overwrite
                // maybe check if it is the same or different first
                // Maybe just overwrite it.
                customDecks[d.Name] = u;
            }
            else
            {
                customDecks.Add(d.Name, u);
                lbCustomDecks.Items.Add(d.Name);
            }
            WriteCustomDecks();
        }

        private void btnLoadBottom_Click(object sender, EventArgs e)
        {
            if (lbCustomDecks.SelectedItem != null)
            {
                User u = customDecks[lbCustomDecks.SelectedItem.ToString()];
                SetDefender(u);
            }
        }

        private void lbDecks_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSelectedDeck();
            }
        }

        private void RemoveSelectedDeck()
        {
            if (lbCustomDecks.SelectedItem != null)
            {
                int index = lbCustomDecks.SelectedIndex;
                var item = lbCustomDecks.SelectedItem;
                if (customDecks.Remove(item.ToString()))
                {
                    lbCustomDecks.Items.Remove(item);
                    WriteCustomDecks();
                }
                lbCustomDecks.SelectedIndex = Math.Min(index, lbCustomDecks.Items.Count - 1);
            }
        }
        #endregion



        private void btnMultiBattle_Click(object sender, EventArgs e)
        {
            if (rbDefault.Checked)
            {
                ExecuteDefaultBattle();
            }
            else if (rbKingdomWar.Checked)
            {
                ExecuteKingdomWar();
            }
        }

        private void btnClearResults_Click(object sender, EventArgs e)
        {
            MultiBattleExecutor.Results.Clear();
            tbResults.Text = "";
            chart1.Series.Clear();
            btnBadReplay.Enabled = false;
            btnGoodReplay.Enabled = false;
        }

        private void ExecuteDefaultBattle()
        {
            btnMultiBattle.Enabled = false;
            MultiBattleExecutor battles = new MultiBattleExecutor(attacker, defender);
            backgroundWorker1.RunWorkerAsync(battles);
        }

        private void ExecuteKingdomWar()
        {
            var defenders = Game.TheGame.KingdomWarDecks.ToArray();
            MultiBattleExecutor battles = new MultiBattleExecutor(attacker, defenders);
            battles.Mode = BattleMode.KingdomWars;
            BattleResult r = battles.Execute(Settings.Default.numberOfSimulations);
            tbResults.AppendText(r.ToString());
            tbResults.AppendText(Environment.NewLine);
            tbResults.AppendText(Environment.NewLine);
            btnBadReplay.Enabled = false;
            btnGoodReplay.Enabled = false;

            List<int> ceilings = new List<int>();
            const int NUM_DATA_POINTS = 30;
            for (int i = 1; i <= NUM_DATA_POINTS; i++)
            {
                ceilings.Add(r.MaxFightsPerLive * i / NUM_DATA_POINTS);
            }
            var g = r.FightsPerLive.GroupBy(item => ceilings.First(ceiling => ceiling >= item));

            //var g = r.FightsPerLive.GroupBy(i => i);
            Dictionary<int, int> bla = new Dictionary<int, int>();
            foreach (var grp in g)
            {
                bla.Add(grp.Key, grp.Count());
                Console.WriteLine("{0} {1}", grp.Key, grp.Count());
            }

            chart1.Series.Clear();
            chart1.Series.Add("Fights per life").Points.DataBindXY(bla.Keys, bla.Values);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            SetAttacker(new User() { Level = Settings.Default.playerLevel });
        }

        private void btnClearBottom_Click(object sender, EventArgs e)
        {
            SetDefender(new User() { Level = Settings.Default.playerLevel });
        }

        private void SetAttacker(User a)
        {
            if (qeTop != null) { qeTop.SetUser(a); }
            SetPlayer(a, true);
        }

        private void SetDefender(User d)
        {
            if (qeBottom != null) { qeBottom.SetUser(d); }
            SetPlayer(d, false);
        }

        private void SetPlayer(User user, bool isAttacker)
        {
            // Copy player to ensure that the default decks cannot be edited in memory
            User player = new User();
            player.Level = user.Level;
            player.NickName = user.NickName;
            player.Avatar = user.Avatar;
            player.UserDeck.Name = user.UserDeck.Name;
            player.UserDeck.Cards = user.UserDeck.Cards.ToList();
            player.UserDeck.Runes = user.UserDeck.Runes.ToList();

            if (isAttacker)
            {
                deckEditor1.SetUser(player);
                attacker = player;
            }
            else
            {
                deckEditor2.SetUser(player);
                defender = player;
            }
        }

        private void BattleSelectForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            util.JsonLoader.SaveRecentData(new RecentData() { Attacker = attacker, Defender = defender, LoggedInUser = loggedInUser });
            util.JsonLoader.SaveFavouriteCards(DeckEditor.FavouriteCards);
        }

        private void btnSingleFight_Click(object sender, EventArgs e)
        {
            BattleForm b = new BattleForm(attacker, defender);
            b.Show();
        }

        private void rbDefault_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbKingdomWar_CheckedChanged(object sender, EventArgs e)
        {
            deckEditor2.Visible = !rbKingdomWar.Checked;
            btnClearBottom.Visible = !rbKingdomWar.Checked;
            btnLoadBottom.Visible = !rbKingdomWar.Checked;
            btnSaveBottom.Visible = !rbKingdomWar.Checked;
            btnSingleFight.Visible = !rbKingdomWar.Checked;
        }

        private void btnImportDeck_Click(object sender, EventArgs e)
        {
            DeckImporter di = new DeckImporter();
            di.ShowDialog();

            foreach (var u in di.Users)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Adding imported user {0}", u.NickName);
                var name = u.NickName;
                int i = 0;
                while (customDecks.ContainsKey(name))
                {
                    name = String.Format("{0}_{1}", u.NickName, i);
                }
                u.NickName = name;
                u.UserDeck.Name = name;
                customDecks.Add(name, u);
                lbCustomDecks.Items.Add(name);
            }
            WriteCustomDecks();
        }

        private void btnDeleteDeck_Click(object sender, EventArgs e)
        {
            RemoveSelectedDeck();
        }

        private void btnExportDeck_Click(object sender, EventArgs e)
        {
            DeckExporter de = new DeckExporter(customDecks);
            de.ShowDialog();
        }

        private void deckEditor1_MouseMove(object sender, MouseEventArgs e)
        {
            deckEditor1.BackColor = Color.Honeydew;
            deckEditor2.BackColor = System.Drawing.SystemColors.Control;
        }

        private void deckEditor2_MouseMove(object sender, MouseEventArgs e)
        {
            deckEditor1.BackColor = System.Drawing.SystemColors.Control;
            deckEditor2.BackColor = Color.Honeydew;
        }

        QuickEdit qeTop;
        QuickEdit qeBottom;

        private void btnQuickEdit_Click(object sender, EventArgs e)
        {
            if (qeTop == null)
            {
                qeTop = new QuickEdit();
                qeTop.DeckChanged += QeTop_DeckChanged;
            }
            qeTop.SetUser(attacker);
            qeTop.Show();
        }

        private void QeTop_DeckChanged(User u)
        {
            deckEditor1.SetUser(u);
            attacker = u;
        }

        private void btnQuickEditBottom_Click(object sender, EventArgs e)
        {
            if (qeBottom == null)
            {
                qeBottom = new QuickEdit();
                qeBottom.DeckChanged += QeBottom_DeckChanged;
            }
            qeBottom.SetUser(defender);
            qeBottom.Show();
        }

        private void QeBottom_DeckChanged(User u)
        {
            deckEditor2.SetUser(u);
            defender = u;
        }

        private void btnReplay_Click(object sender, EventArgs e)
        {
            Control c = sender as Control;

            ElementalKingdoms.core.history.Battle replay = c.Tag as ElementalKingdoms.core.history.Battle;

            if (replay != null)
            {
                var form = new ReplayForm();
                form.SetBattle(replay);
                form.Show();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            MultiBattleExecutor battles = e.Argument as MultiBattleExecutor;
            battles.ProgressChanged += Battles_ProgressChanged;
            BattleResult r = battles.Execute(Settings.Default.numberOfSimulations);
            e.Result = r;
        }

        private void Battles_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            backgroundWorker1.ReportProgress(e.ProgressPercentage);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BattleResult r = e.Result as BattleResult;
            tbResults.AppendText(r.ToString());
            tbResults.AppendText(Environment.NewLine);
            tbResults.AppendText(Environment.NewLine);
            chart1.Series.Clear();

            btnGoodReplay.Enabled = false;
            btnBadReplay.Enabled = false;
            if (r.HistoricBattles.Count > 0)
            {
                btnGoodReplay.Tag = r.HistoricBattles[0];
                btnGoodReplay.Enabled = true;
            }

            if (r.HistoricBattles.Count > 1)
            {
                btnBadReplay.Tag = r.HistoricBattles[1];
                btnBadReplay.Enabled = true;
            }

            if (r.SumMerit > 0)
            {
                List<int> ceilings = new List<int>();
                const int NUM_DATA_POINTS = 30;
                for (int i = 1; i <= NUM_DATA_POINTS; i++)
                {
                    ceilings.Add(r.MaxMerit * i / NUM_DATA_POINTS);
                }
                var g = r.Merit.GroupBy(item => ceilings.First(ceiling => ceiling >= item));

                //var g = r.Merit.GroupBy(i => i);
                Dictionary<int, int> bla = new Dictionary<int, int>();
                foreach (var grp in g)
                {
                    bla.Add(grp.Key, grp.Count());
                    //Console.WriteLine("{0} {1}", grp.Key, grp.Count());
                }

                chart1.Series.Add("Merit").Points.DataBindXY(bla.Keys, bla.Values);
            }
            btnMultiBattle.Enabled = true;
            btnMultiBattle.Text = "Fight!";
        }
        
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Console.WriteLine("Progress changed to {0}", e.ProgressPercentage);
            btnMultiBattle.Text = String.Format("{0}%", e.ProgressPercentage);
        }

		private void BattleSelectForm_Load(object sender, EventArgs e)
		{

		}

		private void deckEditor1_Load(object sender, EventArgs e)
		{

		}
	}
}
