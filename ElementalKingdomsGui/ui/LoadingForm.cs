﻿using ElementalKingdoms.core;
using ElementalKingdoms.server;
using ElementalKingdoms.tools;
using ElementalKingdoms.util;
using ElementalKingdomsGui.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElementalKingdomsGui.ui
{
    public partial class LoadingForm : Form
    {
        bool finished = false;
        bool waitForUserInput = false;
		private ServerCommunicator com;

		public LoadingForm()
        {
            InitializeComponent();
            borderLabel1.Parent = pictureBox1;
            borderLabel2.Parent = pictureBox1;
            borderLabel3.Parent = pictureBox1;
            borderLabel4.Parent = pictureBox1;

            About about = new About();

            borderLabel3.Text = about.AssemblyVersion.Split('.')[0]+"."+about.AssemblyVersion.Split('.')[1];

            borderLabel4.Visible = false;
            tbChangeLog.Visible = false;
            btnDownload.Visible = false;
            btnSkipContinue.Visible = false;
            btnSkipContinue.Enabled = false;
        }

        private void LoadingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!finished) e.Cancel = true;
        }

        private void LoadingForm_Shown(object sender, EventArgs e)
        {
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
			//Nharzhool - disabled Internet activity.
			/*UpdateChecker c = new UpdateChecker();
            backgroundWorker1.ReportProgress(10, "Checking for updates...");
            c.CheckForUpdates();
            if (c.UpdateAvailable)
            {
                waitForUserInput = true;
                // show 'update available window'
                foreach (var update in c.UpdateLog)
                {
                    DoAppend(update.Value);
                }

                borderLabel4.Invoke((MethodInvoker)delegate { borderLabel4.Visible = true; });
                tbChangeLog.Invoke((MethodInvoker)delegate { tbChangeLog.Visible = true; });
                btnDownload.Invoke((MethodInvoker)delegate { btnDownload.Visible = true; });
                btnSkipContinue.Invoke((MethodInvoker)delegate { btnSkipContinue.Visible = true; });
            }*/

			//Nharzhool Added Server comms stuff to get new cards/skills etc.

			backgroundWorker1.ReportProgress(10, "Connecting to servers");

			try
			{
				com = ServerCommunicator.Create(Settings.Default.DeviceId, true);

				backgroundWorker1.ReportProgress(15, "Updating card data");

				com.GetAllCard();

				backgroundWorker1.ReportProgress(20, "Updating skill data");

				com.GetAllSkill();

				backgroundWorker1.ReportProgress(25, "Updating map data");

				com.GetMapStages();

				backgroundWorker1.ReportProgress(30, "Updating rune data");

				com.GetAllRunes();

				backgroundWorker1.ReportProgress(35, "Updating Field of Honor data");

				com.GetLeagueInfo();

			}
			catch { }

			backgroundWorker1.ReportProgress(45, "Loading core game data");
			Game.TheGame.Load();

            Game.TheGame.DemonInvasionMeritCardIds.AddRange(new[] { Settings.Default.diMeritCardId1, Settings.Default.diMeritCardId2 }.Where(x => x != 0));
            
            backgroundWorker1.ReportProgress(60, "Loading stored results");
            MultiBattleExecutor.Load();
            
            backgroundWorker1.ReportProgress(70, "Loading Elemental War decks");
            MitziLoader m = new MitziLoader();
            var users = m.Load(@"cache/decks_EW.txt");
            Game.TheGame.ElementalWarsDecks = users;
            Game.TheGame.ElementalWarMeritCardIds.AddRange(new[] { Settings.Default.ewMeritCardId1, Settings.Default.ewMeritCardId2 }.Where(x => x != 0));

            backgroundWorker1.ReportProgress(80, "Loading Kingdom War decks");
            var kingdomWarUsers = m.Load(@"cache/decks_KW.txt");
            Game.TheGame.KingdomWarDecks = kingdomWarUsers;

            backgroundWorker1.ReportProgress(90, "Loading Hydra decks");
            var hydraUsers = m.Load(@"cache/decks_Hydra.txt");
            Game.TheGame.HydraDecks = hydraUsers;
            Game.TheGame.HydraMeritCardIds.AddRange(new[] { Settings.Default.hydraMeritCardId1, Settings.Default.hydraMeritCardId2, Settings.Default.hydraMeritCardId3 }.Where(x => x != 0));
            
            backgroundWorker1.ReportProgress(100, "Finished!"); // not handled for some reason...
        }


        public void DoAppend(UpdateResult update)
        {
            AppendText(string.Format("------------------- {0} ({1}) ---------------\r\n\r\n{2}\r\n", update.Version, update.Date, update.ChangeLog));
        }

        delegate void SetTextCallback(string text);

        private void AppendText(string text)
        {
            if (tbChangeLog.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(AppendText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                var x = this.Handle;
                tbChangeLog.AppendText(text);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgress(e.ProgressPercentage, e.UserState.ToString());
        }

        private void UpdateProgress(int percentage, string text)
        {
            progressBar1.Value = percentage;
            borderLabel1.Text = string.Format("{0} ({1}%)", text, percentage);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateProgress(100, "Ready!");
            finished = true;
            if (!waitForUserInput)
            {
                Close();
            }
            btnSkipContinue.Enabled = true;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://peppa84.com");
        }

        private void btnSkipContinue_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
