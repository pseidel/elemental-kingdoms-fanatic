﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdomsGui.Properties;
using ElementalKingdomsGui.ui.controls;
using ElementalKingdoms.core;

namespace ElementalKingdoms.ui
{
    public partial class BattleFormOptionsDialog : Form
    {
        public BattleFormOptionsDialog()
        {
            InitializeComponent();
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            SetCardId(editHydraMeritCardButton1, Settings.Default.hydraMeritCardId1);
            SetCardId(editHydraMeritCardButton2, Settings.Default.hydraMeritCardId2);
            SetCardId(editHydraMeritCardButton3, Settings.Default.hydraMeritCardId3);

            SetCardId(editEwMeritCardButton1, Settings.Default.ewMeritCardId1);
            SetCardId(editEwMeritCardButton2, Settings.Default.ewMeritCardId2);

            SetCardId(editDIMeritCardButton1, Settings.Default.diMeritCardId1);
            SetCardId(editDIMeritCardButton2, Settings.Default.diMeritCardId2);
        }

        private void SetCardId(EditCardButton btn, int cardId)
        {
            if (cardId > 0)
            {
                var card = Card.Get(cardId);
                if (card != null)
                {
                    btn.EditCard = new UserCard() { CardId = card.Id };
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Settings.Default.hydraMeritCardId1 = editHydraMeritCardButton1.EditCardId;
            Settings.Default.hydraMeritCardId2 = editHydraMeritCardButton2.EditCardId;
            Settings.Default.hydraMeritCardId3 = editHydraMeritCardButton3.EditCardId;
            Settings.Default.ewMeritCardId1 = editEwMeritCardButton1.EditCardId;
            Settings.Default.ewMeritCardId2 = editEwMeritCardButton2.EditCardId;
            Settings.Default.diMeritCardId1 = editDIMeritCardButton1.EditCardId;
            Settings.Default.diMeritCardId2 = editDIMeritCardButton2.EditCardId;
            Settings.Default.Save();

            Game.TheGame.HydraMeritCardIds.Clear();
            Game.TheGame.HydraMeritCardIds.AddRange(new[] { Settings.Default.hydraMeritCardId1, Settings.Default.hydraMeritCardId2, Settings.Default.hydraMeritCardId3 }.Where(x => x != 0));

            Game.TheGame.ElementalWarMeritCardIds.Clear();
            Game.TheGame.ElementalWarMeritCardIds.AddRange(new[] { Settings.Default.ewMeritCardId1, Settings.Default.ewMeritCardId2 }.Where(x => x != 0));

            Game.TheGame.DemonInvasionMeritCardIds.Clear();
            Game.TheGame.DemonInvasionMeritCardIds.AddRange(new[] { Settings.Default.diMeritCardId1, Settings.Default.diMeritCardId2 }.Where(x => x != 0));

        }
    }
}
