﻿namespace ElementalKingdoms.ui
{
    partial class DeckEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCard1 = new System.Windows.Forms.Button();
            this.btnCard2 = new System.Windows.Forms.Button();
            this.btnCard3 = new System.Windows.Forms.Button();
            this.btnCard4 = new System.Windows.Forms.Button();
            this.btnCard5 = new System.Windows.Forms.Button();
            this.btnCard6 = new System.Windows.Forms.Button();
            this.btnCard7 = new System.Windows.Forms.Button();
            this.btnCard8 = new System.Windows.Forms.Button();
            this.btnCard9 = new System.Windows.Forms.Button();
            this.btnCard10 = new System.Windows.Forms.Button();
            this.btnRune1 = new System.Windows.Forms.Button();
            this.btnRune2 = new System.Windows.Forms.Button();
            this.btnRune3 = new System.Windows.Forms.Button();
            this.btnRune4 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblCost = new System.Windows.Forms.Label();
            this.lblCostTitle = new System.Windows.Forms.Label();
            this.pbDeckCost = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.numEditHeroLevel = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.favouriteCardMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.numEditHeroLevel)).BeginInit();
            this.favouriteCardMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCard1
            // 
            this.btnCard1.Location = new System.Drawing.Point(135, 5);
            this.btnCard1.Name = "btnCard1";
            this.btnCard1.Size = new System.Drawing.Size(50, 50);
            this.btnCard1.TabIndex = 0;
            this.btnCard1.Text = "Card 1";
            this.btnCard1.UseVisualStyleBackColor = true;
            this.btnCard1.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard1.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard2
            // 
            this.btnCard2.Location = new System.Drawing.Point(190, 5);
            this.btnCard2.Name = "btnCard2";
            this.btnCard2.Size = new System.Drawing.Size(50, 50);
            this.btnCard2.TabIndex = 1;
            this.btnCard2.Text = "Card 2";
            this.btnCard2.UseVisualStyleBackColor = true;
            this.btnCard2.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard2.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard3
            // 
            this.btnCard3.Location = new System.Drawing.Point(245, 5);
            this.btnCard3.Name = "btnCard3";
            this.btnCard3.Size = new System.Drawing.Size(50, 50);
            this.btnCard3.TabIndex = 2;
            this.btnCard3.Text = "Card 3";
            this.btnCard3.UseVisualStyleBackColor = true;
            this.btnCard3.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard3.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard4
            // 
            this.btnCard4.Location = new System.Drawing.Point(300, 5);
            this.btnCard4.Name = "btnCard4";
            this.btnCard4.Size = new System.Drawing.Size(50, 50);
            this.btnCard4.TabIndex = 3;
            this.btnCard4.Text = "Card 4";
            this.btnCard4.UseVisualStyleBackColor = true;
            this.btnCard4.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard4.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard5
            // 
            this.btnCard5.Location = new System.Drawing.Point(355, 5);
            this.btnCard5.Name = "btnCard5";
            this.btnCard5.Size = new System.Drawing.Size(50, 50);
            this.btnCard5.TabIndex = 4;
            this.btnCard5.Text = "Card 5";
            this.btnCard5.UseVisualStyleBackColor = true;
            this.btnCard5.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard5.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard6
            // 
            this.btnCard6.Location = new System.Drawing.Point(135, 60);
            this.btnCard6.Name = "btnCard6";
            this.btnCard6.Size = new System.Drawing.Size(50, 50);
            this.btnCard6.TabIndex = 9;
            this.btnCard6.Text = "Card 6";
            this.btnCard6.UseVisualStyleBackColor = true;
            this.btnCard6.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard6.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard7
            // 
            this.btnCard7.Location = new System.Drawing.Point(190, 60);
            this.btnCard7.Name = "btnCard7";
            this.btnCard7.Size = new System.Drawing.Size(50, 50);
            this.btnCard7.TabIndex = 8;
            this.btnCard7.Text = "Card 7";
            this.btnCard7.UseVisualStyleBackColor = true;
            this.btnCard7.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard7.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard8
            // 
            this.btnCard8.Location = new System.Drawing.Point(245, 60);
            this.btnCard8.Name = "btnCard8";
            this.btnCard8.Size = new System.Drawing.Size(50, 50);
            this.btnCard8.TabIndex = 7;
            this.btnCard8.Text = "Card 8";
            this.btnCard8.UseVisualStyleBackColor = true;
            this.btnCard8.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard8.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard9
            // 
            this.btnCard9.Location = new System.Drawing.Point(300, 60);
            this.btnCard9.Name = "btnCard9";
            this.btnCard9.Size = new System.Drawing.Size(50, 50);
            this.btnCard9.TabIndex = 6;
            this.btnCard9.Text = "Card 9";
            this.btnCard9.UseVisualStyleBackColor = true;
            this.btnCard9.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard9.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnCard10
            // 
            this.btnCard10.Location = new System.Drawing.Point(355, 60);
            this.btnCard10.Name = "btnCard10";
            this.btnCard10.Size = new System.Drawing.Size(50, 50);
            this.btnCard10.TabIndex = 5;
            this.btnCard10.Text = "Card 10";
            this.btnCard10.UseVisualStyleBackColor = true;
            this.btnCard10.Click += new System.EventHandler(this.btnCard_Click);
            this.btnCard10.MouseHover += new System.EventHandler(this.btnCard_MouseHover);
            this.btnCard10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCard_MouseUp);
            // 
            // btnRune1
            // 
            this.btnRune1.Location = new System.Drawing.Point(5, 5);
            this.btnRune1.Name = "btnRune1";
            this.btnRune1.Size = new System.Drawing.Size(50, 50);
            this.btnRune1.TabIndex = 13;
            this.btnRune1.Text = "Rune 1";
            this.btnRune1.UseVisualStyleBackColor = true;
            this.btnRune1.Click += new System.EventHandler(this.btnRune_Click);
            this.btnRune1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnRune_MouseUp);
            // 
            // btnRune2
            // 
            this.btnRune2.Location = new System.Drawing.Point(60, 5);
            this.btnRune2.Name = "btnRune2";
            this.btnRune2.Size = new System.Drawing.Size(50, 50);
            this.btnRune2.TabIndex = 12;
            this.btnRune2.Text = "Rune 2";
            this.btnRune2.UseVisualStyleBackColor = true;
            this.btnRune2.Click += new System.EventHandler(this.btnRune_Click);
            this.btnRune2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnRune_MouseUp);
            // 
            // btnRune3
            // 
            this.btnRune3.Location = new System.Drawing.Point(5, 60);
            this.btnRune3.Name = "btnRune3";
            this.btnRune3.Size = new System.Drawing.Size(50, 50);
            this.btnRune3.TabIndex = 11;
            this.btnRune3.Text = "Rune 3";
            this.btnRune3.UseVisualStyleBackColor = true;
            this.btnRune3.Click += new System.EventHandler(this.btnRune_Click);
            this.btnRune3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnRune_MouseUp);
            // 
            // btnRune4
            // 
            this.btnRune4.Location = new System.Drawing.Point(60, 60);
            this.btnRune4.Name = "btnRune4";
            this.btnRune4.Size = new System.Drawing.Size(50, 50);
            this.btnRune4.TabIndex = 10;
            this.btnRune4.Text = "Rune 4";
            this.btnRune4.UseVisualStyleBackColor = true;
            this.btnRune4.Click += new System.EventHandler(this.btnRune_Click);
            this.btnRune4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnRune_MouseUp);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(462, 7);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(131, 22);
            this.tbName.TabIndex = 55;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // lblCost
            // 
            this.lblCost.AutoSize = true;
            this.lblCost.BackColor = System.Drawing.Color.Transparent;
            this.lblCost.Location = new System.Drawing.Point(459, 89);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(16, 17);
            this.lblCost.TabIndex = 54;
            this.lblCost.Text = "0";
            // 
            // lblCostTitle
            // 
            this.lblCostTitle.AutoSize = true;
            this.lblCostTitle.Location = new System.Drawing.Point(411, 63);
            this.lblCostTitle.Name = "lblCostTitle";
            this.lblCostTitle.Size = new System.Drawing.Size(36, 17);
            this.lblCostTitle.TabIndex = 53;
            this.lblCostTitle.Text = "Cost";
            // 
            // pbDeckCost
            // 
            this.pbDeckCost.Location = new System.Drawing.Point(462, 63);
            this.pbDeckCost.Name = "pbDeckCost";
            this.pbDeckCost.Size = new System.Drawing.Size(131, 23);
            this.pbDeckCost.TabIndex = 52;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(411, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 51;
            this.label1.Text = "Level";
            // 
            // numEditHeroLevel
            // 
            this.numEditHeroLevel.Location = new System.Drawing.Point(537, 35);
            this.numEditHeroLevel.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numEditHeroLevel.Name = "numEditHeroLevel";
            this.numEditHeroLevel.Size = new System.Drawing.Size(56, 22);
            this.numEditHeroLevel.TabIndex = 50;
            this.numEditHeroLevel.ValueChanged += new System.EventHandler(this.numEditHeroLevel_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(411, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 56;
            this.label3.Text = "Name";
            // 
            // favouriteCardMenu
            // 
            this.favouriteCardMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.favouriteCardMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.otherToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.favouriteCardMenu.Name = "contextMenuStrip1";
            this.favouriteCardMenu.Size = new System.Drawing.Size(182, 84);
            // 
            // otherToolStripMenuItem
            // 
            this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
            this.otherToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.otherToolStripMenuItem.Text = "Edit...";
            this.otherToolStripMenuItem.Click += new System.EventHandler(this.otherToolStripMenuItem_Click);
            this.otherToolStripMenuItem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.otherToolStripMenuItem_MouseDown);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeToolStripMenuItem_Click);
            // 
            // DeckEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblCost);
            this.Controls.Add(this.lblCostTitle);
            this.Controls.Add(this.pbDeckCost);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numEditHeroLevel);
            this.Controls.Add(this.btnRune1);
            this.Controls.Add(this.btnRune2);
            this.Controls.Add(this.btnRune3);
            this.Controls.Add(this.btnRune4);
            this.Controls.Add(this.btnCard6);
            this.Controls.Add(this.btnCard7);
            this.Controls.Add(this.btnCard8);
            this.Controls.Add(this.btnCard9);
            this.Controls.Add(this.btnCard10);
            this.Controls.Add(this.btnCard5);
            this.Controls.Add(this.btnCard4);
            this.Controls.Add(this.btnCard3);
            this.Controls.Add(this.btnCard2);
            this.Controls.Add(this.btnCard1);
            this.Name = "DeckEditor";
            this.Size = new System.Drawing.Size(601, 120);
            ((System.ComponentModel.ISupportInitialize)(this.numEditHeroLevel)).EndInit();
            this.favouriteCardMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCard1;
        private System.Windows.Forms.Button btnCard2;
        private System.Windows.Forms.Button btnCard3;
        private System.Windows.Forms.Button btnCard4;
        private System.Windows.Forms.Button btnCard5;
        private System.Windows.Forms.Button btnCard6;
        private System.Windows.Forms.Button btnCard7;
        private System.Windows.Forms.Button btnCard8;
        private System.Windows.Forms.Button btnCard9;
        private System.Windows.Forms.Button btnCard10;
        private System.Windows.Forms.Button btnRune1;
        private System.Windows.Forms.Button btnRune2;
        private System.Windows.Forms.Button btnRune3;
        private System.Windows.Forms.Button btnRune4;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Label lblCostTitle;
        private System.Windows.Forms.ProgressBar pbDeckCost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numEditHeroLevel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ContextMenuStrip favouriteCardMenu;
        private System.Windows.Forms.ToolStripMenuItem otherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
    }
}
