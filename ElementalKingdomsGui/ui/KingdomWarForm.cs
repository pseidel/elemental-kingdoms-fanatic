﻿using ElementalKingdoms.core;
using ElementalKingdoms.core.kingdomwar;
using ElementalKingdoms.server;
using ElementalKingdoms.util;
using ElementalKingdomsGui.Properties;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace ElementalKingdomsGui.ui
{
    public partial class KingdomWarForm : Form
    {
        List<KingdomWar> States = new List<KingdomWar>();
        ServerCommunicator com;

        public KingdomWarForm()
        {
            InitializeComponent();

            listBox1.DisplayMember = "Name";
			//Nharzhool
            com = ServerCommunicator.Create(Settings.Default.DeviceId, false);
            com.Uin = "52b4a27ebef81";
            com.Connect();
            com.EKLogin();

            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!backgroundWorker1.CancellationPending)
            {
                var o = com.GetForceMap();

                var dataPrt = o.GetValue("data");
                if (o.Value<int>("status") == 0)
                {
                    System.Console.WriteLine("Could not get data: {0}", o.Value<string>("message"));
                    return;
                }
                var kw =  dataPrt.ToObject<KingdomWar>();

                //var kw = JsonLoader.LoadKingdomWar();
                backgroundWorker1.ReportProgress(0, kw);

                Thread.Sleep(5000);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var kw = (KingdomWar)e.UserState;
            States.Add(kw);


            lblForest.Text = string.Format("Forest {0}", kw.Resource[((int)Race.Forest).ToString()]);
            lblTundra.Text = string.Format("Tundra {0}", kw.Resource[((int)Race.Tundra).ToString()]);
            lblMountain.Text = string.Format("Mountain {0}", kw.Resource[((int)Race.Mountain).ToString()]);
            lblSwamp.Text = string.Format("Swamp {0}", kw.Resource[((int)Race.Swamp).ToString()]);


            string currentSelection = null;
            var selectedItem = listBox1.SelectedItem as Point;
            if (selectedItem != null)
            {
                currentSelection = selectedItem.Name;
            }
            var targets = kw.GetAttackableTargets();
            listBox1.DataSource = targets;

            if (currentSelection != null)
            {
                listBox1.SelectedItem = targets.Find(x => x.Name == currentSelection);
            }
        }
    }
}
