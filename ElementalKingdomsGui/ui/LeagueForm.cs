﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;
using ElementalKingdoms.server;
using ElementalKingdoms.tools;
using ElementalKingdomsGui.Properties;

namespace ElementalKingdoms.ui
{
    public partial class LeagueForm : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Dictionary<int, User> LeagueUsers;
        private Dictionary<string, LeaguePlayer> LeaguePlayerResults;

        public LeagueForm()
        {
            InitializeComponent();
            tbNumberOfBattles.Text = Settings.Default.numberOfSimulations.ToString();
            
            LeagueUsers = new Dictionary<int, User>();
            LoadLeagueInfo();
        }


        private void LoadLeagueInfo()
        {
            if (League.CurrentLeague == null)
            {
                lblStatus.Text = "No league loaded.";
                return;
            }
            LeaguePlayerResults = new Dictionary<string, LeaguePlayer>();
            tableLayoutPanel1.Controls.Clear();
            for (int roundNumber = 0; roundNumber < Math.Min(3, League.CurrentLeague.RoundResult.Count); roundNumber++)
            {
                lblRoundInfo.Text = String.Format("Season {0} Round {1}", League.CurrentLeague.LeagueId, League.CurrentLeague.RoundNow);
                List<League.LeagueRoundResult> round = League.CurrentLeague.RoundResult[roundNumber];
                for (int battleNumber = 0; battleNumber < round.Count; battleNumber++)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("{0}-> {1} -- {2}", roundNumber, battleNumber, round[battleNumber].BattleId);

                    LeaguePlayer r = new LeaguePlayer();
                    r.SetLeagueRoundResult(round[battleNumber]);
                    int row = roundNumber + battleNumber * (roundNumber + 1);
                    tableLayoutPanel1.Controls.Add(r, roundNumber, row);
                    if (log.IsDebugEnabled) log.DebugFormat("column {0} row {1}", roundNumber, row);
                    LeaguePlayerResults[round[battleNumber].BattleInfo.User.NickName] = r;
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            int numberOfBattles = (int)e.Argument;

            int currentLeague = Math.Min(League.CurrentLeague.RoundResult.Count, 3);

            // 'Ab'use the about box to get the version information
            AboutBox about = new AboutBox();

            backgroundWorker1.ReportProgress(0, String.Format("{0} {1} - Running FoH ({2} iterations)\r\n", about.AssemblyTitle, about.AssemblyVersion, numberOfBattles));

            List<League.LeagueRoundResult> currentBattles = League.CurrentLeague.RoundResult[currentLeague-1];
            for (int i = 0; i < currentBattles.Count; i++)
            {
                League.LeagueRoundResult result = currentBattles[i];
                //if (log.IsDebugEnabled) log.DebugFormat("Found player {0} with odds {1} ({2} honor bet)", result.BattleInfo.User.NickName, result.BetOdds, result.BetTotal);


                User attacker = result.BattleInfo.User;
                attacker.UserDeck.Cards = result.BattleInfo.Cards;
                attacker.UserDeck.Runes = result.BattleInfo.Runes;

                int opponentIndex = -1;
                if (i%2 == 0)
                {
                    // opponent is +1
                    opponentIndex = i + 1;
                }
                else
                {
                    opponentIndex = i - 1;
                }

                League.LeagueRoundResult opponentResult = currentBattles[opponentIndex];
                User defender = opponentResult.BattleInfo.User;
                defender.UserDeck.Cards = opponentResult.BattleInfo.Cards;
                defender.UserDeck.Runes = opponentResult.BattleInfo.Runes;

                if (cbDetermineAttacker.Checked && attacker.GetFohMagicNumber() < defender.GetFohMagicNumber())
                {
                    log.InfoFormat("Skipping {0} vs {1} - {1} will be the attacker", attacker.NickName, defender.NickName);
                    continue;
                }

                backgroundWorker1.ReportProgress(i * 100 / currentBattles.Count + 5, String.Format("{0} vs {1}: ", attacker.NickName, defender.NickName));

                MultiBattleExecutor mb = new MultiBattleExecutor(attacker, defender);
                var simResult = mb.Execute(Settings.Default.numberOfSimulations);

                int attackerWinChance = simResult.RoundsWon * 100 / simResult.Iterations;
                //string resultString = String.Format("{0} has {1}% chance to win\r\n", attacker.NickName, attackerWinChance);

                InternalLeagueResult r = new InternalLeagueResult() 
                {
                    Attacker = attacker.NickName, 
                    Defender = defender.NickName, 
                    //Result = resultString, 
                    WinChance = attackerWinChance 
                };

                backgroundWorker1.ReportProgress((i+1) * 100 / currentBattles.Count, r);
            }

            backgroundWorker1.ReportProgress(100, "Finished!");
        }
        private class InternalLeagueResult
        {
            //public string Result { get; set; }
            public string Attacker { get; set; }
            public string Defender { get; set; }
            public int WinChance { get; set; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            progressBar1.Visible = true;
            if (backgroundWorker1.IsBusy) return;

            int numberOfSimulations;
            if (!int.TryParse(tbNumberOfBattles.Text, out numberOfSimulations))
            {
                return;
            }

            Settings.Default.numberOfSimulations = numberOfSimulations;

            if (textBox1.Text.Length > 0)
            {
                textBox1.AppendText("\r\n\r\n");
            }

            backgroundWorker1.RunWorkerAsync(numberOfSimulations);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine("PROGRESS: {0}", e.UserState);
            progressBar1.Value = e.ProgressPercentage;

            if (e.UserState is string)
            {
                textBox1.AppendText(e.UserState.ToString());
            }
            else if (e.UserState is InternalLeagueResult)
            {
                InternalLeagueResult r = (InternalLeagueResult)e.UserState;

                string resultString = String.Format("{0} has {1}% chance to win\r\n", r.Attacker, r.WinChance);
                textBox1.AppendText(resultString);

                LeaguePlayerResults[r.Attacker].SetAttackWinChance(r.WinChance);
                LeaguePlayerResults[r.Defender].SetDefendWinChance(100 - r.WinChance);
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button1.Enabled = true;
        }

        private void btnLoadFromServer_Click(object sender, EventArgs e)
        {
            ServerConnectorForm serverConDialog = new ServerConnectorForm();
            serverConDialog.ShowDialog();
            
            if (String.IsNullOrEmpty(Settings.Default.DeviceId))
            {
                Settings.Default.DeviceId = ServerCommunicator.GenerateDeviceId();
                Settings.Default.Save();
            }

            ServerCommunicator com = ServerCommunicator.Create(Settings.Default.DeviceId, true);
            com.GetLeagueInfo();
            LoadLeagueInfo();
        }
    }
}
