﻿namespace ElementalKingdoms.ui
{
    partial class BattleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BattleForm));
			this.lblDefenderHP = new System.Windows.Forms.Label();
			this.lblAttackerHP = new System.Windows.Forms.Label();
			this.lbDefenderCemetary = new System.Windows.Forms.ListBox();
			this.lbAttackerCemetary = new System.Windows.Forms.ListBox();
			this.lbDefenderDeck = new System.Windows.Forms.ListBox();
			this.lbAttackerDeck = new System.Windows.Forms.ListBox();
			this.btnNext = new System.Windows.Forms.Button();
			this.imageLoader = new System.ComponentModel.BackgroundWorker();
			this.label3 = new System.Windows.Forms.Label();
			this.lblRound = new System.Windows.Forms.Label();
			this.lblDefenderName = new System.Windows.Forms.Label();
			this.lblAttackerName = new System.Windows.Forms.Label();
			this.btnEditDefender = new System.Windows.Forms.Button();
			this.btnEditAttacker = new System.Windows.Forms.Button();
			this.btnResetBattle = new System.Windows.Forms.Button();
			this.btnAuto = new System.Windows.Forms.Button();
			this.timerAutoBattle = new System.Windows.Forms.Timer(this.components);
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.button1 = new System.Windows.Forms.Button();
			this.battleFieldDefender = new ElementalKingdoms.ui.BattleField();
			this.battleFieldAttacker = new ElementalKingdoms.ui.BattleField();
			this.handViewerDefender = new ElementalKingdoms.ui.HandViewer();
			this.handViewerAttacker = new ElementalKingdoms.ui.HandViewer();
			this.rvDefenderRunes = new ElementalKingdoms.ui.BattleRuneViewer();
			this.rvAttackerRunes = new ElementalKingdoms.ui.BattleRuneViewer();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			this.SuspendLayout();
			// 
			// lblDefenderHP
			// 
			this.lblDefenderHP.AutoSize = true;
			this.lblDefenderHP.Location = new System.Drawing.Point(74, 235);
			this.lblDefenderHP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblDefenderHP.Name = "lblDefenderHP";
			this.lblDefenderHP.Size = new System.Drawing.Size(79, 13);
			this.lblDefenderHP.TabIndex = 2;
			this.lblDefenderHP.Text = "Opponent LIFE";
			// 
			// lblAttackerHP
			// 
			this.lblAttackerHP.AutoSize = true;
			this.lblAttackerHP.Location = new System.Drawing.Point(79, 285);
			this.lblAttackerHP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblAttackerHP.Name = "lblAttackerHP";
			this.lblAttackerHP.Size = new System.Drawing.Size(61, 13);
			this.lblAttackerHP.TabIndex = 3;
			this.lblAttackerHP.Text = "Player LIFE";
			// 
			// lbDefenderCemetary
			// 
			this.lbDefenderCemetary.FormattingEnabled = true;
			this.lbDefenderCemetary.Location = new System.Drawing.Point(419, 93);
			this.lbDefenderCemetary.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbDefenderCemetary.Name = "lbDefenderCemetary";
			this.lbDefenderCemetary.Size = new System.Drawing.Size(131, 56);
			this.lbDefenderCemetary.TabIndex = 8;
			// 
			// lbAttackerCemetary
			// 
			this.lbAttackerCemetary.FormattingEnabled = true;
			this.lbAttackerCemetary.Location = new System.Drawing.Point(418, 392);
			this.lbAttackerCemetary.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbAttackerCemetary.Name = "lbAttackerCemetary";
			this.lbAttackerCemetary.Size = new System.Drawing.Size(132, 56);
			this.lbAttackerCemetary.TabIndex = 9;
			// 
			// lbDefenderDeck
			// 
			this.lbDefenderDeck.FormattingEnabled = true;
			this.lbDefenderDeck.Location = new System.Drawing.Point(12, 93);
			this.lbDefenderDeck.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbDefenderDeck.Name = "lbDefenderDeck";
			this.lbDefenderDeck.Size = new System.Drawing.Size(107, 17);
			this.lbDefenderDeck.TabIndex = 10;
			// 
			// lbAttackerDeck
			// 
			this.lbAttackerDeck.FormattingEnabled = true;
			this.lbAttackerDeck.Location = new System.Drawing.Point(12, 431);
			this.lbAttackerDeck.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbAttackerDeck.Name = "lbAttackerDeck";
			this.lbAttackerDeck.Size = new System.Drawing.Size(107, 17);
			this.lbAttackerDeck.TabIndex = 11;
			// 
			// btnNext
			// 
			this.btnNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnNext.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.btnNext.Location = new System.Drawing.Point(10, 11);
			this.btnNext.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.btnNext.Name = "btnNext";
			this.btnNext.Size = new System.Drawing.Size(51, 51);
			this.btnNext.TabIndex = 12;
			this.btnNext.Text = "NEXT";
			this.btnNext.UseVisualStyleBackColor = false;
			this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
			// 
			// imageLoader
			// 
			this.imageLoader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.imageLoader_DoWork);
			this.imageLoader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.imageLoader_RunWorkerCompleted);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(40, 259);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(39, 13);
			this.label3.TabIndex = 18;
			this.label3.Text = "Round";
			// 
			// lblRound
			// 
			this.lblRound.AutoSize = true;
			this.lblRound.Location = new System.Drawing.Point(75, 259);
			this.lblRound.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblRound.Name = "lblRound";
			this.lblRound.Size = new System.Drawing.Size(13, 13);
			this.lblRound.TabIndex = 19;
			this.lblRound.Text = "1";
			// 
			// lblDefenderName
			// 
			this.lblDefenderName.AutoSize = true;
			this.lblDefenderName.Location = new System.Drawing.Point(12, 235);
			this.lblDefenderName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblDefenderName.Name = "lblDefenderName";
			this.lblDefenderName.Size = new System.Drawing.Size(88, 13);
			this.lblDefenderName.TabIndex = 22;
			this.lblDefenderName.Text = "Opponent NAME";
			// 
			// lblAttackerName
			// 
			this.lblAttackerName.AutoSize = true;
			this.lblAttackerName.Location = new System.Drawing.Point(12, 285);
			this.lblAttackerName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblAttackerName.Name = "lblAttackerName";
			this.lblAttackerName.Size = new System.Drawing.Size(70, 13);
			this.lblAttackerName.TabIndex = 23;
			this.lblAttackerName.Text = "Player NAME";
			// 
			// btnEditDefender
			// 
			this.btnEditDefender.BackColor = System.Drawing.SystemColors.Highlight;
			this.btnEditDefender.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnEditDefender.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.btnEditDefender.Location = new System.Drawing.Point(571, 84);
			this.btnEditDefender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.btnEditDefender.Name = "btnEditDefender";
			this.btnEditDefender.Size = new System.Drawing.Size(64, 68);
			this.btnEditDefender.TabIndex = 28;
			this.btnEditDefender.Text = "Edit defender";
			this.btnEditDefender.UseVisualStyleBackColor = false;
			this.btnEditDefender.Visible = false;
			this.btnEditDefender.Click += new System.EventHandler(this.btnEditDefender_Click);
			// 
			// btnEditAttacker
			// 
			this.btnEditAttacker.BackColor = System.Drawing.SystemColors.Highlight;
			this.btnEditAttacker.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnEditAttacker.ForeColor = System.Drawing.SystemColors.HighlightText;
			this.btnEditAttacker.Location = new System.Drawing.Point(571, 389);
			this.btnEditAttacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.btnEditAttacker.Name = "btnEditAttacker";
			this.btnEditAttacker.Size = new System.Drawing.Size(64, 68);
			this.btnEditAttacker.TabIndex = 29;
			this.btnEditAttacker.Text = "Edit attacker";
			this.btnEditAttacker.UseVisualStyleBackColor = false;
			this.btnEditAttacker.Visible = false;
			this.btnEditAttacker.Click += new System.EventHandler(this.btnEditAttacker_Click);
			// 
			// btnResetBattle
			// 
			this.btnResetBattle.Location = new System.Drawing.Point(124, 11);
			this.btnResetBattle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.btnResetBattle.Name = "btnResetBattle";
			this.btnResetBattle.Size = new System.Drawing.Size(53, 51);
			this.btnResetBattle.TabIndex = 32;
			this.btnResetBattle.Text = "Reset battle";
			this.btnResetBattle.UseVisualStyleBackColor = true;
			this.btnResetBattle.Click += new System.EventHandler(this.btnResetBattle_Click);
			// 
			// btnAuto
			// 
			this.btnAuto.Location = new System.Drawing.Point(66, 11);
			this.btnAuto.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.btnAuto.Name = "btnAuto";
			this.btnAuto.Size = new System.Drawing.Size(53, 28);
			this.btnAuto.TabIndex = 35;
			this.btnAuto.Text = "Auto";
			this.btnAuto.UseVisualStyleBackColor = true;
			this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
			// 
			// timerAutoBattle
			// 
			this.timerAutoBattle.Interval = 1000;
			this.timerAutoBattle.Tick += new System.EventHandler(this.timerAutoBattle_Tick);
			// 
			// trackBar1
			// 
			this.trackBar1.AutoSize = false;
			this.trackBar1.Location = new System.Drawing.Point(66, 43);
			this.trackBar1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.trackBar1.Maximum = 5;
			this.trackBar1.Minimum = 1;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Size = new System.Drawing.Size(53, 31);
			this.trackBar1.TabIndex = 36;
			this.trackBar1.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
			this.trackBar1.Value = 1;
			this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(181, 11);
			this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(57, 51);
			this.button1.TabIndex = 41;
			this.button1.Text = "Save replay";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// battleFieldDefender
			// 
			this.battleFieldDefender.AutoScroll = true;
			this.battleFieldDefender.AutoSize = true;
			this.battleFieldDefender.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.battleFieldDefender.Location = new System.Drawing.Point(134, 151);
			this.battleFieldDefender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.battleFieldDefender.Name = "battleFieldDefender";
			this.battleFieldDefender.Size = new System.Drawing.Size(255, 107);
			this.battleFieldDefender.TabIndex = 27;
			// 
			// battleFieldAttacker
			// 
			this.battleFieldAttacker.AutoScroll = true;
			this.battleFieldAttacker.AutoSize = true;
			this.battleFieldAttacker.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.battleFieldAttacker.Location = new System.Drawing.Point(134, 277);
			this.battleFieldAttacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.battleFieldAttacker.Name = "battleFieldAttacker";
			this.battleFieldAttacker.Size = new System.Drawing.Size(255, 107);
			this.battleFieldAttacker.TabIndex = 26;
			// 
			// handViewerDefender
			// 
			this.handViewerDefender.Location = new System.Drawing.Point(134, 87);
			this.handViewerDefender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.handViewerDefender.Name = "handViewerDefender";
			this.handViewerDefender.Size = new System.Drawing.Size(255, 65);
			this.handViewerDefender.TabIndex = 25;
			// 
			// handViewerAttacker
			// 
			this.handViewerAttacker.Location = new System.Drawing.Point(134, 393);
			this.handViewerAttacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.handViewerAttacker.Name = "handViewerAttacker";
			this.handViewerAttacker.Size = new System.Drawing.Size(255, 65);
			this.handViewerAttacker.TabIndex = 24;
			// 
			// rvDefenderRunes
			// 
			this.rvDefenderRunes.Location = new System.Drawing.Point(25, 127);
			this.rvDefenderRunes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.rvDefenderRunes.Name = "rvDefenderRunes";
			this.rvDefenderRunes.Size = new System.Drawing.Size(98, 106);
			this.rvDefenderRunes.TabIndex = 21;
			// 
			// rvAttackerRunes
			// 
			this.rvAttackerRunes.Location = new System.Drawing.Point(25, 301);
			this.rvAttackerRunes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.rvAttackerRunes.Name = "rvAttackerRunes";
			this.rvAttackerRunes.Size = new System.Drawing.Size(98, 106);
			this.rvAttackerRunes.TabIndex = 20;
			// 
			// BattleForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(717, 468);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.trackBar1);
			this.Controls.Add(this.btnAuto);
			this.Controls.Add(this.btnResetBattle);
			this.Controls.Add(this.btnEditAttacker);
			this.Controls.Add(this.btnEditDefender);
			this.Controls.Add(this.battleFieldDefender);
			this.Controls.Add(this.battleFieldAttacker);
			this.Controls.Add(this.handViewerDefender);
			this.Controls.Add(this.handViewerAttacker);
			this.Controls.Add(this.lblAttackerName);
			this.Controls.Add(this.lblDefenderName);
			this.Controls.Add(this.rvDefenderRunes);
			this.Controls.Add(this.rvAttackerRunes);
			this.Controls.Add(this.lblRound);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btnNext);
			this.Controls.Add(this.lbAttackerDeck);
			this.Controls.Add(this.lbDefenderDeck);
			this.Controls.Add(this.lbAttackerCemetary);
			this.Controls.Add(this.lbDefenderCemetary);
			this.Controls.Add(this.lblAttackerHP);
			this.Controls.Add(this.lblDefenderHP);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Name = "BattleForm";
			this.Text = "Battle Simulator";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BattleForm_FormClosing);
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDefenderHP;
        private System.Windows.Forms.Label lblAttackerHP;
        private System.Windows.Forms.ListBox lbDefenderCemetary;
        private System.Windows.Forms.ListBox lbAttackerCemetary;
        private System.Windows.Forms.ListBox lbDefenderDeck;
        private System.Windows.Forms.ListBox lbAttackerDeck;
        private System.Windows.Forms.Button btnNext;
        private System.ComponentModel.BackgroundWorker imageLoader;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblRound;
        private BattleRuneViewer rvAttackerRunes;
        private BattleRuneViewer rvDefenderRunes;
        private System.Windows.Forms.Label lblDefenderName;
        private System.Windows.Forms.Label lblAttackerName;
        private HandViewer handViewerAttacker;
        private HandViewer handViewerDefender;
        private BattleField battleFieldAttacker;
        private BattleField battleFieldDefender;
        private System.Windows.Forms.Button btnEditDefender;
        private System.Windows.Forms.Button btnEditAttacker;
        private System.Windows.Forms.Button btnResetBattle;
        private System.Windows.Forms.Button btnAuto;
        private System.Windows.Forms.Timer timerAutoBattle;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button button1;
    }
}