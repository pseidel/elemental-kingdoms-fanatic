﻿using ElementalKingdoms.core;
using ElementalKingdoms.tools;
using ElementalKingdoms.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ElementalKingdomsGui.Properties;
using ElementalKingdomsGui.util;


namespace ElementalKingdoms.ui
{
    public partial class BattleForm : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private Battle battle;

        private BindingSource bsAttackPlayerDeck;
        private BindingSource bsAttackPlayerCemetary;
        private Binding bsAttackPlayerHealth;

        private BindingSource bsDefendPlayerDeck;
        private BindingSource bsDefendPlayerCemetary;
        private Binding bsDefendPlayerHealth;

        private User attacker;
        private User defender;

        public BattleForm(User attacker, User defender)
        {
            InitializeComponent();

            this.attacker = attacker;
            this.defender = defender;

            //new MainForm();
            //Card.PrintAll();
            battle = InitializeBattle();
            DataBind();
        }

        void ResetBattle()
        {
            btnAuto.Text = "Auto";
            timerAutoBattle.Enabled = false;
            lblAttackerHP.DataBindings.Clear();
            lblDefenderHP.DataBindings.Clear();
            battle = InitializeBattle();
            DataBind();
            RefreshDatabindings();
            
            //MultiBattleExecutor battles = new MultiBattleExecutor(attacker, defender);
            //MultiBattleExecutor.Result r = battles.Execute(20);

            //lblQuickStats.Text = r.ToString();
        }

        private Battle InitializeBattle()
        {
            lblAttackerName.Text = attacker.NickName;
            lblDefenderName.Text = defender.NickName;

            return new Battle(attacker, defender);
        }

        private void DataBind()
        {
            lbAttackerDeck.DataSource = bsAttackPlayerDeck = new BindingSource() { DataSource = battle.AttackPlayer.Deck };
            lbAttackerCemetary.DataSource = bsAttackPlayerCemetary = new BindingSource() { DataSource = battle.AttackPlayer.Cemetary };
            lblAttackerHP.DataBindings.Add(bsAttackPlayerHealth = new Binding("Text", battle.AttackPlayer, "CurrentHP"));

            lbDefenderDeck.DataSource = bsDefendPlayerDeck = new BindingSource() { DataSource = battle.DefendPlayer.Deck };
            lbDefenderCemetary.DataSource = bsDefendPlayerCemetary = new BindingSource() { DataSource = battle.DefendPlayer.Cemetary };
            lblDefenderHP.DataBindings.Add(bsDefendPlayerHealth = new Binding("Text", battle.DefendPlayer, "CurrentHP"));

            rvAttackerRunes.SetRunes(battle.AttackPlayer.Runes);
            rvDefenderRunes.SetRunes(battle.DefendPlayer.Runes);

            handViewerAttacker.SetHand(battle.AttackPlayer.Hand);
            handViewerDefender.SetHand(battle.DefendPlayer.Hand);
        }

        private void RefreshDatabindings()
        {
            this.SuspendLayout();

            lblRound.Text = battle.Rounds.Count.ToString();

            bsAttackPlayerDeck.ResetBindings(false);
            bsAttackPlayerCemetary.ResetBindings(false);
            bsAttackPlayerHealth.ReadValue();

            bsDefendPlayerDeck.ResetBindings(false);
            bsDefendPlayerCemetary.ResetBindings(false);
            bsDefendPlayerHealth.ReadValue();

            handViewerAttacker.SetHand(battle.AttackPlayer.Hand);
            handViewerDefender.SetHand(battle.DefendPlayer.Hand);

            battleFieldAttacker.SetBattlefield(battle.AttackPlayer.Battlefield);
            battleFieldDefender.SetBattlefield(battle.DefendPlayer.Battlefield);

            rvAttackerRunes.UpdateLabels();
            rvDefenderRunes.UpdateLabels();

            this.ResumeLayout();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            battle.Round();

            if (battle.Finished)
            {
                if (MessageBox.Show(String.Format("You {0} \nDo you want to fight again?", battle.Win ? "won!" : "lost :("), "Fight over!", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    ResetBattle();
                }
            }
            RefreshDatabindings();
        }
        

        private void imageLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            //pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            //pictureBox1.Image = Properties.Resources.LoadingSpinner;

            if (log.IsDebugEnabled) log.DebugFormat("Loading image {0}", (string)e.Argument);
            e.Result = ImageLoader.GetSmallImageByName((string)e.Argument);
            // e.Result = ImageLoader.GetImage((string)e.Argument);
        }

        private void imageLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            //pictureBox1.Image = (Image)e.Result;
        }

        private void btnEditDefender_Click(object sender, EventArgs e)
        {
        }

        private void btnEditAttacker_Click(object sender, EventArgs e)
        {
        }

        private void btnResetBattle_Click(object sender, EventArgs e)
        {
            ResetBattle();
        }

        private void BattleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.Save();
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            if (battle.Finished) return;
            if (timerAutoBattle.Enabled)
            {
                timerAutoBattle.Stop();
                btnAuto.Text = "Auto";
            }
            else
            {
                timerAutoBattle.Start();
                btnAuto.Text = "Pause";
            }
        }

        private void timerAutoBattle_Tick(object sender, EventArgs e)
        {
            if (battle.Finished)
            {
                timerAutoBattle.Enabled = false;
                btnAuto.Text = "Finished";
                return;
            }

            battle.Round();

            RefreshDatabindings();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            // 1 = 1000 ms
            // 5 = 200 ms
            int ms = 200 * (6 - trackBar1.Value);
            timerAutoBattle.Interval = ms;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            core.history.Battle b = new core.history.Battle();
            b.AttackPlayer = new core.history.BattlePlayer();
            b.AttackPlayer.Cards = attacker.UserDeck.Cards.ToList();
            b.AttackPlayer.Runes = attacker.UserDeck.Runes.ToList();
            b.AttackPlayer.NickName = attacker.NickName;
            b.AttackPlayer.Level = attacker.Level.ToString();
            b.AttackPlayer.Avatar = attacker.Avatar;
            b.AttackPlayer.Sex = attacker.Sex;
            b.AttackPlayer.HP = attacker.HP.ToString();
            b.AttackPlayer.RemainHP = battle.AttackPlayer.CurrentHP.ToString();

            b.DefendPlayer = new core.history.BattlePlayer();
            b.DefendPlayer.Cards = defender.UserDeck.Cards.ToList();
            b.DefendPlayer.Runes = defender.UserDeck.Runes.ToList();
            b.DefendPlayer.NickName = defender.NickName;
            b.DefendPlayer.Level = defender.Level.ToString();
            b.DefendPlayer.Avatar = defender.Avatar;
            b.DefendPlayer.Sex = defender.Sex;
            b.DefendPlayer.HP = defender.HP.ToString();
            b.DefendPlayer.RemainHP = battle.DefendPlayer.CurrentHP.ToString();

            b.BattleId = ElementalKingdoms.Util.GetFilenameFriendlyCurrentTime() + ElementalKingdoms.server.ServerHashGenerator.GetMd5(DateTime.Now.ToFileTime().ToString());
            b.Rounds = battle.Rounds;

            foreach (var card in battle.summonedCards.Values)
            {
                if (card.UUID.StartsWith("atk_"))
                {
                    b.AttackPlayer.Cards.Add(card);
                }
                else
                {
                    b.DefendPlayer.Cards.Add(card);
                }
            }

            util.JsonLoader.SaveBattle(b, "cache/battle/" + b.BattleId + ".json");

            MessageBox.Show("Replay saved in " + "cache/battle/" + b.BattleId + ".json");
        }
    }
}
