﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ElementalKingdoms.core;

namespace ElementalKingdoms.ui
{
    public partial class RuneSearcher : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public UserRune SelectedRune { get; set; }
        private List<UserRune> UserRunes;
        private List<UserRune> allRunes;
        
        public RuneSearcher(UserRune preSelectedRune, List<UserRune> userRunes)
        {
            InitializeComponent();

            UserRunes = userRunes;

            allRunes = new List<UserRune>();
            foreach (var rune in Rune.Runes)
            {
                allRunes.Add(new UserRune() { RuneId = rune.Id });
            }

            SelectedRune = new UserRune();
            SelectedRune.Level = 4;

            lbRunes.DisplayMember = "Name";

            if (preSelectedRune != null)
            {
                SelectedRune.RuneId = preSelectedRune.RuneId;
                SelectedRune.Level = preSelectedRune.Level;

                tbFilter.Text = preSelectedRune.Name;
            }

            if (userRunes == null || userRunes.Count == 0)
            {
                cbDisplayOnlyOwnedRunes.Checked = false;
                cbDisplayOnlyOwnedRunes.Enabled = false;
            }

            UpdateFilter();
            //UpdateLabels();

            this.ActiveControl = tbFilter;
        }

        private void UpdateFilter()
        {
            List<UserRune> runes = allRunes;
            if (cbDisplayOnlyOwnedRunes.Checked)
            {
                runes = UserRunes;
            }

            if (!cbStars1.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Stars != 1);
            }
            if (!cbStars2.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Stars != 2);
            }
            if (!cbStars3.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Stars != 3);
            }
            if (!cbStars4.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Stars != 4);
            }
            if (!cbStars5.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Stars != 5);
            }

            if (!cbElementEarth.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Element != Element.Earth);
            }
            if (!cbElementWater.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Element != Element.Water);
            }
            if (!cbElementAir.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Element != Element.Air);
            }
            if (!cbElementFire.Checked)
            {
                runes = runes.FindAll(x => x.BaseRune.Element != Element.Fire);
            }

            if (tbFilter.Text.Length > 0)
            {
                runes = runes.FindAll(x => x.BaseRune.Name.IndexOf(tbFilter.Text, StringComparison.CurrentCultureIgnoreCase) != -1
                    || x.GetSkill().Name.IndexOf(tbFilter.Text, StringComparison.CurrentCultureIgnoreCase) != -1);
            }

            lbRunes.DataSource = runes;
        }

        private void tbFilter_TextChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }

        private void cbAny_CheckedChanged(object sender, EventArgs e)
        {
            UpdateFilter();
        }

        private void lbRunes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var c = lbRunes.SelectedItem as UserRune;
            if (c.BaseRune.Id != SelectedRune.RuneId)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Changing selected rune to {0}", c.Name);
                SelectedRune.RuneId = c.BaseRune.Id;
                if (cbDisplayOnlyOwnedRunes.Checked)
                {
                    //SelectedRune.UserRuneId = c.UserRuneId;
                    //SelectedRune.Level = c.Level;
                    numLevel.Value = c.Level;
                }
                //UpdateLabels();
            }
        }

        private void numLevel_ValueChanged(object sender, EventArgs e)
        {
            SelectedRune.Level = Decimal.ToInt32(numLevel.Value);
        }

    }
}
