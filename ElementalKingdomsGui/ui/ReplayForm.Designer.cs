﻿namespace ElementalKingdoms.ui
{
    partial class ReplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReplayForm));
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.lblAttackerName = new System.Windows.Forms.Label();
			this.lblDefenderName = new System.Windows.Forms.Label();
			this.lblRound = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lblQuickStats = new System.Windows.Forms.Label();
			this.lbAttackerDeck = new System.Windows.Forms.ListBox();
			this.lbDefenderDeck = new System.Windows.Forms.ListBox();
			this.lbAttackerCemetary = new System.Windows.Forms.ListBox();
			this.lbDefenderCemetary = new System.Windows.Forms.ListBox();
			this.lblAttackerHP = new System.Windows.Forms.Label();
			this.lblDefenderHP = new System.Windows.Forms.Label();
			this.tbRounds = new System.Windows.Forms.TrackBar();
			this.label1 = new System.Windows.Forms.Label();
			this.lblRounds = new System.Windows.Forms.Label();
			this.tbOps = new System.Windows.Forms.TextBox();
			this.lblDropFileHint = new System.Windows.Forms.Label();
			this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			this.replayFolderViewer1 = new ElementalKingdomsGui.ui.replay.ReplayFolderViewer();
			this.battleFieldDefender = new ElementalKingdoms.ui.BattleField();
			this.battleFieldAttacker = new ElementalKingdoms.ui.BattleField();
			this.handViewerDefender = new ElementalKingdoms.ui.HandViewer();
			this.handViewerAttacker = new ElementalKingdoms.ui.HandViewer();
			this.rvDefenderRunes = new ElementalKingdoms.ui.BattleRuneViewer();
			this.rvAttackerRunes = new ElementalKingdoms.ui.BattleRuneViewer();
			((System.ComponentModel.ISupportInitialize)(this.tbRounds)).BeginInit();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(0, 459);
			this.textBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textBox1.Size = new System.Drawing.Size(1503, 185);
			this.textBox1.TabIndex = 0;
			// 
			// lblAttackerName
			// 
			this.lblAttackerName.AutoSize = true;
			this.lblAttackerName.Location = new System.Drawing.Point(9, 228);
			this.lblAttackerName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblAttackerName.Name = "lblAttackerName";
			this.lblAttackerName.Size = new System.Drawing.Size(70, 13);
			this.lblAttackerName.TabIndex = 50;
			this.lblAttackerName.Text = "Player NAME";
			// 
			// lblDefenderName
			// 
			this.lblDefenderName.AutoSize = true;
			this.lblDefenderName.Location = new System.Drawing.Point(9, 178);
			this.lblDefenderName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblDefenderName.Name = "lblDefenderName";
			this.lblDefenderName.Size = new System.Drawing.Size(88, 13);
			this.lblDefenderName.TabIndex = 49;
			this.lblDefenderName.Text = "Opponent NAME";
			// 
			// lblRound
			// 
			this.lblRound.AutoSize = true;
			this.lblRound.Location = new System.Drawing.Point(393, 412);
			this.lblRound.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblRound.Name = "lblRound";
			this.lblRound.Size = new System.Drawing.Size(13, 13);
			this.lblRound.TabIndex = 46;
			this.lblRound.Text = "1";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(358, 412);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(39, 13);
			this.label3.TabIndex = 45;
			this.label3.Text = "Round";
			// 
			// lblQuickStats
			// 
			this.lblQuickStats.AutoSize = true;
			this.lblQuickStats.Location = new System.Drawing.Point(412, 244);
			this.lblQuickStats.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblQuickStats.Name = "lblQuickStats";
			this.lblQuickStats.Size = new System.Drawing.Size(0, 13);
			this.lblQuickStats.TabIndex = 44;
			// 
			// lbAttackerDeck
			// 
			this.lbAttackerDeck.FormattingEnabled = true;
			this.lbAttackerDeck.Location = new System.Drawing.Point(9, 354);
			this.lbAttackerDeck.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbAttackerDeck.Name = "lbAttackerDeck";
			this.lbAttackerDeck.Size = new System.Drawing.Size(107, 56);
			this.lbAttackerDeck.TabIndex = 42;
			// 
			// lbDefenderDeck
			// 
			this.lbDefenderDeck.FormattingEnabled = true;
			this.lbDefenderDeck.Location = new System.Drawing.Point(9, 10);
			this.lbDefenderDeck.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbDefenderDeck.Name = "lbDefenderDeck";
			this.lbDefenderDeck.Size = new System.Drawing.Size(107, 56);
			this.lbDefenderDeck.TabIndex = 41;
			// 
			// lbAttackerCemetary
			// 
			this.lbAttackerCemetary.FormattingEnabled = true;
			this.lbAttackerCemetary.Location = new System.Drawing.Point(844, 319);
			this.lbAttackerCemetary.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbAttackerCemetary.Name = "lbAttackerCemetary";
			this.lbAttackerCemetary.Size = new System.Drawing.Size(132, 69);
			this.lbAttackerCemetary.TabIndex = 40;
			// 
			// lbDefenderCemetary
			// 
			this.lbDefenderCemetary.FormattingEnabled = true;
			this.lbDefenderCemetary.Location = new System.Drawing.Point(845, 33);
			this.lbDefenderCemetary.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.lbDefenderCemetary.Name = "lbDefenderCemetary";
			this.lbDefenderCemetary.Size = new System.Drawing.Size(131, 69);
			this.lbDefenderCemetary.TabIndex = 39;
			// 
			// lblAttackerHP
			// 
			this.lblAttackerHP.AutoSize = true;
			this.lblAttackerHP.Location = new System.Drawing.Point(53, 214);
			this.lblAttackerHP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblAttackerHP.Name = "lblAttackerHP";
			this.lblAttackerHP.Size = new System.Drawing.Size(61, 13);
			this.lblAttackerHP.TabIndex = 38;
			this.lblAttackerHP.Text = "Player LIFE";
			// 
			// lblDefenderHP
			// 
			this.lblDefenderHP.AutoSize = true;
			this.lblDefenderHP.Location = new System.Drawing.Point(52, 193);
			this.lblDefenderHP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblDefenderHP.Name = "lblDefenderHP";
			this.lblDefenderHP.Size = new System.Drawing.Size(79, 13);
			this.lblDefenderHP.TabIndex = 37;
			this.lblDefenderHP.Text = "Opponent LIFE";
			// 
			// tbRounds
			// 
			this.tbRounds.AutoSize = false;
			this.tbRounds.Location = new System.Drawing.Point(0, 428);
			this.tbRounds.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tbRounds.Maximum = 5;
			this.tbRounds.Minimum = 1;
			this.tbRounds.Name = "tbRounds";
			this.tbRounds.Size = new System.Drawing.Size(961, 26);
			this.tbRounds.TabIndex = 58;
			this.tbRounds.Value = 1;
			this.tbRounds.Scroll += new System.EventHandler(this.tbRounds_Scroll);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 412);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(13, 13);
			this.label1.TabIndex = 59;
			this.label1.Text = "1";
			// 
			// lblRounds
			// 
			this.lblRounds.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.lblRounds.Location = new System.Drawing.Point(1118, 430);
			this.lblRounds.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblRounds.Name = "lblRounds";
			this.lblRounds.Size = new System.Drawing.Size(166, 14);
			this.lblRounds.TabIndex = 60;
			this.lblRounds.Text = "1";
			this.lblRounds.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbOps
			// 
			this.tbOps.Location = new System.Drawing.Point(844, 133);
			this.tbOps.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.tbOps.Multiline = true;
			this.tbOps.Name = "tbOps";
			this.tbOps.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.tbOps.Size = new System.Drawing.Size(385, 150);
			this.tbOps.TabIndex = 61;
			// 
			// lblDropFileHint
			// 
			this.lblDropFileHint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.lblDropFileHint.BackColor = System.Drawing.Color.Transparent;
			this.lblDropFileHint.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDropFileHint.ForeColor = System.Drawing.Color.Lime;
			this.lblDropFileHint.Location = new System.Drawing.Point(88, 425);
			this.lblDropFileHint.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblDropFileHint.Name = "lblDropFileHint";
			this.lblDropFileHint.Size = new System.Drawing.Size(1713, 197);
			this.lblDropFileHint.TabIndex = 62;
			this.lblDropFileHint.Text = "Drop a replay json file in this window to watch";
			this.lblDropFileHint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// backgroundWorker1
			// 
			this.backgroundWorker1.WorkerReportsProgress = true;
			this.backgroundWorker1.WorkerSupportsCancellation = true;
			this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
			this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
			this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
			// 
			// replayFolderViewer1
			// 
			this.replayFolderViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.replayFolderViewer1.Folder = "D:\\projects\\Games\\ElementalKingdomsGui\\bin\\Debug\\cache\\battle";
			this.replayFolderViewer1.Location = new System.Drawing.Point(1246, 10);
			this.replayFolderViewer1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.replayFolderViewer1.Name = "replayFolderViewer1";
			this.replayFolderViewer1.Size = new System.Drawing.Size(247, 444);
			this.replayFolderViewer1.TabIndex = 63;
			// 
			// battleFieldDefender
			// 
			this.battleFieldDefender.AutoScroll = true;
			this.battleFieldDefender.Location = new System.Drawing.Point(131, 93);
			this.battleFieldDefender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.battleFieldDefender.Name = "battleFieldDefender";
			this.battleFieldDefender.Size = new System.Drawing.Size(656, 122);
			this.battleFieldDefender.TabIndex = 54;
			// 
			// battleFieldAttacker
			// 
			this.battleFieldAttacker.AutoScroll = true;
			this.battleFieldAttacker.Location = new System.Drawing.Point(131, 220);
			this.battleFieldAttacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.battleFieldAttacker.Name = "battleFieldAttacker";
			this.battleFieldAttacker.Size = new System.Drawing.Size(656, 122);
			this.battleFieldAttacker.TabIndex = 53;
			// 
			// handViewerDefender
			// 
			this.handViewerDefender.Location = new System.Drawing.Point(131, 30);
			this.handViewerDefender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.handViewerDefender.Name = "handViewerDefender";
			this.handViewerDefender.Size = new System.Drawing.Size(631, 65);
			this.handViewerDefender.TabIndex = 52;
			// 
			// handViewerAttacker
			// 
			this.handViewerAttacker.Location = new System.Drawing.Point(131, 336);
			this.handViewerAttacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.handViewerAttacker.Name = "handViewerAttacker";
			this.handViewerAttacker.Size = new System.Drawing.Size(631, 65);
			this.handViewerAttacker.TabIndex = 51;
			// 
			// rvDefenderRunes
			// 
			this.rvDefenderRunes.Location = new System.Drawing.Point(22, 70);
			this.rvDefenderRunes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.rvDefenderRunes.Name = "rvDefenderRunes";
			this.rvDefenderRunes.Size = new System.Drawing.Size(98, 106);
			this.rvDefenderRunes.TabIndex = 48;
			// 
			// rvAttackerRunes
			// 
			this.rvAttackerRunes.Location = new System.Drawing.Point(22, 244);
			this.rvAttackerRunes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.rvAttackerRunes.Name = "rvAttackerRunes";
			this.rvAttackerRunes.Size = new System.Drawing.Size(98, 106);
			this.rvAttackerRunes.TabIndex = 47;
			// 
			// ReplayForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1503, 644);
			this.Controls.Add(this.lblDropFileHint);
			this.Controls.Add(this.replayFolderViewer1);
			this.Controls.Add(this.tbOps);
			this.Controls.Add(this.lblRounds);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tbRounds);
			this.Controls.Add(this.battleFieldDefender);
			this.Controls.Add(this.battleFieldAttacker);
			this.Controls.Add(this.handViewerDefender);
			this.Controls.Add(this.handViewerAttacker);
			this.Controls.Add(this.lblAttackerName);
			this.Controls.Add(this.lblDefenderName);
			this.Controls.Add(this.rvDefenderRunes);
			this.Controls.Add(this.rvAttackerRunes);
			this.Controls.Add(this.lblRound);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lblQuickStats);
			this.Controls.Add(this.lbAttackerDeck);
			this.Controls.Add(this.lbDefenderDeck);
			this.Controls.Add(this.lbAttackerCemetary);
			this.Controls.Add(this.lbDefenderCemetary);
			this.Controls.Add(this.lblAttackerHP);
			this.Controls.Add(this.lblDefenderHP);
			this.Controls.Add(this.textBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.MinimizeBox = false;
			this.Name = "ReplayForm";
			this.Text = "Replayer";
			((System.ComponentModel.ISupportInitialize)(this.tbRounds)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private BattleField battleFieldDefender;
        private BattleField battleFieldAttacker;
        private HandViewer handViewerDefender;
        private HandViewer handViewerAttacker;
        private System.Windows.Forms.Label lblAttackerName;
        private System.Windows.Forms.Label lblDefenderName;
        private BattleRuneViewer rvDefenderRunes;
        private BattleRuneViewer rvAttackerRunes;
        private System.Windows.Forms.Label lblRound;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblQuickStats;
        private System.Windows.Forms.ListBox lbAttackerDeck;
        private System.Windows.Forms.ListBox lbDefenderDeck;
        private System.Windows.Forms.ListBox lbAttackerCemetary;
        private System.Windows.Forms.ListBox lbDefenderCemetary;
        private System.Windows.Forms.Label lblAttackerHP;
        private System.Windows.Forms.Label lblDefenderHP;
        private System.Windows.Forms.TrackBar tbRounds;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRounds;
        private System.Windows.Forms.TextBox tbOps;
        private System.Windows.Forms.Label lblDropFileHint;
        private ElementalKingdomsGui.ui.replay.ReplayFolderViewer replayFolderViewer1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}