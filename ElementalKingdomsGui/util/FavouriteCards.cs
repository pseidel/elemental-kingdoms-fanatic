﻿using ElementalKingdoms.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdomsGui.util
{
    class FavouriteCards
    {
        public List<UserCard> Cards { get; set; }

        public int Count { get { return Cards.Count; } }

        public FavouriteCards()
        {
            Cards = new List<UserCard>();
        }

        public UserCard FindFavourite(UserCard card)
        {
            if (card == null) return null;

            foreach (var c in Cards)
            {
                if (c.CardId == card.CardId && c.Level == card.Level && c.SkillNew == card.SkillNew)
                {
                    return c;
                }
            }
            return null;
        }

        public bool IsFavourite(UserCard card)
        {
            return FindFavourite(card) != null;
        }

        public void Add(UserCard card)
        {
            if (card != null)
            {
                Cards.Add(new UserCard() { CardId = card.CardId, Level = card.Level, SkillNew = card.SkillNew });
            }
        }

        public void Remove(UserCard card)
        {
            Cards.Remove(FindFavourite(card));
        }
    }
}
