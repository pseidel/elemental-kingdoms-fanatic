﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElementalKingdoms.core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ElementalKingdomsGui.Properties;

namespace ElementalKingdomsGui.util
{
    class JsonLoader
    {

        public static RecentData LoadRecentData()
        {
            //string mapStagesPath = @"D:\projects\Games\ElementalKingdoms\replies\GetMapStageAll.json";
            string recentDataPath = "cache/recent_data.json";

            if (!File.Exists(recentDataPath)) return new RecentData();

            using (StreamReader streamReader = new StreamReader(recentDataPath, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                RecentData loadedData = JsonConvert.DeserializeObject<RecentData>(response);

                return loadedData;
            }
        }

        public static void SaveRecentData(RecentData data)
        {
            string json = JsonConvert.SerializeObject(data, Formatting.Indented);

            File.WriteAllText(@"cache/recent_data.json", json);
        }

        public static FavouriteCards LoadFavouriteCards()
        {
            string path = Path.Combine(Settings.Default.DataPath, "favourite_cards.json");

            if (!File.Exists(path)) return new FavouriteCards();

            using (StreamReader streamReader = new StreamReader(path, Encoding.UTF8))
            {
                var response = streamReader.ReadToEnd();

                FavouriteCards loadedData = JsonConvert.DeserializeObject<FavouriteCards>(response);

                return loadedData;
            }
        }

        public static void SaveFavouriteCards(FavouriteCards data)
        {
            string json = JsonConvert.SerializeObject(data, Formatting.Indented);

            string path = Path.Combine(Settings.Default.DataPath, "favourite_cards.json");

            File.WriteAllText(path, json);
        }
    }
}
