﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ElementalKingdoms;
using ElementalKingdoms.core;
using ElementalKingdoms.tools;
using ElementalKingdoms.ui;
using ElementalKingdomsGui.Properties;
using ElementalKingdomsGui.ui;

namespace ElementalKingdomsGui
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

#if DEBUG
            //Util.SetLoglevel(log4net.Core.Level.All);
#else
                //Util.SetLoglevel(log4net.Core.Level.Info);
#endif

            if (Settings.Default.UpgradeOldSettings)
            {
                Settings.Default.Upgrade();
                Settings.Default.UpgradeOldSettings = false;
                Settings.Default.Save();
                try
                {
                    System.IO.File.Delete(@"cache/results.json");
                }
                catch (Exception)
                {
                }
            }

            if (string.IsNullOrEmpty(Settings.Default.DeviceId))
            {
                Settings.Default.DeviceId = ElementalKingdoms.server.ServerCommunicator.GenerateDeviceId();
                Settings.Default.Save();
            }

            SetApplicationDirectory();

            MigrateDataFiles();

            //Console.WriteLine(Environment.CurrentDirectory);
            //Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

            //Environment.CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            //Console.WriteLine(Environment.CurrentDirectory);

            //if (!System.IO.Directory.Exists("Elemental Kingdoms Fanatic/cache"))
            //return;

            ElementalKingdoms.util.ImageLoader.DeviceId = Settings.Default.DeviceId;
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoadingForm());
            //Application.Run(new KingdomWarForm());

            //Application.Run(new ElementalKingdoms.MainForm());
            //Application.Run(new ServerLoginForm());
            //Application.Run(new MapStagesForm());
            //Application.Run(new ReplayForm());
            Application.Run(new BattleSelectForm());
            //Application.Run(new CounterdeckGeneratorForm());
        }

        static void SetApplicationDirectory()
        {
            if (string.IsNullOrEmpty(Settings.Default.DataPath))
            {
                Settings.Default.DataPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Magic Realms Fanatic");
                System.IO.Directory.CreateDirectory(Settings.Default.DataPath);
                Settings.Default.Save();
            }
        }

        static void MigrateDataFiles()
        {
            if (System.IO.Directory.Exists("cache"))
            {

            }

            if (System.IO.File.Exists("cache/custom_decks.json"))
            {
                if (System.IO.File.Exists(BattleSelectForm.CUSTOM_DECK_FILE))
                {
                    // if the new custom deck file already exists, we either need to merge or backup + overwrite
                    System.IO.File.Move(BattleSelectForm.CUSTOM_DECK_FILE, System.IO.Path.Combine(Settings.Default.DataPath, String.Format("custom_decks_backup_{0}.json", Util.GetFilenameFriendlyCurrentTime())));
                }

                System.IO.File.Move("cache/custom_decks.json", BattleSelectForm.CUSTOM_DECK_FILE);
            }
            //Console.WriteLine(Environment.CurrentDirectory);
            //Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));

            //Environment.CurrentDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            //Console.WriteLine(Environment.CurrentDirectory);

            //if (!System.IO.Directory.Exists("Elemental Kingdoms Fanatic/cache"))
            //return;
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return System.IO.Path.GetDirectoryName(path);
            }
        }
    }
}
