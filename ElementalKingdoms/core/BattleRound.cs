﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class BattleRound
    {
        [Newtonsoft.Json.JsonProperty("Round")]
        public int Number { get; set; }
        public bool isAttack { get; set; }
        public List<Op> Opps { get; set; }

        public BattleRound()
        {
            Opps = new List<Op>();
        }
    }
}
