﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public enum Race
    {
        Tundra = 1,
        Forest = 2,
        Swamp = 3,
        Mountain = 4,
        Neutral = 5,
        Gold = 95,
        Feast = 96,
        Special = 99,
        Demon = 100,
        Hydra = 97
    }

    public class Card
    {
        public static List<Card> Cards = new List<Card>();

        public static Card Get(int id)
        {
            if (Cards == null) { throw new Exception("Cards is not initialized"); }
            return Cards.Find(c => c.Id == id);
        }

        public static int GetIdFromName(string name)
        {
            Card c = Cards.Find(x => x.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            if (c == null)
            {
                throw new ArgumentException(String.Format("Could not find a card with name {0}", name));
            }
            return c.Id;
        }

        public static void PrintAll()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CardId\tCardName\tCost\tColor\tRace\tAttack\tWait\tSkill\tLockSkill1\tLockSkill2\tLockSkillDemon\tAttackArray\tHpArray\tExpArray");
            foreach (Card c in Cards)
            {
                sb.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\r\n", c.Id, c.Name, c.Cost, c.Stars, c.Race, c.Attack, c.Wait, c.Skill, c.LockSkill1, c.LockSkill2, c.LockSkillDemon, string.Join(", ", c.AttackArray), string.Join(", ", c.HpArray), string.Join(", ", c.ExpArray));
            }

            System.IO.File.WriteAllText(@"cache/all_cards.csv", sb.ToString());
        }

        [JsonProperty("CardId")]
        public int Id { get; set; }
        [JsonProperty("CardName")]
        public string Name { get; set; } = "";
        public int Cost { get; set; }
        [JsonProperty("Color")]
        public int Stars { get; set; }
        public Race Race { get; set; }
        public int Attack { get; set; }

        public int[] HpArray { get; set; }
        public int[] AttackArray { get; set; }
        public int[] ExpArray { get; set; }

        public int Wait { get; set; }

        [JsonIgnore]
        public int? Skill { get; set; }
        [JsonIgnore]
        public int? Skill2 { get; set; }

        [JsonProperty("Skill")]
        private string Skill_helper
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (Skill != null)
                {
                    sb.Append(Skill);
                    if (Skill2 != null)
                    {
                        sb.Append("_");
                        sb.Append(Skill2);
                    }
                }
                return sb.ToString();
            }
            set
            {
                if (value.Length > 0)
                {
                    string[] l = Regex.Split(value, "_");

                    if (l.Length > 0)
                    {
                        Skill = int.Parse(l[0]);
                    }
                    if (l.Length > 1)
                    {
                        Skill2 = int.Parse(l[1]);
                    }
                }
            }
        }


        [JsonIgnore]
        public int? LockSkill1 { get; set; }
        [JsonIgnore]
        public int? LockSkill1b { get; set; }

        [JsonProperty("LockSkill1")]
        private string LockSkill1_helper
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (LockSkill1 != null)
                {
                    sb.Append(LockSkill1);
                    if (LockSkill1b != null)
                    {
                        sb.Append("_");
                        sb.Append(LockSkill1b);
                    }
                }
                return sb.ToString();
            }
            set
            {
                if (value.Length > 0)
                {
                    string[] l = Regex.Split(value, "_");

                    if (l.Length > 0)
                    {
                        LockSkill1 = int.Parse(l[0]);
                    }
                    if (l.Length > 1)
                    {
                        LockSkill1b = int.Parse(l[1]);
                    }
                }
            }
        }

        [JsonIgnore]
        public int? LockSkill2 { get; set; }
        [JsonIgnore]
        public int? LockSkillDemon { get; set; }

        /// <summary>
        /// Helper setter for LockSkill2. For some reason, they decided that Demons / Hydra's (which have 4 skills in total)
        /// should use the LockSkill2 field for the last 2 skills, where they use an underscore to separate them. Ofcourse, the JsonParser does not
        /// approve since the _ does not parse to an int. Handling it like this, since it is cleaner then using a string for this field.
        /// </summary>
        [JsonProperty("LockSkill2")]
        private string LockSkill2_helper
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (LockSkill2 != null)
                {
                    sb.Append(LockSkill2);
                    if (LockSkillDemon != null)
                    {
                        sb.Append("_");
                        sb.Append(LockSkillDemon);
                    }
                }
                return sb.ToString();
            }
            set
            {
                if (value.Length > 0)
                {
                    string[] l = Regex.Split(value, "_");

                    if (l.Length > 0)
                    {
                        LockSkill2 = int.Parse(l[0]);
                    }
                    if (l.Length > 1)
                    {
                        LockSkillDemon = int.Parse(l[1]);
                    }
                }
            }
        }

        public int CanEvo { get; set; }

        [JsonIgnore]
        public int AlternateEvoCardId { get; set; }
        [JsonIgnore]
        public int AlternateEvoCardAmount { get; set; }

        /// <summary>
        /// Helper setter for AlternateEvoCardId and AlternateEvoCardAmount. 
        /// They really love to put multiple variables into a single variable using _ separators
        /// </summary>
        [JsonProperty("KeyCard")]
        private string AlternateEvoCard_helper
        {
            get
            {
                if (CanEvo == 1)
                {
                    return String.Format("{0}_{1}", AlternateEvoCardId, AlternateEvoCardAmount);
                }
                return "0";
            }
            set
            {
                if (value.Length > 0)
                {
                    string[] l = Regex.Split(value, "_");

                    if (l.Length > 0)
                    {
                        AlternateEvoCardId = int.Parse(l[0]);
                    }
                    if (l.Length > 1)
                    {
                        AlternateEvoCardAmount = int.Parse(l[1]);
                    }
                }
            }
        }

        /*
        {
        "CardId": "298",
        "CardName": "Mountain King",
        "Cost": "12",
        "Color": "5",
        "Race": "4",
        "Attack": "340",
        "Wait": "2",
        "Skill": "48",
        "LockSkill1": "88",
        "LockSkill2": "837",
        "ImageId": "0",
        "FullImageId": "0",
        "Price": "11200",
        "BaseExp": "1100",
        "MaxInDeck": "0",
        "SeniorPacket": "0",
        "MasterPacket": "0",
        "Maze": "0",
        "Robber": "0",
        "MagicCard": "0",
        "Boss": "0",
        "BossCounter": "0",
        "FactionCounter": "0",
        "Glory": "0",
        "FightMPacket": "0",
        "BigYearPacket": "1",
        "BigYearPacketRoll": "100",
        "StandardPacket": "0",
        "StandardPacketRoll": "100",
        "GuruPacket": "0",
        "GuruPacketRoll": "100",
        "ActivityRobber": "0",
        "ForceFightAuction": "0",
        "ForceAuctionInitPrice": "20000",
        "ForceFightExchange": "0",
        "ForceExchangeInitPrice": "10000",
        "IcePacket": "0",
        "ForestPacket": "0",
        "SwampPacket": "0",
        "VolcanoPacket": "0",
        "IcePacketRoll": "100",
        "ForestPacketRoll": "100",
        "SwampPacketRoll": "100",
        "VolcanoPacketRoll": "100",
        "CanDecompose": "0",
        "Rank": "10",
        "Fragment": "110",
        "ComposePrice": "500000",
        "DecomposeGet": "1",
        "ThieveChipWeight": "0",
        "MazeChipWeight": "0",
        "CanEvo": "1",
        "KeyCard": "286_3",
        "HpArray": [
          1300,
          1333,
          1366,
          1399,
          1432,
          1465,
          1498,
          1531,
          1564,
          1597,
          1630,
          1663,
          1696,
          1729,
          1762,
          1795
        ],
        "AttackArray": [
          340,
          370,
          400,
          430,
          460,
          490,
          520,
          550,
          580,
          610,
          640,
          670,
          700,
          730,
          760,
          790
        ],
        "ExpArray": [
          "0",
          "500",
          "1500",
          "3000",
          "5000",
          "7500",
          "12500",
          "18500",
          "25500",
          "33500",
          "48500",
          "66000",
          "86000",
          "108500",
          "133500",
          "163500"
        ]
      },
    */
    }
}
