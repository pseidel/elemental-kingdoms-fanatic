﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class BattleField : IList<BattleCard>
    {
        public List<BattleCard> All { get { return cards; } }
        List<BattleCard> cards;
        BattleCard[] cardPositions;

        public BattleField()
        {
            cards = new List<BattleCard>();
            cardPositions = cards.ToArray();
        }

        /// <summary>
        /// Clear all the empty spots.
        /// </summary>
        public void RemoveDeadCards()
        {
            cardPositions = cards.ToArray();
        }

        public int IndexOf(BattleCard item)
        {
            return Array.IndexOf(cardPositions, item);
        }

        public void Insert(int index, BattleCard item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public BattleCard this[int index]
        {
            get
            {
                if (index >= cardPositions.Length || index < 0) { return null; }
                return cardPositions[index];
            }
            set
            {
                cardPositions[index] = value;
            }
        }

        public void Add(BattleCard item)
        {
            cards.Add(item);
            bool needsCopy = false;
            for (int i = 0; i < cardPositions.Length; i++)
            {
                if (cardPositions[i] == null)
                {
                    needsCopy = true;
                    break;
                }
            }
            if (needsCopy)
            {
                Array.Resize(ref cardPositions, cardPositions.Length + 1);
                cardPositions[cardPositions.Length - 1] = item;
            }
            else
            {
                cardPositions = cards.ToArray();
            }
        }

        public void Clear()
        {
            cards.Clear();
            cardPositions = cards.ToArray();
        }

        public bool Contains(BattleCard item)
        {
            return cards.Contains(item);
        }

        public void CopyTo(BattleCard[] array, int arrayIndex)
        {
            cards.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return cards.Count; }
        }

        public bool IsReadOnly
        {
            get { return cardPositions.IsReadOnly; }
        }

        public bool Remove(BattleCard item)
        {
            int index = Array.IndexOf(cardPositions, item);
            if (index != -1)
            {
                cardPositions[index] = null;
            }
            return cards.Remove(item);
            throw new NotImplementedException();
        }

        public IEnumerator<BattleCard> GetEnumerator()
        {
            return cards.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return cards.GetEnumerator();
        }
    }
}
