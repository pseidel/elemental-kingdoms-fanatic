﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElementalKingdoms.core
{
    public class UserMapStage
    {
        public int Uid { get; set; }
        public int MapStageDetailId { get; set; }
        public int Type { get; set; }
        public int MapStageId { get; set; }
        public int FinishedStage { get; set; }
        public DateTime LastFinishedTime { get; set; }
        public int CounterAttackTime { get; set; }
    }
}
