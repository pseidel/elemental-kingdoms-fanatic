﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core.history
{
    public class BattlePlayer
    {
        public int Uid { get; set; }
        public String NickName { get; set; }
        public int Avatar { get; set; }
        public Gender Sex { get; set; }
        public string Level { get; set; }
        public string HP { get; set; }

        public List<UserCard> Cards { get; set; }
        public List<UserRune> Runes { get; set; }
        public string RemainHP { get; set; }
        /*
         * "AttackPlayer": {
			"Uid": 314173,
			"NickName": "JackieIsaac",
			"Avatar": 0,
			"Sex": 0,
			"Level": "1",
			"HP": "1000",
			"Cards": [{
				"UUID": "atk_1",
				"CardId": "31",
				"UserCardId": 21568014,
				"Attack": 85,
				"HP": 300,
				"Wait": "2",
				"Level": 0,
				"SkillNew": 0,
				"Evolution": 0,
				"WashTime": null
			},
			{
				"UUID": "atk_2",
				"CardId": "33",
				"UserCardId": 21568015,
				"Attack": 125,
				"HP": 370,
				"Wait": "2",
				"Level": 0,
				"SkillNew": 0,
				"Evolution": 0,
				"WashTime": null
			},
			{
				"UUID": "atk_3",
				"CardId": "33",
				"UserCardId": 21568016,
				"Attack": 125,
				"HP": 370,
				"Wait": "2",
				"Level": 0,
				"SkillNew": 0,
				"Evolution": 0,
				"WashTime": null
			}],
			"Runes": [],
			"RemainHP": "1000"
		},
		"DefendPlayer": {
			"Uid": 0,
			"NickName": "Forest Wolf",
			"Avatar": "4",
			"Sex": 0,
			"Level": "1",
			"HP": "1000",
			"Cards": [{
				"UUID": "def_1",
				"CardId": "4",
				"UserCardId": 0,
				"Attack": 110,
				"HP": 250,
				"Wait": "2",
				"Level": "0"
			},
			{
				"UUID": "def_2",
				"CardId": "4",
				"UserCardId": 1,
				"Attack": 110,
				"HP": 250,
				"Wait": "2",
				"Level": "0"
			}],
			"Runes": [],
			"RemainHP": 665
		},*/
    }
}
