﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core.history
{
    public class BattleState
    {
        public StatePlayer Attacker { get; private set; }
        public StatePlayer Defender { get; private set; }
        public List<string> Ops { get; private set; }
        public int Round { get; set; }
        public bool IsAttack { get; set; }

        public BattleState()
        {
            Attacker = new StatePlayer();
            Defender = new StatePlayer();
            Ops = new List<string>();
        }
        // BattleCards attacker, defender
        // BattleRunes attacker, defender
        // HP attacker, defender
        // ???
        // Profit

        public BattleState Clone()
        {
            BattleState s = new BattleState();
            s.Round = Round;

            s.Attacker.MaxHP = Attacker.MaxHP;
            s.Attacker.HP = Attacker.HP;
            s.Attacker.Cards = CopyCards(Attacker.Cards);
            s.Defender.MaxHP = Defender.MaxHP;
            s.Defender.HP = Defender.HP;
            s.Defender.Cards = CopyCards(Defender.Cards);
            s.IsAttack = IsAttack;

            return s;
        }

        private List<BattleCard> CopyCards(List<BattleCard> list)
        {
            List<BattleCard> cards = new List<BattleCard>();
            foreach (var c in list)
            {
                cards.Add(c.Clone());
            }

            return cards;
        }
    }

    public class StatePlayer
    {
        public List<BattleCard> Cards { get; set; }
        // runes
        public int MaxHP { get; set; }
        public int HP { get; set; }

        public StatePlayer()
        {
            Cards = new List<BattleCard>();
        }
    }
}
