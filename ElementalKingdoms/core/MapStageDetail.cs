﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class MapStageDetail
    {
        public int MapStageDetailId { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int MapStageId { get; set; }
        public int Rank { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Prev { get; set; }
        public int Next { get; set; }
        public int NextBranch { get; set; }
        public string FightName { get; set; }
        public int? FightImg { get; set; }
        // Dialog -> List?
        // DialogAfter -> List?
        public List<MapStageLevel> Levels { get; set; }

        public bool IsTower { get { return Prev == 0 && Next == 0; } }
    }
}
