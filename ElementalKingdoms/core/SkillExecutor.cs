﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class SkillExecutor
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Battle b;

        // s is the working skill, which might change in case of Quick Strike / Desperation
        private Skill s;
        // originalSkill always contains the original skill, which is needed for Fire God to see the difference between QS: FG10 and Desp: FG10 (since both will be internally converted to FG10)
        private readonly Skill originalSkill;
        private BattleUser you;
        private BattleUser opponent;
        private BattleCard source;

        private string sourceUUID;

        public SkillExecutor(Battle b, Skill s, BattleUser you, BattleUser opponent, BattleCard source)
        {
            this.b = b;
            this.s = s;
            this.originalSkill = s;
            this.you = you;
            this.opponent = opponent;
            this.source = source;

            if (source != null)
            {
                sourceUUID = source.Card.UUID;
            }

            ExecuteSkill();
        }

        private void ExecuteSkill()
        {

            switch (s.LanchType)
            {
                case LaunchType.BeforeAttack:
                    break;
                case LaunchType.AfterAttack:
                    break;
                case LaunchType.AfterDefend:
                    break;
                case LaunchType.OnSkillExecutePhase:
                    break;
                case LaunchType.BeforeDefend:
                    break;
                case LaunchType.WhileOnBattlefield:
                    break;
                case LaunchType.Regen:
                    break;
                case LaunchType.AfterDeath:
                    break;
                case LaunchType.OnDeath:
					if (s.AffectType == 53)
					{
						// Desperation: XXX, we are actually casting another skill (id is in AffectValue)
						s = Skill.Get(s.AffectValue);
					}
					else if (s.AffectType == (int)EffectType.DesperationReincarnate)
					{
						if (b.SoulImprisons > 0)
						{
							if (log.IsDebugEnabled) log.DebugFormat("Unable to reincarnate - Soul Imprison is active");
						}
						else
						{
							// Making a copy with ToList so we can safely remove items from the actual Cemetary.
							var reincarnateTargets = you.Cemetary.TakeRandom(s.AffectValue).ToList();
							//Reincarnation atk_4 [atk_10] --> 0
							foreach (var c in reincarnateTargets)
							{
								if (b.StoreReplay) b.AddOp(OpType.CardDies, sourceUUID, new List<string>() { c.Card.UUID }, 0);
								you.Cemetary.Remove(c);
								you.Hand.Add(c);
								//CemetaryToDeck atk_10 [atk_10] --> 0
								if (b.StoreReplay) b.AddOp(OpType.CemetaryToHand, c.Card.UUID, new List<string>() { c.Card.UUID }, 0);
							}
						}
					}
                    break;
                case LaunchType.OnEnterBattlefield:
                    if (s.AffectType == 59)
                    {
                        // Quick Strike: XXX, we are actually casting another skill (id is in AffectValue)
                        s = Skill.Get(s.AffectValue);
                    }
                    break;
                case LaunchType.OnElementalHit:
                    break;
                case LaunchType.OnInstagibHit:
                    break;
                case LaunchType.OnSkillHit:
                    break;
                case LaunchType.OnHeroDamaged:
                    break;
                case LaunchType.Runes:
                    UseRuneSkill();
                    return;
                default:
                    if (s.AffectType == 141 || s.AffectType == 142)
                    {
                        // Death Whisper: XXX and Preemptive Strike: XXX, we are actually casting another skill (id is in AffectValue)
                        s = Skill.Get(s.AffectValue);
                    }
                    break;
            }

			switch (s.AffectType)
			{
				case (int)EffectType.ConditionalMulti:
					{
						ConditionalMultiSkill();
						break;
					}
			}
			

            if (log.IsDebugEnabled) log.DebugFormat("Executing skill {0} {1} {2} {3} {4} {5} {6} {7}", s.Name, s.AffectType, s.LanchCondition, s.SkillCategory, s.Type, s.AffectValue, s.AffectValue2, s.AffectValueExtra);

            switch (s.Type)
            {
                case SkillType.Poison:
                case SkillType.Stun:
                case SkillType.Freeze:
                case SkillType.Damage:
                case SkillType.DamageAndHeal:
                case SkillType.Destroy:
                case SkillType.ManaCorruption:
                case SkillType.AttackDecrease:
                case SkillType.Confusion:
                case SkillType.AttackAndHealthDecrease:
                case SkillType.FrostShock:
                case SkillType.Asura:
                case SkillType.Crazy:
                case SkillType.Blind:
                case SkillType.SpellMark:
                case SkillType.ManaPuncture:
                case SkillType.EarthQuake:
                    FindDefenderTarget();
                    break;
                case SkillType.Trap:
                    Trap();
                    break;
                case SkillType.Heal:
                    Heal();
                    break;
                case SkillType.Snipe:
                    int numTargets = Math.Max(1, s.AffectValue2); // dual snipe has 2 filled in, but the single snipes have 0...

                    foreach (var target in opponent.Battlefield.OrderBy(x => x.CurrentHP).Take(numTargets))
                    {
                        if (target == null) continue; // There were enough targets at the start of the turn, but due to casualities there are not enough anymore (defenders array has null values) 
                        if (target.HasSensitive && Util.PercentChance(target.Sensitive))
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Sensitive triggered - escaping {0}", s.Name);
                        }
                        else
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Sniped {0} for {1}", target, s.AffectValue);
                            DamageCard(target, s.AffectValue);
                        }
                    }
                    break;
                case SkillType.AttackIncrease:
                    if (s.AffectType == 72)
                    {
                        // Mania
                        DamageCard(source, s.AffectValue);
                        source.CurrentAttack += s.AffectValue;
                    }
                    else if (s.AffectType == 111)
                    {
                        // Dying Strike
                        int damage = source.BaseAttack * s.AffectValue / 100;
                        int myIndex = you.Battlefield.IndexOf(source);
                        var target = opponent.Battlefield[myIndex];
                        if (target != null)
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Dying strike: hitting {0} for {1} (current attack={2}, bonus={3}%)", target.Name, damage, source.BaseAttack, s.AffectValue);
                            DamageCard(target, damage);
                        }
                    }
                    else
                    {
                        log.Warn("Unhandled attack increase skill");
                    }
                    break;
				case SkillType.ConjureCard:
					if (you.Deck.Count > 0)
					{
						if (log.IsDebugEnabled) log.DebugFormat("Conjure skill {0}", s.Name);

						int maxWait = 0;
						List<BattleCard> conjureTargets = new List<BattleCard>(10);
						foreach (var c in you.Deck)
						{
							if (c.CurrentWaitTime > maxWait)
							{
								conjureTargets.Clear();
								maxWait = c.CurrentWaitTime;
								conjureTargets.Add(c);
							}
							else if (c.CurrentWaitTime == maxWait)
							{
								conjureTargets.Add(c);
							}
						}
						Random r = new Random();
						int k = r.Next(0, conjureTargets.Count - 1);
						BattleCard conjureTarget = conjureTargets[k];

						if (b.StoreReplay) b.AddOp(OpType.ConjureCard, sourceUUID, new List<string>() { conjureTarget.Card.UUID }, 0);
						you.Deck.Remove(conjureTarget);
						you.Battlefield.Add(conjureTarget);
						if (b.StoreReplay) b.AddOp(OpType.DeckToBattlefield, sourceUUID, new List<string>() { conjureTarget.Card.UUID }, 0);
					}
					break;
                case SkillType.Resurrection:
                    if (you.Cemetary.Count > 0)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Resurrection skill {0}", s.Name);

                        if (s.AffectType == 44) // Reincarnate (Cemetary to deck)
                        {
                            if (b.SoulImprisons > 0)
                            {
                                if (log.IsDebugEnabled) log.DebugFormat("Unable to reincarnate - Soul Imprison is active");
                            }
                            else
                            {
                                // Making a copy with ToList so we can safely remove items from the actual Cemetary.
                                var reincarnateTargets = you.Cemetary.TakeRandom(s.AffectValue).ToList();
                                //Reincarnation atk_4 [atk_10] --> 0
                                foreach (var c in reincarnateTargets)
                                {
                                    if (b.StoreReplay) b.AddOp(OpType.Reincarnation, sourceUUID, new List<string>() { c.Card.UUID }, 0);
                                    you.Cemetary.Remove(c);
                                    you.Deck.Add(c);
                                    //CemetaryToDeck atk_10 [atk_10] --> 0
                                    if (b.StoreReplay) b.AddOp(OpType.CemetaryToDeck, c.Card.UUID, new List<string>() { c.Card.UUID }, 0);
                                }
                            }
                        }
                        else if (s.AffectType == 27) // Resurrection (Cemetary to hand)
                        {
                            if (b.Suppressors > 0)
                            {
                                if (log.IsDebugEnabled) log.DebugFormat("Unable to resurrect - Supressing is active");
                            }
                            else
                            {
                                if (!you.Cemetary.Contains(source))
                                {
                                    // Already resurrected - this can happen if the card itself has resurrection and the Dirt rune is active.
                                    if (log.IsDebugEnabled) log.Debug("Already resurrected");
                                }
                                else
                                if (Util.PercentChance(s.AffectValue))
                                {
                                    if (log.IsDebugEnabled) log.DebugFormat("Resurrected!");
                                    you.Cemetary.Remove(source);
                                    if (you.Hand.Count >= Battle.MAX_HAND_SIZE)
                                    {
                                        if (b.StoreReplay) b.AddOp(OpType.BattlefieldToLibrary, sourceUUID, new List<string>() { sourceUUID }, 0);
                                        you.Deck.Add(source);
                                    }
                                    else
                                    {
                                        if (b.StoreReplay) b.AddOp(OpType.CemetaryToHand, sourceUUID, new List<string>() { sourceUUID }, 0);
                                        you.Hand.Add(source);
                                    }
                                }
                            }
                        }
                        else if (s.AffectType == 39) // Reanimation (Cemetary to Battlefield)
                        {
                            Reanimation();
                        }
                        else
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Unhandled resurrection skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
                        }
                        //var validReanimationTargets = you.Cemetary.FindAll(x => !x.HasImmunity && !x.HasReanimation);
                        //if (validReanimationTargets.Count > 0)
                        //{
                        //    BattleCard bc = validReanimationTargets.TakeRandom(1).ElementAt(0);
                        //    if (log.IsDebugEnabled) log.DebugFormat("Reanimating {0}", bc);
                        //    bc.Reset();
                        //    bc.OnDeath += b_OnDeath;
                        //    // TODO: Fire Quick Strike skills
                        //    you.Cemetary.Remove(bc);
                        //    you.Battlefield.Add(bc);

                        //    List<BattleCard> tmp = new List<BattleCard>() { bc };
                        //    you.Battlefield.FindAll(x => x != bc).ForEach(y => ApplyBattlefieldBuffs(y, tmp));
                        //}
                    }
                    break;
                case SkillType.Selfdestruct:
                    SelfDestruct();
                    break;
                case SkillType.HeroHeal:
                    HealHero();
                    break;
                case SkillType.HeroDamage:
                    if (log.IsDebugEnabled) log.DebugFormat("Hero damage");
                    if (s.AffectType == 34)
                    {
                        // Obstinacy
                        b.DamageHero(sourceUUID, you, s.AffectValue, true);
                        if (log.IsDebugEnabled) log.DebugFormat("Obstinacy - Dealing {0} damage (HP down to {1}", s.AffectValue, you.CurrentHP);
                        // TODO: Check if that didn't kill us.

                    }
                    else
                    {
                        // Curse and Damnation
                        int damage = s.AffectValue;
                        if (s.AffectType == 67)
                        {
                            // Damnation
                            damage *= opponent.Battlefield.Count;
                        }
                        if (log.IsDebugEnabled) log.DebugFormat("Dealing {0} damage to the hero", damage);
                        b.DamageHero(sourceUUID, opponent, damage, true);
                    }
                    break;
                case SkillType.Teleportation:
                    Teleportation();
                    break;
                case SkillType.Burn:
                    Burn();
                    break;
                case SkillType.Sacrifice:
                    if (you.Battlefield.Count > 1)
                    {
                        var sac = you.Battlefield.All.FindAll(x => x != source).TakeRandom(1).First();
                        if (log.IsDebugEnabled) log.DebugFormat("Saccing: {0}", sac);
                        if (sac.HasImmunity)
                        {
                            if (log.IsDebugEnabled) log.Debug("NOT");
                        }
                        else
                        {
                            b.Sacrifice(sourceUUID, sac);
                            source.BonusSkillHP += source.CurrentHP * s.AffectValue / 100;
                            source.BonusSkillAttack += source.CurrentAttack * s.AffectValue / 100;
                        }
                    }
                    break;
                case SkillType.SacredFlame:
                    if (b.SoulImprisons > 0)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Unable to use Sacred Flame - Soul Imprison is active");
                    }
                    else
                    {
                        if (opponent.Cemetary.Count > 0)
                        {
                            var target = opponent.Cemetary.TakeRandom(1).First();
                            if (log.IsDebugEnabled) log.DebugFormat("Sacred Flame: Removing {0} from the cemetary", target);
                            //SacredFlame def_5 [atk_1] --> 0
                            //CemetaryToDeck atk_1 [atk_1] --> 0
                            if (b.StoreReplay) b.AddOp(OpType.SacredFlame, sourceUUID, new List<string>() { target.Card.UUID }, 0);
                            if (b.StoreReplay) b.AddOp(OpType.CemetaryToDeck, target.Card.UUID, new List<string>() { target.Card.UUID }, 0);
                            opponent.Cemetary.Remove(target);
                        }
                    }
                    break;
                case SkillType.WaitTimeReduction:
                    ReduceWaitTime();
                    break;
                case SkillType.WaitTimeIncrease:
                    IncreaseWaitTime();
                    break;
                case SkillType.Roar:
                    Roar();
                    break;
                case SkillType.DeathMarker:
                    DeathMarker();
                    break;
                case SkillType.Silence:
                    Silence();
                    break;
                case SkillType.Purification:
                    Purify();
                    break;
                case SkillType.HolyLight:
                case SkillType.Naturalize:
                case SkillType.Corruption:
                case SkillType.LavaTrial:
                    ChangeRace();
                    break;
                case SkillType.SummonCards:
                    SummonCards();
                    break;
                case SkillType.PreventRecycle:
                    break;
                case SkillType.PercentageHeal:
                    PercentageHeal();
                    break;
                case SkillType.Exile:
                    Exile();
                    break;
                case SkillType.LastChance:
                    // Skills that are handled elsewhere (should be moved here though at some point)
                    break;
                case SkillType.Puncture:
                    Puncture();
                    break;
                default:
                    log.WarnFormat("Unhandled skill {0} {1} {2} {3}", s.Name, s.LanchCondition, s.SkillCategory, s.Type);
                    break;
            }
        }

        private void Puncture()
        {
            if (s.AffectType == 143) // Banish
            {
                if (log.IsDebugEnabled) log.DebugFormat("Banish - removing opponent summoned cards.");
                foreach (var card in opponent.Battlefield.All.ToList())
                {
                    if (card.IsSummoned)
                    {
                        b.Destroy(sourceUUID, card);
                    }
                }
            }
        }

        private void Reanimation()
        {
            if (b.SoulImprisons > 0)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Soul Imprison is active, unable to reanimate");
            }
            else if (you.Cemetary.Count > 0)
            {
				//TD
				// Modified: var validReanimationTargets = you.Cemetary.FindAll(x => !x.HasImmunity && !x.HasReanimation);
				var validReanimationTargets = you.Cemetary.FindAll(x => !x.HasReanimation);
                if (validReanimationTargets.Count > 0)
                {
                    BattleCard bc = validReanimationTargets.TakeRandom(1).ElementAt(0);
                    if (log.IsDebugEnabled) log.DebugFormat("Reanimating {0}", bc);
                    bc.CurrentWaitTime = 0;
                    b.ToBattlefield(bc, you);
                    if (b.StoreReplay) b.AddOp(OpType.CemetaryToBattlefield, bc.Card.UUID, new List<string>() { bc.Card.UUID }, 0);
                }
            }
        }

        private void Exile()
        {
            // Exile, Time Reverse and Get Out have SkillType 'Exile'

            if (s.AffectType == 42) // Exile
            {
                int myPos = you.Battlefield.IndexOf(source);
                var target = opponent.Battlefield[myPos];

                if (target != null)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("{0} -> Exiling {1} @ pos: {2} ", s.Name, target, myPos);
                    if (b.StoreReplay) b.AddOp(OpType.Exile, sourceUUID, new List<string>() { target.Card.UUID }, 0);

                    if (target.HasImmunity)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Immune");
                    }
                    else if (target.HasResistance)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Resistance");
                    }
                    else
                    {
                        Exile(target);
                    }
                }
            }
            else if (s.AffectType == 95) // Time Reverse
            {
                if (source.TimeReverseAvailable)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("{0} -> Reversing time...", s.Name);
                    // Upon entering the battlefield, except this card, all cards on battlefield and in both players' hand will go back to decks. 
                    // Ignores Immunity. This skill doesn't work against Demons. This card can only cast this skill once in a battle.
                    /*
                    Exile def_5 [atk_8,atk_15,atk_3,atk_14,atk_2,atk_9,def_6,def_9,def_14,def_15,def_13,atk_5,atk_7,atk_6,def_8,def_3] --> 0
                    BattlefieldToLibrary atk_8 [def_5] --> 0
                    BattlefieldToVoid atk_15 [def_5] --> 0
                    BattlefieldToLibrary atk_3 [def_5] --> 0
                    BattlefieldToVoid atk_14 [def_5] --> 0
                    BattlefieldToLibrary atk_2 [def_5] --> 0
                    BattlefieldToLibrary atk_9 [def_5] --> 0
                    BattlefieldToLibrary def_6 [def_5] --> 0
                    BattlefieldToLibrary def_9 [def_5] --> 0
                    CardModifyAttack def_14 [def_15] --> -175
                    CardModifyAttack def_14 [def_13] --> -175
                    BattlefieldToVoid def_14 [def_5] --> 0
                    BattlefieldToVoid def_15 [def_5] --> 0
                    BattlefieldToVoid def_13 [def_5] --> 0
                    1010 atk_5 [def_5] --> 0
                    1010 atk_7 [def_5] --> 0
                    1010 atk_6 [def_5] --> 0
                    1010 def_8 [def_5] --> 0
                    1010 def_3 [def_5] --> 0
                    */
                    var targets = new List<BattleCard>();
                    targets.AddRange(b.AttackPlayer.Battlefield);
                    targets.AddRange(b.DefendPlayer.Battlefield);
                    targets.AddRange(b.AttackPlayer.Hand);
                    targets.AddRange(b.DefendPlayer.Hand);
                    targets.Remove(source); // Skip yourself
                    targets.RemoveAll(x => x.Race == Race.Demon || x.Race == Race.Hydra); // TODO: Check if demons / hydra's get targeted but give an 'immune' or if they won't get targeted at all

                    if (b.StoreReplay) b.AddOp(OpType.Exile, sourceUUID, targets.Select(x => x.Card.UUID).ToList(), 0);

                    targets.ForEach(target => Exile(target));
                    source.TimeReverseAvailable = false;
                }
                else
                {
                    if (log.IsDebugEnabled) log.DebugFormat("{0} -> Already used this battle", s.Name);
                }
            }
            else if (s.AffectType == 135) // Get Out
            {
                // TODO: Need to implement
            }
        }

        private void Exile(BattleCard target)
        {
            b.ToDeck(sourceUUID, target);
        }

        private void HealHero()
        {
            int amount = s.AffectValue;
            if (s.AffectType == 102) // Bless, percentage heal instead of fixed value
            {
                amount = you.MaxHP * s.AffectValue / 100;
            }
            if (log.IsDebugEnabled) log.DebugFormat("Healing hero for {0}", amount);
            b.HealHero(sourceUUID, you, amount);
        }

        private void DeathMarker()
        {
            int myIndex = you.Battlefield.IndexOf(source);
            if (myIndex == -1)
            {
                log.ErrorFormat("Could not find {0} in battlefield", source);
                return;
            }

            SkillHit(opponent.Battlefield[myIndex]);
        }

        private void Roar()
        {
            int numTargets = s.AffectValue;
            /* Dread Roar has affect value 10, but has 'all cards' in its description. In case someone mentions that 
             it really affects all targets, uncomment these lines
            if (numTargets == 10)
            {
                numTargets = opponent.Battlefield.Count;
            }
            */

                foreach (var target in opponent.Battlefield.All.TakeRandom(numTargets))
            {
                SkillHit(target);
            }
        }

        private void Purify()
        {
            foreach (var card in you.Battlefield)
            {
                if (card.Trapped)
                {
                    card.Trapped = false;
                    if (b.StoreReplay) b.AddOp(OpType.Trapped, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Paralyzed)
                {
                    card.Paralyzed = false;
                    if (b.StoreReplay) b.AddOp(OpType.Paralyzed, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Frozen)
                {
                    card.Frozen = false;
                    if (b.StoreReplay) b.AddOp(OpType.Frozen, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Confused)
                {
                    card.Confused = false;
                    if (b.StoreReplay) b.AddOp(OpType.Confused, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Stunned)
                {
                    card.Stunned = false;
                    if (b.StoreReplay) b.AddOp(OpType.Stunned, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.Poisoned)
                {
                    card.Poisons.Clear();
                    if (b.StoreReplay) b.AddOp(OpType.Poisoned, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.Laceration)
                {
                    card.Laceration = false;
                    if (b.StoreReplay) b.AddOp(OpType.Lacerated, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.Burned)
                {
                    card.Burns.Clear();
                    if (b.StoreReplay) b.AddOp(OpType.Burned, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
                if (card.Roared)
                {
                    card.Roared = false;
                    if (b.StoreReplay) b.AddOp(OpType.Roared, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.DeathMark > 0)
                {
                    card.DeathMark = 0;
                    if (b.StoreReplay) b.AddOp(OpType.DeathMarked, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }

                if (card.Blinded)
                {
                    card.Blind = 0;
                    if (b.StoreReplay) b.AddOp(OpType.Blinded, card.Card.UUID, new List<string>() { card.Card.UUID }, 0);
                }
            }
        }

        private void SummonCards()
        {
            // A summoned card will not summon more cards
            if (source.IsSummoned)
            {
                return;
            }
            string summonNameAffix = String.Format("{0}_{1}_", source.Card.UUID, s.GetHashCode());
            BattleCard existing = you.Battlefield.All.Find(x => x.Card.UUID.StartsWith(summonNameAffix));
			if (this.source.Name == "Gemini")
			{
				s.AffectValue = 431;		
			}
			if (this.source.Name == "Cloris")
			{
				s.AffectValue = 513;
				s.AffectValue2 = 513;
			}
			if (existing == null)
            {
                SummonCard(s.AffectValue, summonNameAffix + "0");
                SummonCard(s.AffectValue2, summonNameAffix + "1");
            }
            else
            {
                if (log.IsDebugEnabled) log.DebugFormat("Summon {0} {1} is already on the field", summonNameAffix, existing.Name);
            }
        }

        private void SummonCard(int cardId, string summonName)
        {
            if (cardId == 0)
            {
                return;
            }
            if (Card.Get(cardId) == null)
            {
                log.ErrorFormat("Summoning card with unknown or invalid id: {0}", cardId);
                return;
            }
            if (log.IsDebugEnabled) log.DebugFormat("Summoning card with internal name " + summonName);

            UserCard c = new UserCard();
            c.CardId = cardId;
            c.Level = 10;
            c.UUID = summonName;

            BattleCard b = new BattleCard(c)
            {
                IsSummoned = true,
                Owner = you
            };

            this.b.SummonCard(b, you);
        }

        private void ChangeRace()
        {
            foreach (var card in opponent.Battlefield)
            {
                if (card.Race != Race.Demon && card.Race != Race.Hydra)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("Setting {0}'s race to {1}", card.Name, (Race)s.AffectValue);
                    card.Race = (Race)s.AffectValue;
                }
                else
                {
                    if (log.IsDebugEnabled) log.DebugFormat("{0}'s race cannot be changed", card.Name);
                }
            }
        }

        private void Silence()
        {
			if (s.AffectValue2 > 0)
			{
				foreach (var card in opponent.Battlefield)
				{
					SkillHit(card);
				}
			}
			else
			{
				int myIndex = you.Battlefield.IndexOf(source);
				if (myIndex == -1)
				{
					log.ErrorFormat("Could not find {0} in battlefield", source);
					return;
				}
				SkillHit(opponent.Battlefield[myIndex]);
			}
        }

        private void SelfDestruct()
        {
            int myIndex = you.Battlefield.IndexOf(source);
            if (myIndex == -1)
            {
                log.ErrorFormat("Could not find {0} in battlefield", source);
                return;
            }

            SkillHit(opponent.Battlefield[myIndex]);
            SkillHit(opponent.Battlefield[myIndex - 1]); // left
            SkillHit(opponent.Battlefield[myIndex + 1]); // right
        }

        private void Burn()
        {
            if (s.AffectType == 45)
            {
                // hmm, I have no idea who actually attacked me. I can guess, but it could be via Clean Sweep...
                //BurnTarget()
            }
            else
            {
                foreach (var target in opponent.Battlefield)
                {
                    BurnTarget(target);
                }
            }
        }

        private void BurnTarget(BattleCard target)
        {
            if (target.HasImmunity)
            {
                if (log.IsDebugEnabled) log.DebugFormat("{0} is immune!", target.Name);
                return;
            }
            string src = "";
            if (source == null)
            {
                src += "RUNE";
            }
            else
            {
                src += source.Name + source.GetHashCode();
            }
            src += "_" + originalSkill.Name;
            target.AddBurn(src, s.AffectValue);
            if (b.StoreReplay) b.AddOp(OpType.Burned, sourceUUID, new List<string>() { target.Card.UUID }, 1);
        }

        private void IncreaseWaitTime()
        {
            if (opponent.Hand.Count == 0) return;
            if (s.AffectType == 81)
            {
                // Impede - 
                var target = opponent.Hand.OrderBy(x => x.CurrentWaitTime).First();
                if (log.IsDebugEnabled) log.DebugFormat("Increasing wait time for {0} by {1}", target, s.AffectValue);
                target.CurrentWaitTime += s.AffectValue;
            }
            else if (s.AffectType == 93)
            {
                // Mass Attrition
                foreach (var target in opponent.Hand)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("Increasing wait time for {0} by {1}", target, s.AffectValue);
                    target.CurrentWaitTime += s.AffectValue;
                }
            }
            else
            {
                log.WarnFormat("Unknown AffectType {0} for skill {1} (id: {2})", s.AffectType, s.Name, s.SkillId);
            }
        }

        private void ReduceWaitTime()
        {
            if (you.Hand.Count == 0) return;
            if (s.AffectType == 80)
            {
                // Advanced Strike
                var target = you.Hand.OrderByDescending(x => x.CurrentWaitTime).First();
                if (log.IsDebugEnabled) log.DebugFormat("Reducing wait time for {0} by {1}", target, s.AffectValue);
                target.CurrentWaitTime += s.AffectValue;
            }
            else if (s.AffectType == 92)
            {
                // Warcry
                foreach (var target in you.Hand)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("Reducing wait time for {0} by {1}", target, s.AffectValue);
                    target.CurrentWaitTime += s.AffectValue;
                }
            }
            else
            {
                log.WarnFormat("Unknown AffectType {0} for skill {1} (id: {2})", s.AffectType, s.Name, s.SkillId);
            }
        }


        //private void Exile(int i, BattleCard defendingCard)
        //{
        //    if (defendingCard != null)
        //    {
        //        if (log.IsDebugEnabled) log.DebugFormat("Exiling {0}", defendingCard);
        //        if (defendingCard.HasImmunity || defendingCard.HasResistance)
        //        {
        //            if (log.IsDebugEnabled) log.DebugFormat("NOT");
        //        }
        //        else
        //        {
        //            defendingCard.OnDeath -= b_OnDeath;
        //            CleanupBuffs(defendingCard, false);
        //            TargetPlayer.Battlefield.Remove(defendingCard);
        //            TargetPlayer.Deck.Add(defendingCard);
        //        }
        //    }
        //}

        private void Teleportation()
        {
            //Casts the card with the longest waiting time in the opponent's hand directly into their cemetery.
            if (opponent.Hand.Count > 0)
            {
                var target = opponent.Hand.OrderByDescending(x => x.CurrentWaitTime).First();
                if (b.StoreReplay) b.AddOp(OpType.Teleportation, sourceUUID, new List<string>() { target.Card.UUID }, 0);
                if (log.IsDebugEnabled) log.DebugFormat("Teleportating {0} from hand to the cemetary", target);
                if (target.HasImmunity)
                {
                    // Immune atk_4 [atk_4] --> 0
                    if (log.IsDebugEnabled) log.DebugFormat("Immune");
                    if (b.StoreReplay) b.AddOp(OpType.Immune, target.Card.UUID, new List<string>() { target.Card.UUID }, 0);
                }
                else if (target.HasResistance)
                {
                    // Resistance def_5 [def_5] --> 0
                    if (log.IsDebugEnabled) log.DebugFormat("Resistance");
                    if (b.StoreReplay) b.AddOp(OpType.Resistance, target.Card.UUID, new List<string>() { target.Card.UUID }, 0);
                }
                else
                {
                    // CardDies atk_6 [] --> 0
                    if (b.StoreReplay) b.AddOp(OpType.CardDies, target.Card.UUID, new List<string>() { }, 0);
                    opponent.Hand.Remove(target);
                    opponent.Cemetary.Add(target);
                }
            }
        }

        private void UseRuneSkill()
        {
            if (s.AffectType == 60)
            {
                // Add the skill in s.AffectValue to every card you have.
                Skill applySkill = Skill.Get(s.AffectValue);

                foreach (var card in you.Battlefield)
                {
                    card.AddRuneSkill(applySkill);
                }
            }
            else if (s.AffectType == 61)
            {
                // Shield: Allows one card on your side to ward off X damage in the following round.
                //log.WarnFormat("Shield not implemented");
                if (you.Battlefield.Count > 0)
                {
                    you.Battlefield.All.TakeRandom(1).First().AddRuneSkill(s);
                }
            }
            else if (s.AffectType == 62)
            {
                // Barricade: Allows all cards on your side to ward off X damage in the following round.
                //log.WarnFormat("Barricade not implemented");
                foreach (var card in you.Battlefield)
                {
                    card.AddRuneSkill(s);
                }
            }
            else if (s.AffectType == 63)
            {
                // Inspiration: Increases the Attack of one card on your battlefield by X.
                // Unused I believe...
                log.WarnFormat("Inspiration not implemented");
            }
            else if (s.AffectType == 64)
            {
                // Group Morale: Increases the Attack of ALL cards on your battlefield by X.
                //log.WarnFormat("Group Morale not implemented");
                foreach (var card in you.Battlefield)
                {
                    card.AddRuneSkill(s);
                }
            }
        }


        private void Heal()
        {
            if (log.IsDebugEnabled) log.DebugFormat("Handling heal {0}", s.Name);
            if (source != null)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Source: {0}", source.Name);
            }
            else
            {
                if (log.IsDebugEnabled) log.DebugFormat("No source");
            }

            IEnumerable<BattleCard> targets = null;
            if (s.AffectType == 20)
            {
                // Healing
                targets = you.Battlefield.OrderByDescending(x => x.BaseHP - x.CurrentHP).Take(1);
            }
            else if (s.AffectType == 21)
            {
                // Regeneration
                targets = you.Battlefield;
            }
            else if (s.AffectType == 83)
            {
                // healing mists
                List<BattleCard> tmp = new List<BattleCard>(3);
                tmp.Add(source);

                int i = you.Battlefield.IndexOf(source);
                if (i > 1)
                {
                    tmp.Add(you.Battlefield[i - 1]);
                }

                if (i < you.Battlefield.Count - 1)
                {
                    tmp.Add(you.Battlefield[i + 1]);
                }
                targets = tmp;
            }

            if (targets != null)
            {
                foreach (var target in targets)
                {
					//TD
					//Changed: if (target != null && !target.Laceration && !target.HasImmunity)
					if (target != null && !target.Laceration)
                    {
                        HealCard(target, s.AffectValue);
                    }
                }
            }
        }
        
        private void PercentageHeal()
        {
            if (log.IsDebugEnabled) log.DebugFormat("Handling heal {0}", s.Name);
            if (source != null)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Source: {0}", source.Name);
            }
            else
            {
                if (log.IsDebugEnabled) log.DebugFormat("No source");
            }

            if (s.AffectType == 119)
            {
                // Diana's Touch
                var target = you.Battlefield.OrderByDescending(x => x.BaseHP - x.CurrentHP).Take(1).First();
                int healingAmount = (target.BaseHP + target.BonusSkillHP) * s.AffectValue / 100;
                HealCard(target, healingAmount);
            }
            else
            {
                // Diana's Protection

                foreach (var target in you.Battlefield)
                {
                    if (target != null && !target.Laceration)
                    {
                        int healingAmount = (target.BaseHP + target.BonusSkillHP) * s.AffectValue / 100;
                        HealCard(target, healingAmount);
                    }
                }
            }
        }

		private void ConditionalMultiSkill()
		{
			switch (s.AffectValue)
			{
				case (int)ConditionCheckLocation.AttackerCheck:
					{
						switch (s.AffectValueExtra)
						{
							case (int)ConditionCheckSubLocations.HandSize:
								{
									if (you.Hand.Count == s.AffectValueExtra2)
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
							case (int)ConditionCheckSubLocations.CardsOnBattlefield:
								{
									if (you.Battlefield.Count > s.AffectValueExtra2)
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
							case (int)ConditionCheckSubLocations.CardsWithImmunityCount:
								{
									if (you.Battlefield.Count + opponent.Battlefield.Count > s.AffectValueExtra2)
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
							case (int)ConditionCheckSubLocations.HeroHP:
								{
									if (you.CurrentHP > (int)(you.MaxHP * ((double)s.AffectValueExtra2 / (double)100)))
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
						}
						break;
					}
				case (int)ConditionCheckLocation.DefenderCheck:
					{
						switch (s.AffectValueExtra)
						{
							case (int)ConditionCheckSubLocations.HandSize:
								{
									if (opponent.Hand.Count == s.AffectValueExtra2)
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
							case (int)ConditionCheckSubLocations.CardsOnBattlefield:
								{
									if (opponent.Battlefield.Count > s.AffectValueExtra2)
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
							case (int)ConditionCheckSubLocations.CardsWithImmunityCount:
								{
									if (you.Battlefield.Count + opponent.Battlefield.Count > s.AffectValueExtra2)
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
							case (int)ConditionCheckSubLocations.HeroHP:
								{
									if (opponent.CurrentHP > (int)(opponent.MaxHP * ((double)s.AffectValueExtra2 / (double)100)))
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										new SkillExecutor(b, Skill.Get(s.AffectValue2Extra), you, opponent, source);
									}
									break;
								}
						}
						break;
					}
				case (int)ConditionCheckLocation.BattlefieldCheck:
					{
						switch (s.AffectValueExtra)
						{
							case (int)ConditionCheckSubLocations.CardsWithImmunityCount:
								{
									if (you.Battlefield.Count + opponent.Battlefield.Count > s.AffectValueExtra2)
									{
										Skill condSkill = Skill.Get(s.AffectValue2);
										new SkillExecutor(b, Skill.Get(s.AffectValue2), you, opponent, source);
									}
									else
									{
										Skill condSkill = Skill.Get(s.AffectValue2Extra);
										new SkillExecutor(b, condSkill, you, opponent, source);
									}
									break;
								}
						}
						break;
					}
			}
		}


		private void Trap()
        {
            if (s.AffectType == 48 && source.FirstAttack == false)
            {
                // Seal only 1 time
                return;
            }

            int numTargets = s.AffectValue;
            if (numTargets == 0)
            {
                numTargets = opponent.Battlefield.Count;
            }

            foreach (var target in opponent.Battlefield.All.TakeRandom(numTargets))
            {
                if (target == null)
                {
                    // Don't think this can happen anymore now that I am looking at Battlefield instead of defenders, keeping logging anyway.
                    log.ErrorFormat("I am skipping a trap target, but not looking for a new one.");
                    continue;
                }
                SkillHit(target);
            }
        }

        private void FindDefenderTarget()
        {
            if (opponent.Battlefield.Count == 1)
            {
                SkillHit(opponent.Battlefield.All.First());
                return;
            }

            int numTargets = 1;
            if (s.AffectType == 7 // Chain Lightning
                || s.AffectType == 10 // Nova Frost
                || s.AffectType == 13 // Fire Wall
                || s.AffectType == 17 // Smog
                || s.AffectType == 114 // Mana Burn
                || s.AffectType == 151 // Mana Break
                )
            {
                numTargets = 3;
            }
            else if (s.AffectType == 8 // Electric Shock
                || s.AffectType == 11 // Blizzard
                || s.AffectType == 14 // Firestorm
                || s.AffectType == 18 // Toxic Clouds
                || s.AffectType == 29 // Group Weaken
                || s.AffectType == 38 // Plague
                || s.AffectType == 85 // Feast of Blood
                || s.AffectType == 96 // Frost Shock
                || s.AffectType == 107 // Flash
                || s.AffectType == 121 // Asura's Flame
                || s.AffectType == 145 // Earthquake
                )
            {
                numTargets = opponent.Battlefield.Count;
            }
            else if (s.AffectType == 69) // Confusion / Field of Chaos )
            {
                if (s.AffectValue2 == 0) // Confusion
                {
                    numTargets = 1;
                }
                else // Field of Chaos
                {
                    numTargets = s.AffectValue2;
                }
            }
            else if (s.AffectType == 109) // Spell Mark / Magic Arrays
            {
                numTargets = s.AffectValue2;
            }

            List<BattleCard> targets;

            if (numTargets < opponent.Battlefield.Count)
            {
                targets = opponent.Battlefield.TakeRandom(numTargets).ToList();
            }
            else
            {
                targets = opponent.Battlefield.All.ToList();
                numTargets = targets.Count;
            }

            for (int i = 0; i < numTargets; i++)
            {
                if (targets[i] == null) continue;
                if (log.IsDebugEnabled) log.DebugFormat("Selected target {0}", targets[i].Card.UUID);
                SkillHit(targets[i]);
            }
        }


        private void SkillHit(BattleCard target)
        {
            if (target == null)
            {
                // Invalid target
                return;
            }
            if (log.IsDebugEnabled) log.DebugFormat("{0} is getting a {1} thrown by {2}", target.Name, s.Name, source);

            // Check if we should apply a Spell Mark percentage to the damage done. This multiplier should only be applied for certain types of spells, like Reflect.
            bool applySpellMark = false;

            if (s.Type == SkillType.Stun || s.Type == SkillType.Freeze || s.Type == SkillType.DamageAndHeal || s.Type == SkillType.Damage || s.Type == SkillType.FrostShock || s.Type == SkillType.Asura)
            {
                if (target.HasReflection)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("{0} has reflection. OUCH {1} fails!", target, s);
                    if (source != null)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("{0} damage dealt", target.ReflectDamage);
                        DamageCard(source, target.ReflectDamage);
                    }
                    return;
                }

                if (target.SpellMark > 0)
                {
                    applySpellMark = true;
                }
            }

            if (target.HasImmunity)
            {
                if (s.Type == SkillType.Roar)
                {
                    // Roar skips immunity, but doesn't work against demons
                    if (target.Race == Race.Demon || target.Race == Race.Hydra)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Roar - Demon / Hydra immunity!");
                        return;
                    }
                }
                else
                if (s.Type != SkillType.ManaCorruption && s.Type != SkillType.ManaPuncture && s.Type != SkillType.Silence && s.Type != SkillType.EarthQuake)
                {
                    // pretty much everything is ignored if I have immunity.
                    if (log.IsDebugEnabled) log.DebugFormat("Immune!");
                    return;
                }
            }

            if (target.LastChanceActive)
            {
                if (s.Type != SkillType.Destroy)
                {
                    if (log.IsDebugEnabled) log.Debug("Ignored - last chance is active");
                    return;
                }
            }

            // Most of the skills use AffectValue for base damage, so we just set it here. 
            int damage = s.AffectValue;

            if (applySpellMark)
            {
                damage += damage * target.SpellMark / 100;
            }

            switch (s.Type)
            {
                case SkillType.Trap:
                    if (log.IsDebugEnabled) log.DebugFormat("Trapping / sealing");
                    if (!target.HasEvasion)
                    {
                        if (s.AffectType == 48 || Util.PercentChance(65))
                        {
                            target.Trapped = true;
                            if (b.StoreReplay) b.AddOp(OpType.Trapped, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                            if (log.IsDebugEnabled) log.Debug("Success");
                        }
                        else
                        {
                            if (log.IsDebugEnabled) log.Debug("Failed");
                        }
                    }
                    break;
                case SkillType.Stun:
                    damage = ApplyMagicShield(target, damage);

                    DamageCard(target, damage);

                    if (!target.HasEvasion)
                    {
                        if (Util.PercentChance(s.AffectValue2))
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Stunned!");
                            target.Paralyzed = true;
                            if (b.StoreReplay) b.AddOp(OpType.Paralyzed, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                        }
                    }
                    break;
                case SkillType.Freeze:
                    damage = ApplyMagicShield(target, damage);

                    DamageCard(target, damage);

                    if (!target.HasEvasion)
                    {
                        if (Util.PercentChance(s.AffectValue2))
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Frozen!");
                            target.Frozen = true;
                            if (b.StoreReplay) b.AddOp(OpType.Frozen, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                        }
                    }
                    break;
                case SkillType.Damage:
                    damage = ThreadSafeRandom.ThisThreadsRandom.Next(s.AffectValue, 1 + s.AffectValue * 2);
                    if (log.IsDebugEnabled) log.DebugFormat("Fire damage {0}-{1} => {2}", s.AffectValue, s.AffectValue * 2, damage);
                    if (applySpellMark)
                    {
                        // Recalculate as this case does not have fixed damage
                        damage += damage * target.SpellMark / 100;
                    }

                    damage = ApplyMagicShield(target, damage);

                    DamageCard(target, damage);
                    break;
                case SkillType.DamageAndHeal:
                    damage = Math.Min(target.CurrentHP, damage);

                    damage = ApplyMagicShield(target, damage);

                    if (log.IsDebugEnabled) log.DebugFormat("Blood attack for {0}", damage);
                    DamageCard(target, damage);
                    HealCard(source, damage);
                    break;
                case SkillType.Poison:
                    DamageCard(target, s.AffectValue);
                    if (b.StoreReplay) b.AddOp(OpType.Poisoned, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                    target.Poisons.Add(s.AffectValue);
                    break;
                case SkillType.AttackDecrease:
                    if (s.AffectValue / 10 > 11)
                    {
                        // Something went wrong
                        throw new ArgumentException("Trying to execute Quick Strike or Desperation: Group Weaken as a normal skill");
                    }
                    target.CurrentAttack -= s.AffectValue;
                    break;
                case SkillType.Selfdestruct:
                    DamageCard(target, s.AffectValue);
                    break;
                case SkillType.AttackAndHealthDecrease:
                    DamageCard(target, s.AffectValue);
                    target.CurrentAttack -= s.AffectValue;
                    break;
                case SkillType.Destroy:
                    if (!(target.HasImmunity || target.HasResistance))
                    {
                        b.Destroy(sourceUUID, target);
                    }
                    else
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Resist / immune");
                    }
                    break;
                case SkillType.Confusion:
                    if (!target.HasEvasion)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Confusing...{0}% chance", s.AffectValue);
                        if (Util.PercentChance(s.AffectValue))
                        {
                            target.Confused = true;
                            if (b.StoreReplay) b.AddOp(OpType.Confused, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                            if (log.IsDebugEnabled) log.Debug("Success");
                        }
                        else
                        {
                            if (log.IsDebugEnabled) log.Debug("Failed");
                        }
                    }
                    break;
                case SkillType.ManaCorruption:
                    if (log.IsDebugEnabled) log.DebugFormat("Mana Corruption");
                    if (target.HasSensitive && Util.PercentChance(target.Sensitive))
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Sensitive triggered - escaped {0}", s.Name);
                    }
                    else
                    {
                        damage = s.AffectValue;
                        if (target.HasImmunity || target.HasReflection)
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Target has immunity or reflection. 300% damage.");
                            damage *= 3;
                        }
                        DamageCard(target, damage);
                    }
                    break;
                case SkillType.Roar:
                    if (log.IsDebugEnabled) log.Debug("Roared");
                    target.Roared = true;
                    if (b.StoreReplay) b.AddOp(OpType.Roared, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                    break;
                case SkillType.DeathMarker:
                    if (log.IsDebugEnabled) log.Debug("Marked for death");
                    if (s.AffectValue > 1)
                    {
                        log.WarnFormat("Death Marker with more then 1 turn is not supported yet.");
                    }
                    target.DeathMark += s.AffectValue2;
                    if (b.StoreReplay) b.AddOp(OpType.DeathMarked, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                    break;
                case SkillType.Silence:
                    if (target.HasSpiritualVoice || target.BaseRace == Race.Demon || target.BaseRace == Race.Hydra)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Spiritual Voice - Immune to silence!");
                    }
                    else
                    {
                        target.Silence();
                    }
                    break;
                case SkillType.FrostShock:
                    damage = s.AffectValue + opponent.Battlefield.Count * s.AffectValue2;

                    if (applySpellMark)
                    {
                        // Recalculate as this case does not have fixed damage
                        damage += damage * target.SpellMark / 100;
                    }

                    damage = ApplyMagicShield(target, damage);

                    DamageCard(target, damage);

                    if (!target.HasEvasion)
                    {
                        if (Util.PercentChance(50))
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Frozen!");
                            target.Frozen = true;
                            if (b.StoreReplay) b.AddOp(OpType.Frozen, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                        }
                    }
                    break;
                case SkillType.Blind:
                    if (log.IsDebugEnabled) log.DebugFormat("Blinding {0}%", s.AffectValue);
                    // TODO: What happens when multiple blinds are being applied with different values? I assume that the highest one will count.
                    // Also, replays show that when multiple blinds are applied, multiple are also removed. So not sure how that will all add up.
                    target.Blind = s.AffectValue;
                    if (b.StoreReplay) b.AddOp(OpType.Blinded, sourceUUID, new List<string>() { target.Card.UUID }, 1);
                    break;
                case SkillType.Crazy:
                    damage = target.CurrentTotalAttack;
                    if (log.IsDebugEnabled) log.Debug("Crazy");
                    int index = opponent.Battlefield.IndexOf(target);

                    var left = opponent.Battlefield[index - 1];
                    if (left != null)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Crazy hitting left target {0} {1} for {2}", left.Card.UUID, left.Name, damage);
                        DamageCard(left, damage);
                    }

                    var right = opponent.Battlefield[index + 1];
                    if (right != null)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Crazy hitting right target {0} {1} for {2}", right.Card.UUID, right.Name, damage);
                        DamageCard(right, damage);
                    }
                    break;
                case SkillType.Asura:
                    damage = s.AffectValue + opponent.Battlefield.Count * s.AffectValue2;

                    if (applySpellMark)
                    {
                        // Recalculate as this case does not have fixed damage
                        damage += damage * target.SpellMark / 100;
                    }

                    if (log.IsDebugEnabled) log.DebugFormat("Hitting for {0} damage", damage);

                    DamageCard(target, damage);
                    break;
                case SkillType.ManaPuncture:
                    if (log.IsDebugEnabled) log.DebugFormat("Mana Puncture");
                    if (target.HasSensitive && Util.PercentChance(target.Sensitive))
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Sensitive triggered - escaped {0}", s.Name);
                    }
                    else
                    {
                        damage = s.AffectValue;
                        if (target.HasImmunity || target.HasReflection)
                        {
                            if (log.IsDebugEnabled) log.DebugFormat("Target has immunity or reflection. {0}% damage.", s.AffectValue2);
                            damage *= s.AffectValue2 / 100;
                        }
                        DamageCard(target, damage);
                    }
                    break;
                case SkillType.SpellMark:
                    if (log.IsDebugEnabled) log.DebugFormat("Applying Spell Mark...");
                    // If the target already has a spell mark, use the biggest one. Don't think he can get spell mark AND magic arrays either.
                    target.SpellMark = Math.Max(target.SpellMark, s.AffectValue);
                    break;
                case SkillType.EarthQuake:
                    if (log.IsDebugEnabled) log.DebugFormat("Earthquake...");
                    if (target.HasResistance)
                    {
                        damage = s.AffectValue2;
                    }
                    DamageCard(target, damage);

                    break;
                default:
                    log.WarnFormat("Unhandled skill hit: {0}", s);
                    break;
            }
        }

        private int ApplyMagicShield(BattleCard target, int damage)
        {
            if (target.HasSpellSpellReduction)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Spell Block trigged");
                damage -= damage * s.AffectValue2 / 100;
            }

            if (target.HasMagicShield)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Magical shield trigged");
                damage = Math.Min(target.MagicShield, damage);
            }

            return damage;
        }

        private void DamageCard(BattleCard target, int damage)
        {
            if (log.IsDebugEnabled) log.DebugFormat("Hitting {0} ({1}) for {2} damage", target.Name, target.Card.UUID, damage);
            b.DamageCard(sourceUUID, target, damage);
        }

        private void HealCard(BattleCard target, int amount)
        {
            b.HealCard(sourceUUID, target, amount);
        }
    }
}
