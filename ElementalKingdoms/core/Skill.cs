﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public enum SkillType
    {
        PercentageDamageHeal = 2,
        CounterAttack = 3,
        Trap = 4,
        Stun = 5,
        Freeze = 6,
        Damage = 7,
        DamageAndHeal = 8,
        Poison = 9,
        DamageReduction = 10,
        Heal = 11,
        Snipe = 12,
        MultiHit = 13,
        AttackIncrease = 14,
        HPIncrease = 15,
        Resurrection = 16,
        AttackDecrease = 17,
        Selfdestruct = 18,
        HeroHeal = 19,
        HeroDamage = 20,
        AttackIncreaseAfterDealingDamage = 21,
        AttackIncreaseAfterReceivingDamage = 22,
        AttackAndHealthDecrease = 23,
        Teleportation = 24,
        Reflection = 25,
        Exile = 26,
        Destroy = 27,
        Burn = 28,
        IceShield = 29,
        Laceration = 30,
        MagicShield = 31,
        Resistance = 32,
        Immunity = 33,
        Guard = 34,
        Dodge = 35,
        Sacrifice = 36,
        Puncture = 37,
        Confusion = 38,
        Leech = 39,
        ManaCorruption = 40,
        SacredFlame = 41,
        WaitTimeReduction = 42,
        WaitTimeIncrease = 43,
        Roar = 44,
        DeathMarker = 45,
        Silence = 46,
        Purification = 47,
        LastChance = 48,
        HolyLight = 49,
        Naturalize = 50,
        Corruption = 51,
        LavaTrial = 52,
        DivineShield = 53, // The first physical attack against this card will be nullified.
        FrostShock = 54,
        ShieldOfEarth = 55,
        SummonCards = 56,
		ConjureCard = 58, // Gets card with highest wait time from deck onto battlefield

        Blind = 60,
        SpellMark = 61, // Spell Mark and Magic Arrays
        PreventRecycle = 62, // Suppressing and Soul Imprison
        Crazy = 63,
        SummonWeapon = 64,
        PercentageHeal = 65,
        Asura = 66,

        ColdBlood = 68,
        SpiritualVoice = 69,
        WaterShield = 70,
        BreakIce = 71,

        EarthQuake = 73,
        ManaPuncture = 74, // Mana Puncture and Mana Break

        NonCombat = 99,
    }

    public enum LaunchType
    {
        BeforeAttack = 1,
        AfterAttack = 2,
        AfterDefend = 3,
        OnSkillExecutePhase = 4,
        BeforeDefend = 5,
        WhileOnBattlefield = 6,
        Regen = 7,
        AfterDeath = 8,
        OnDeath = 9,
        OnEnterBattlefield = 10,
        OnElementalHit = 11,
        OnInstagibHit = 12,
        OnSkillHit = 13,
        OnHeroDamaged = 14,
        Runes = 15,
        OnEnterBattleFieldTimeReverse = 16,
        OnStartOfTurn = 17,
    }

    public enum LaunchCondition
    {
        Always = 0,
        BeforeDamage = 1,
        AfterDamage = 2,
        ReceivedDamage = 3,
        TargetHasMoreHealth = 4,
        TargetHasAffliction = 5
    }

	public enum EffectType
	{
		AttackerAllCardsBuff = 60,
		MultiSkill = 122,
		ConditionalMulti = 158,
		DesperationReincarnate = 176
	}

	public enum ConditionCheckLocation
	{
		DefenderCheck = 1002,
		AttackerCheck = 1001,
		BattlefieldCheck = 1000
	}

	public enum ConditionCheckSubLocations
	{
		HandSize = 3,
		CardsOnBattlefield = 1,
		HeroHP = 5,
		CardsWithImmunityCount = 6
	}

    public class Skill
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static List<Skill> skills = new List<Skill>();
        public static List<Skill> Skills 
        {
            get { return skills; }
            set { skills = value; ClearCache(); } 
        }

        private static Dictionary<int, Skill> _skillLookup = null;

        public static Skill Get(int id)
        {
            if (id == 0) { return null; }
            if (Skills == null) { throw new Exception("Skills is not initialized"); }
            if (_skillLookup == null)
            {
                InitCache();
            }
            try
            {
                return _skillLookup[id];// Skills.Find(s => s.SkillId == id);
            }
            catch (Exception ex)
            {
                // Unknown skill most likely
                log.WarnFormat("Caught exception while trying to get skill with id {0} - {1}", id, ex.Message);
            }
            return new Skill() { Name = "UNKOWN " + id };// return a bogus skill that does nothing. Later on, we might want to return null instead.
        }

        public static void InitCache()
        {
            _skillLookup = new Dictionary<int, Skill>();

            foreach (Skill s in Skills)
            {
                _skillLookup.Add(s.SkillId, s);
            }
        }

        public static void ClearCache()
        {
            _skillLookup = null;
        }

        public int SkillId { get; set; }
        public string Name { get; set; }
        public SkillType Type { get; set; }
        public LaunchType LanchType { get; set; }
        public LaunchCondition LanchCondition { get; set; }
        public int LanchConditionValue { get; set; }
        public int AffectType { get; set; }

        [JsonIgnore]
        public int AffectValue { get; set; }
        [JsonIgnore]
        public int AffectValueExtra { get; set; }
        [JsonIgnore]
        public int AffectValueExtra2 { get; set; }
		[JsonIgnore]
		public int[] AffectValueExtra3 { get; set; }

		[JsonProperty("AffectValue")]
        private string AffectValueHelper
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(AffectValue);
                if (AffectValueExtra != 0)
                {
                    sb.Append("_");
                    sb.Append(AffectValueExtra);
                }
                if (AffectValueExtra2 != 0)
                {
                    sb.Append("_");
                    sb.Append(AffectValueExtra2);
				}
				if (AffectValueExtra3 != null)
				{
					sb.Append("_");
					sb.Append(AffectValueExtra3);
				}
				return sb.ToString();
            }
            set
            {
                if (value.Length > 0)
                {
                    string[] l = value.Split('_', ',');

					if (l[0].StartsWith("%"))
					{
						l[0] = l[0].Remove(0, 1);
					}
					if (l[0].EndsWith("%"))
					{
						l[0] = l[0].Remove(l[0].Length-1);
					}

					if (l.Length > 0)
                    {
                        AffectValue = int.Parse(l[0]);
                    }
                    if (l.Length > 1)
                    {
                        AffectValueExtra = int.Parse(l[1]);
                    }
                    if (l.Length > 2)
                    {
						if (l[2].Split('&').Length > 1)
						{
							AffectValue2Extra3 = new int[l[2].Split('&').Length];
							for (int i = 0; i < l[2].Split('&').Length; i++)
							{
								AffectValue2Extra3[i] = int.Parse(l[2].Split('&')[i]);
							}
						}
						else
						{
							AffectValueExtra2 = int.Parse(l[2]);
							AffectValue2Extra3 = null;
						}
                        
                    }
                }
            }
        }

        [JsonIgnore]
        public int AffectValue2 { get; set; }
        [JsonIgnore]
        public int AffectValue2Extra { get; set; }
        [JsonIgnore]
        public int AffectValue2Extra2 { get; set; }
		[JsonIgnore]
		public int[] AffectValue2Extra3 { get; set; }

		[JsonProperty("AffectValue2")]
        private string AffectValue2Helper
        {
            get
            {
                StringBuilder sb = new StringBuilder();
				sb.Append(AffectValue2);
                if (AffectValue2Extra != 0)
                {
                    sb.Append("_");
                    sb.Append(AffectValue2Extra);
                }
                if (AffectValue2Extra2 != 0)
                {
                    sb.Append("_");
                    sb.Append(AffectValue2Extra2);
				}
				if (AffectValue2Extra3 != null)
				{
					sb.Append("_");
					sb.Append(AffectValue2Extra3);
				}
				return sb.ToString();
            }
            set
            {
                if (value.Length > 0)
                {
                    string[] l = value.Split('_', ',');

                    if (l.Length > 0)
                    {
                        AffectValue2 = int.Parse(l[0]);
                    }
                    if (l.Length > 1)
                    {
                        AffectValue2Extra = int.Parse(l[1]);
                    }
                    if (l.Length > 2)
                    {
                        AffectValue2Extra2 = int.Parse(l[2]);
                    }
                }
            }
        }

        public int SkillCategory { get; set; }
        public string Desc { get; set; }
        public int EvoRank { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
