﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElementalKingdoms.core
{
    public class UserRune
    {
        public int UserRuneId { get; set; }
        public int Uid { get; set; }
        public int RuneId { get; set; }
        public int Level { get; set; }
        public int Exp { get; set; }

        public string UUID { get; set; }

        private Rune r = null;

        [Newtonsoft.Json.JsonIgnore]
        public string Name { get { return BaseRune.Name; } }

        [Newtonsoft.Json.JsonIgnore]
        public Rune BaseRune { 
            get 
            {
                if (r == null || r.Id != RuneId)
                {
                    r = Rune.Runes.Find(x => x.Id == RuneId);
                }
                if (r == null) throw new TypeInitializationException("UserRune does not have a valid reference to a Rune", null);
                return r; 
            } 
        }

        public Skill GetSkill()
        {

            int skillId = 0;
            if (Level == 0) skillId = BaseRune.LockSkill1;
            if (Level == 1) skillId = BaseRune.LockSkill2;
            if (Level == 2) skillId = BaseRune.LockSkill3;
            if (Level == 3) skillId = BaseRune.LockSkill4;
            if (Level == 4) skillId = BaseRune.LockSkill5;

            return Skill.Get(skillId);
        }

        public override bool Equals(object obj)
        {
            if ((Object)this == null)                        //this is necessary to guard against reverse-pinvokes and
                throw new NullReferenceException();  //other callers who do not use the callvirt instruction

            if ((Object)this == (Object)obj)
            {
                return true;
            }

            if ((Object)this == null || (Object)obj == null)
            {
                return false;
            }

            UserRune other = obj as UserRune;
            if (other == null)
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            return this.RuneId == other.RuneId && this.Level == other.Level;
        }

        public override string ToString()
        {
            return Name + " (" + Level + ")";
        }
    }
}
