﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ElementalKingdoms.core
{
    public class BattleCard : INotifyPropertyChanged
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public UserCard Card { get; private set; }
        public BattleUser Owner { get; set; }

        public Race Race { get; set; }
        public Race BaseRace { get; private set; }

        private Card baseCard;

        private int currentHP;
        private int currentAttack;
        private int currentWaitTime;

        public BattleLocation Location { get; set; }
        public int LocationIndex { get; set; }

        // Store amount of attack added by Craze. This to ensure the bug is properly handled.
        public int CrazeExtraAttack { get; set; }
        public int BonusRoundAttack { get; set; }
        public int BonusSkillAttack { get; set; }
        public int CurrentAttack { 
            get { return currentAttack; } 
            set 
            {
                currentAttack = Math.Max(0, value);
                //OnPropertyChanged("CurrentTotalAttack"); 
            } 
        }

        public int CurrentTotalAttack { get { return CurrentAttack + BonusSkillAttack + BonusRoundAttack; } }

        public int CardBaseHP { get; private set; }
        public int BaseHP { get; private set; }
        public int CardBaseAttack { get; private set; }
        public int BaseAttack { get; private set; }

        private int bonusSkillHP;
        public int BonusSkillHP {
            get { return bonusSkillHP; }
            set 
            {
                int bonusHPIncrease = value - bonusSkillHP;
                bonusSkillHP = value;

                if (bonusHPIncrease < 0)
                {
                    // We are decreasing the bonus hp. We only need to remove it from current hp if the current hp would be above it's max + leftover bonusses
                    if (currentHP > BaseHP + bonusSkillHP)
                    {
                        bonusHPIncrease = (BaseHP + bonusSkillHP) - currentHP;
                    }
                    else
                    {
                        bonusHPIncrease = 0;
                    }
                }

                CurrentHP = currentHP + bonusHPIncrease;
            }
        }

        public int CurrentHP {
            get { return currentHP; } 
            set
            {
                currentHP = Math.Max(0, value);
            }
        }
        public int CurrentWaitTime { 
            get { return currentWaitTime; } 
            set 
            { 
                currentWaitTime = Math.Max(0, value); 
                //OnPropertyChanged("CurrentWaitTime"); 
            } 
        }

        public String Name { get; set; }

        public bool FirstAttack { get; set; }

        public bool LastChanceAvailable { get; set; }
        public bool LastChanceActive { get; set; }

        public bool DivineShieldAvailable { get; set; }
        public bool TimeReverseAvailable { get; set; }

        public int Sensitive { get; set; }
        public bool HasSensitive { get { return Sensitive > 0; } }

        public bool HasDoubleAttack { get; set; }

        public bool HasExtinction { get; set; }
        public bool IsExtinctionMarked { get; set; }

        public bool Laceration { get; set; }

        public int MagicShield { get; set; }
        public int SpellReduction { get; set; }
        public bool HasMagicShield { get { return MagicShield > 0; } }
        public bool HasSpellSpellReduction { get { return SpellReduction > 0; } }

        public int SpellMark { get; set; }

        public int ReflectDamage { get; private set; }
        public bool HasReflection { get { return ReflectDamage > 0; } }
        public bool HasResistance { get; private set; }
        public bool HasImmunity { get; private set; }
		public bool HasPhysicalShield { get; private set; }
		public bool HasCatTank { get; private set; }
		public bool PhysicalShieldActivated { get; set; }
		public bool HasSpiritualVoice { get; private set; }

        public bool HasAffliction { get { return Burned || Poisoned || Frozen || Paralyzed; } }

        // TODO: Need to think about burns, since you can only be burned once by each source...
        public Dictionary<string, int> Burns = new Dictionary<string, int>();
        public void AddBurn(string source, int amount)
        {
            if (HasImmunity)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Immune!");
                return;
            }
            if (Burns.ContainsKey(source))
            {
                if (log.IsDebugEnabled) log.DebugFormat("Already burned by {0}", source);
                return;
            }
            if (log.IsDebugEnabled) log.DebugFormat("Adding burn {0} {1}", source, amount);
            Burns.Add(source, amount);
        }
        public bool Burned { get { return Burns.Count > 0; } }

        public bool Frozen { get; set; }
        public bool Paralyzed { get; set; }
        public bool Trapped { get; set; }
        public bool Confused { get; set; }
        public bool Stunned { get; set; }
        public bool Silenced { get; set; }

        public bool Roared { get; set; }

        public int DeathMark { get; set; }

        public List<int> Poisons = new List<int>();
        public bool Poisoned { get { return Poisons.Count > 0; } }

        public int Blind { get; set; }
        public bool Blinded { get { return Blind > 0; } }

        private List<Skill> baseSkills;
        private List<Skill> currentSkills;
        private List<Skill> runeSkills;
        internal List<Skill> ActiveSkills()
        {
            // TODO: Add Rune skills here.
            if (runeSkills.Count > 0)
            {
                var ret = new List<Skill>(currentSkills.Count + runeSkills.Count);
                ret.AddRange(currentSkills);
                ret.AddRange(runeSkills);
                return ret;
            }
            return currentSkills;
        }

        public List<Skill> PreemptiveStrikeSkills { get; private set; } = new List<Skill>();
        public List<Skill> OnEnterBattlefieldTimeReverseSkills { get; private set; } = new List<Skill>();
        public List<Skill> OnEnterBattlefieldSkills { get; private set; } = new List<Skill>();
        public List<Skill> WhileOnBattlefieldSkills { get; private set; } = new List<Skill>();
        public List<Skill> DeathWisperSkills { get; private set; } = new List<Skill>();

        public bool HasEvasion { get; private set; }
        public bool HasGuard { get; private set; }
        public bool HasInfiltrator { get; private set; }
        public bool HasReanimation { get; private set; }
        public bool HasSupressing { get; private set; }
        public bool HasSoulImprison { get; private set; }

        public bool IsSummoned { get; set; }
        internal bool IsDieing { get; set; }
        public bool RecentlyNotDodged { get; internal set; }

        public BattleCard(UserCard card)
        {
            Card = card;
            baseCard = Card.BaseCard;
            BaseRace = baseCard.Race;

            // Split up the dual skills (Plague Blast, Demon Skin, Cererus, Apocalypse, etc)
            baseSkills = new List<Skill>(Card.ActiveSkills().Count);
            foreach (var skill in Card.ActiveSkills())
            {
				if (skill.AffectType == (int)EffectType.MultiSkill)
				{
					baseSkills.Add(Skill.Get(skill.AffectValue));
					baseSkills.Add(Skill.Get(skill.AffectValueExtra));
					// Or triple skills like Acrobatics
					if (skill.AffectValueExtra2 != 0) baseSkills.Add(Skill.Get(skill.AffectValueExtra2));
					if (skill.AffectValueExtra3 != null)
					{
						foreach (int s in skill.AffectValue2Extra3)
						{
							baseSkills.Add(Skill.Get(s));
						}
					};
				}
				else
				{
					baseSkills.Add(skill);
				}
            }
            currentSkills = new List<Skill>();
            runeSkills = new List<Skill>();

            BaseHP = CardBaseHP = baseCard.HpArray[Card.Level];
            BaseAttack = CardBaseAttack = baseCard.AttackArray[Card.Level];
            
            TimeReverseAvailable = true;

            Reset();
        }

        public void ClearSilence()
        {
            Silenced = false;
            if (currentSkills.Count != baseSkills.Count)
            {
                currentSkills = new List<Skill>(baseSkills);

                // Cache some often used skills. These are only reset 
                HasGuard = ActiveSkills().Find(y => y.Type == SkillType.Guard) != null;
                HasInfiltrator = ActiveSkills().Find(x => x.Type == SkillType.Puncture && x.AffectType == 66) != null;
                HasResistance = ActiveSkills().Find(x => x.Type == SkillType.Resistance) != null;
                HasImmunity = ActiveSkills().Find(x => x.Type == SkillType.Immunity && x.AffectType == 52) != null;
                HasReanimation = ActiveSkills().Find(x => x.Type == SkillType.Resurrection && (x.SkillId == 597 || x.AffectValue == 597)) != null;
                HasDoubleAttack = ActiveSkills().Find(x => x.AffectType == 131) != null;
                HasSupressing = ActiveSkills().Find(x => x.Type == SkillType.PreventRecycle && x.AffectType == 126) != null;
                HasSoulImprison = ActiveSkills().Find(x => x.Type == SkillType.PreventRecycle && x.AffectType == 115) != null;
				HasPhysicalShield = ActiveSkills().Find(x => x.Type == SkillType.Naturalize && x.AffectType == 173 && x.SkillCategory == 3) != null;
				HasCatTank = ActiveSkills().Find(x => x.Type == SkillType.HeroHeal && x.AffectType == 174 && x.SkillCategory == 3) != null;

				var sensitive = ActiveSkills().Find(x => x.Type == SkillType.Dodge && x.AffectType == 130);
                if (sensitive != null)
                {
                    Sensitive = sensitive.AffectValue;
                }

                HasSpiritualVoice = ActiveSkills().Find(x => x.Type == SkillType.SpiritualVoice) != null;
                HasExtinction = ActiveSkills().Find(x => x.Type == SkillType.Puncture && x.AffectType == 132) != null;

                UpdateSkillCache();
            }

            // Update the cached skills that could have added by a rune
            HasEvasion = ActiveSkills().Find(x => x.Type == SkillType.Immunity && x.AffectType == 71) != null;

            var reflectSkill = ActiveSkills().Find(x => x.Type == SkillType.Reflection);
            if (reflectSkill != null)
            {
                ReflectDamage = reflectSkill.AffectValue;
            }
            else
            {
                ReflectDamage = 0;
            }

            var magicShieldSkills = ActiveSkills().FindAll(x => x.Type == SkillType.MagicShield);
            if (magicShieldSkills.Count > 0)
            {
                MagicShield = magicShieldSkills[0].AffectValue;
                SpellReduction = magicShieldSkills[0].AffectValue2;

                for (int i = 1; i < magicShieldSkills.Count; i++)
                {
                    // If there are more, use the one with lowest value.
                    MagicShield = Math.Min(MagicShield, magicShieldSkills[i].AffectValue);
                    SpellReduction = Math.Max(SpellReduction, magicShieldSkills[i].AffectValue2);
                }
            }
            else
            {
                MagicShield = 0;
                SpellReduction = 0;
            }
        }

        private void UpdateSkillCache()
        {
            PreemptiveStrikeSkills = ActiveSkills().FindAll(x => x.AffectType == 142);
            OnEnterBattlefieldTimeReverseSkills = ActiveSkills().FindAll(x => x.LanchType == LaunchType.OnEnterBattleFieldTimeReverse);
            OnEnterBattlefieldSkills = ActiveSkills().FindAll(x => x.LanchType == LaunchType.OnEnterBattlefield);
            WhileOnBattlefieldSkills = ActiveSkills().FindAll(x => x.LanchType == LaunchType.WhileOnBattlefield);
            DeathWisperSkills = ActiveSkills().FindAll(x => x.AffectType == 141);
        }

        private void ResetAllSkills()
        {
            // Skills should only be removed by Silence, so we just need to check if the counts match
            if (currentSkills.Count != baseSkills.Count)
            {
                currentSkills = new List<Skill>(baseSkills);

                // Cache some often used skills. These are only reset 
                HasGuard = ActiveSkills().Find(y => y.Type == SkillType.Guard) != null;
                HasInfiltrator = ActiveSkills().Find(x => x.Type == SkillType.Puncture && x.AffectType == 66) != null;
                HasResistance = ActiveSkills().Find(x => x.Type == SkillType.Resistance) != null;
                HasImmunity = ActiveSkills().Find(x => x.Type == SkillType.Immunity && x.AffectType == 52) != null;
                HasReanimation = ActiveSkills().Find(x => x.Type == SkillType.Resurrection && (x.SkillId == 597 || x.AffectValue == 597)) != null;
                HasDoubleAttack = ActiveSkills().Find(x => x.AffectType == 131) != null;
                HasSupressing = ActiveSkills().Find(x => x.Type == SkillType.PreventRecycle && x.AffectType == 126) != null;
                HasSoulImprison = ActiveSkills().Find(x => x.Type == SkillType.PreventRecycle && x.AffectType == 115) != null;
				HasPhysicalShield = ActiveSkills().Find(x => x.Type == SkillType.Naturalize && x.AffectType == 173 && x.SkillCategory == 3) != null;
				HasCatTank = ActiveSkills().Find(x => x.Type == SkillType.HeroHeal && x.AffectType == 174 && x.SkillCategory == 3) != null;

				var sensitive = ActiveSkills().Find(x => x.Type == SkillType.Dodge && x.AffectType == 130);
                if (sensitive != null)
                {
                    Sensitive = sensitive.AffectValue;
                }

                HasSpiritualVoice = ActiveSkills().Find(x => x.Type == SkillType.SpiritualVoice) != null;
                HasExtinction = ActiveSkills().Find(x => x.Type == SkillType.Puncture && x.AffectType == 132) != null;

                UpdateSkillCache();
            }
            runeSkills.Clear();

            ClearRuneSkills(); // this ensures that Magic Shield, Reflection and Evasion are cached, saves me from duplicating the find-call
        }


        public void Reset()
        {
            CurrentHP = BaseHP;
            CurrentAttack = BaseAttack;
            CurrentWaitTime = baseCard.Wait;
            Name = baseCard.Name;
            FirstAttack = true;
            LastChanceActive = false;
            LastChanceAvailable = true;
            DivineShieldAvailable = true;
            IsDieing = false;
            Race = BaseRace;

            Poisons.Clear();
            Burns.Clear();
            Laceration = false;
            Trapped = false;
            Paralyzed = false;
            Frozen = false;
            Roared = false;
            Blind = 0;
            DeathMark = 0;
            BonusRoundAttack = 0;
            BonusSkillAttack = 0;
            BonusSkillHP = 0;
            CrazeExtraAttack = 0;
            SpellMark = 0;

            ResetAllSkills();
        }

        public void ModifyTemporaryAttack(int amount)
        {
        }

        public override string ToString()
        {
            if (CurrentWaitTime > 0)
            {
                return String.Format("{0} L {1} HP {2} W {3} A {4}", Name, Card.Level, CurrentHP, CurrentWaitTime, CurrentTotalAttack);
            }
            else
            {
                return String.Format("{0} L {1} HP {2}/{3} A {4}", Name, Card.Level, CurrentHP, BaseHP + BonusSkillHP, CurrentTotalAttack);
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion

        internal void AddRuneSkill(Skill skill)
        {
            runeSkills.Add(skill);

            // Update cached skills
            if (skill.AffectType == 71)
            {
                HasEvasion = true;
            }
            else if (skill.AffectType == 41)
            {
                ReflectDamage = Math.Max(skill.AffectValue, ReflectDamage);
            }
            else if (skill.AffectType == 50)
            {
                // if current magic shield, take the lower of the two. Otherwise, this is the new value
                if (HasMagicShield)
                {
                    MagicShield = Math.Min(MagicShield, skill.AffectValue);
                }
                else
                {
                    MagicShield = skill.AffectValue;
                }
            }
            else if (skill.AffectType == 61)
            {
                // Shield
                if (log.IsDebugEnabled) log.DebugFormat("Shield: {0} bonus hp for {1}", skill.AffectValue, this.ToString());
                BonusSkillHP += skill.AffectValue;
            }
            else if (skill.AffectType == 62)
            {
                // Barricade
                if (log.IsDebugEnabled) log.DebugFormat("Barricade: {0} bonus hp for {1}", skill.AffectValue, this.ToString());
                BonusSkillHP += skill.AffectValue;
            }
            else if (skill.AffectType == 64)
            {
                // Group Morale
                if (log.IsDebugEnabled) log.DebugFormat("Group Morale: {0} bonus attack for {1}", skill.AffectValue, this.ToString());
                BonusSkillAttack += skill.AffectValue;
            }
        }

        internal void ClearRuneSkills()
        {
            var shield = runeSkills.Find(x => x.AffectType == 61);
            if (shield != null)
            {
                BonusSkillHP -= shield.AffectValue;
            }

            var barricade = runeSkills.Find(x => x.AffectType == 62);
            if (barricade != null)
            {
                BonusSkillHP -= barricade.AffectValue;
            }

            var groupMorale = runeSkills.Find(x => x.AffectType == 64);
            if (groupMorale != null)
            {
                BonusSkillAttack -= groupMorale.AffectValue;
            }

            runeSkills.Clear();
            // Update the cached skills that could have added by a rune
            HasEvasion = ActiveSkills().Find(x => x.Type == SkillType.Immunity && x.AffectType == 71) != null;

            var reflectSkill = ActiveSkills().Find(x => x.Type == SkillType.Reflection);
            if (reflectSkill != null)
            {
                ReflectDamage = reflectSkill.AffectValue;
            }
            else
            {
                ReflectDamage = 0;
            }


            var magicShieldSkills = ActiveSkills().FindAll(x => x.Type == SkillType.MagicShield);
            if (magicShieldSkills.Count > 0)
            {
                MagicShield = magicShieldSkills[0].AffectValue;
                SpellReduction = magicShieldSkills[0].AffectValue2;

                for (int i = 1; i < magicShieldSkills.Count;i++)
                {
                    // If there are more, use the one with lowest value.
                    MagicShield = Math.Min(MagicShield, magicShieldSkills[i].AffectValue);
                    SpellReduction = Math.Max(SpellReduction, magicShieldSkills[i].AffectValue2);
                }
            }
            else
            {
                MagicShield = 0;
                SpellReduction = 0;
            }
        }

        /// <summary>
        /// Silences the card, deactivating all skills except Resistance.
        /// </summary>
        public void Silence()
        {
            // Silence should be removed at some point, but I have no idea how that can happen since only a demon has the skill, so it is applied every turn anyway.
            Silenced = true;
            if (log.IsDebugEnabled) log.DebugFormat("Silence - removing skills");

            HasGuard = false;
            HasInfiltrator = false;
            HasImmunity = false;
            HasReanimation = false;
            HasDoubleAttack = false;
            HasSupressing = false;
            HasSoulImprison = false;
            HasExtinction = false;
			HasPhysicalShield = false;
			HasCatTank = false;

            Sensitive = 0;

            foreach (Skill s in currentSkills.ToList()) // copy with ToList to allow removal in loop
            {
                if (s.Type == SkillType.Resistance)
                {
                    if (log.IsDebugEnabled) log.Debug("You can keep your puny resistance");
                }
                else if (s.Type == SkillType.AttackIncrease && s.LanchType == LaunchType.WhileOnBattlefield)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("You can keep your puny {0}", s.Name);
                }
                else if (s.Type == SkillType.HPIncrease && s.LanchType == LaunchType.WhileOnBattlefield)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("You can keep your puny {0}", s.Name);
                }
                else
                {
                    if (log.IsDebugEnabled) log.DebugFormat("NO MORE {0}", s.Name);
                    currentSkills.Remove(s);
                }
            }

            ClearRuneSkills();

            UpdateSkillCache();
        }

        public void SetBaseStats(int attack, int hp)
        {
            BaseAttack = attack;
            BaseHP = hp;
            Reset();
        }

        private void SetThiefStats(int attack, int hp, bool legendary)
        {
            if (legendary)
            {
                hp *= 2;
            }
            SetBaseStats(attack, hp);
        }

        public void SetThief(bool legendary)
        {
            //log.InfoFormat("Found card {0}", Name);
            switch (Name)
            {
                case "Fire Demon":
                    SetThiefStats(3283, 4959, legendary);
                    break;
                case "Ice Dragon":
                    SetThiefStats(3087, 4901, legendary);
                    break;
                case "Water Elemental":
                    SetThiefStats(2082, 3451, legendary);
                    break;
                case "Kitsune":
                    SetThiefStats(2254, 3944, legendary);
                    break;
                case "Behemoth":
                    SetThiefStats(2646, 5104, legendary);
                    break;
                case "Demonic Imp":
                    SetThiefStats(2371, 2856, legendary);
                    break;
                case "Swamp Golem":
                    SetThiefStats(2141, 3642, legendary);
                    break;
                case "Polar Bearborn":
                    SetThiefStats(2009, 2624, legendary);
                    break;
                default:
                    break;
            }
        }
    }
}
