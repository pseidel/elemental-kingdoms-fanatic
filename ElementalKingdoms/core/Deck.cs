﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core
{
    public class Deck
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [JsonProperty("UserCardInfo")]
        public List<UserCard> Cards { get; set; }
        [JsonProperty("UserRuneInfo")]
        public List<UserRune> Runes { get; set; }

        [JsonProperty("Runes")]
        private List<UserRune> runeCompat_0_0_8_2
        {
            set
            {
                if (Runes == null || Runes.Count == 0)
                {
                    if (value == null)
                    {
                        log.Warn("Runes not loaded, going to set it to null");
                    }
                    Runes = value;
                }
                else if (value != null)
                {
                    log.WarnFormat("Found old and new Rune format in deck; ignoring new format {0} -> {1}", string.Join(",", Runes), string.Join(",", value));
                }
                else
                {
                    log.Warn("Found old runes, tried to set it to null (which is not gonna happen baby)");
                }
            }
        }

        public int Uid { get; set; }
        public int GroupId { get; set; }

        public string Name { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public int Cost { get { return Cards.Sum(x => x.BaseCard.Cost); } }
        [Newtonsoft.Json.JsonIgnore]
        public int FullCost { get { return Cards.Sum(x => x.Cost); } }

        public Deck()
        {
            Name = string.Empty;
            Cards = new List<UserCard>();
            Runes = new List<UserRune>();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(Name);
            sb.Append(": ");
            foreach (var card in Cards)
            {
                sb.AppendFormat("{0}, ", card.Name);
            }
            sb.Append(" | ");
            foreach (var rune in Runes)
            {
                sb.AppendFormat("{0},", rune.BaseRune.Name);
            }
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (this == null)                        //this is necessary to guard against reverse-pinvokes and
                throw new NullReferenceException();  //other callers who do not use the callvirt instruction

            Deck d = obj as Deck;
            if (d == null)
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;
            
            return EqualsHelper(this, d);
        }

        private bool CardListEquals<T>(IEnumerable<T> list1, IEnumerable<T> list2)
        {
            if (list1.Count() != list2.Count()) return false;

            var cnt = list1.ToList();
            foreach (T s in list2)
            {
                if (!cnt.Remove(s))
                {
                    return false;
                }
            }
            return cnt.Count == 0;
        }

        private bool EqualsHelper(Deck deckA, Deck deckB)
        {
            if (deckA.Cards.Count != deckB.Cards.Count || deckA.Runes.Count != deckB.Runes.Count)
                return false;

            return CardListEquals(deckA.Cards, deckB.Cards)
                && CardListEquals(deckA.Runes, deckB.Runes);
        }
        /*
  "Groups": [
    {
      "Uid": 314173,
      "UserCardIds": "21568014_21568015_21568016",
      "UserRuneIds": "",
      "GroupId": 44912,
      "UserCardInfo": [
        {
          "Uid": 314173,
          "CardId": 31,
          "Level": 0,
          "Evolution": 0,
          "WashTime": 0,
          "SkillNew": 0,
          "Exp": "0",
          "UserCardId": 21568014
        },
        {
          "Uid": 314173,
          "CardId": 33,
          "Level": 0,
          "Evolution": 0,
          "WashTime": 0,
          "SkillNew": 0,
          "Exp": "0",
          "UserCardId": 21568015
        },
        {
          "Uid": 314173,
          "CardId": 33,
          "Level": 0,
          "Evolution": 0,
          "WashTime": 0,
          "SkillNew": 0,
          "Exp": "0",
          "UserCardId": 21568016
        }
      ]
    }
*/
    }
}
