﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElementalKingdoms.core
{
    public enum BattleLocation
    {
        None,
        Deck,
        Hand,
        Battlefield,
        Cemetary,
    }
}
