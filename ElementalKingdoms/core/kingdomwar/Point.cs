﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.core.kingdomwar
{
    public class Point
    {
        [JsonProperty("point_id")]
        public string Id { get; set; }
        [JsonProperty("point_name")]
        public string Name { get; set; }

        [JsonProperty("force_id")]
        public Race Ruler { get; set; }

        public int Value { get; set; }
        public int Resource { get; set; }
        public int IsHome { get; set; }
        public string Link { get; set; }
        public Dictionary<string, int> Around { get; set; }
        /*
        
        "point_id": "1",
        "point_name": "Fire Furnace",
        "force_id": "4",
        "value": 1000,
        "resource": 10,
        "ishome": 2,
        "link": "2,3,5",
        "around": {
            "1": 0,
            "2": 0,
            "3": 0,
            "4": 0
        }
        */
    }
}
