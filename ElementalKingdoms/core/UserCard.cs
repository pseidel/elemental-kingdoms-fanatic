﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ElementalKingdoms.core
{
    [Serializable]
    public class UserCard
    {
        //public int Uid { get; set; } // user id
        public int CardId { get; set; }
        public int Level { get; set; }
        [Newtonsoft.Json.JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int Evolution { get; set; }
        public int? WashTime { get; set; }
        [Newtonsoft.Json.JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int SkillNew { get; set; }
        public int Exp { get; set; }
        public int UserCardId { get; set; }

        public string UUID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Attack { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? HP { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public Card BaseCard { get { return Card.Cards.Find(c => c.Id == CardId); } }
        [Newtonsoft.Json.JsonIgnore]
        public string Name { get { return BaseCard.Name; } }
        [Newtonsoft.Json.JsonIgnore]
        public int Cost 
        { 
            get 
            { 
                // At the first, third and fifth evolve, the card's cost will increase by one.
                return BaseCard.Cost + Math.Min((int)((WashTime.GetValueOrDefault(0) + 1)/2), 3); 
            } 
        }

        public List<Skill> ActiveSkills()
        {
            Card baseCard = Card.Get(CardId);
            List<Skill> ret = new List<Skill>();

            if (baseCard.Skill != null)
            {
                ret.Add(Skill.Get(baseCard.Skill.Value));
            }

            if (baseCard.Skill2 != null)
            {
                ret.Add(Skill.Get(baseCard.Skill2.Value));
            }

            if (Level >= 5 && baseCard.LockSkill1 != null)
            {
                ret.Add(Skill.Get(baseCard.LockSkill1.Value));
            }

            if (Level >= 5 && baseCard.LockSkill1b != null)
            {
                ret.Add(Skill.Get(baseCard.LockSkill1b.Value));
            }

            if (Level >= 10 && baseCard.LockSkill2 != null)
            {
                ret.Add(Skill.Get(baseCard.LockSkill2.Value));
            }

            if (baseCard.LockSkillDemon != null)
            {
                ret.Add(Skill.Get(baseCard.LockSkillDemon.Value));
            }

            if (SkillNew != 0)
            {
                ret.Add(Skill.Get(SkillNew));
            }

            return ret;
        }

        public override bool Equals(object obj)
        {
            if ((Object)this == null)                        //this is necessary to guard against reverse-pinvokes and
                throw new NullReferenceException();  //other callers who do not use the callvirt instruction

            if ((Object)this == (Object)obj)
            {
                return true;
            }

            if ((Object)this == null || (Object)obj == null)
            {
                return false;
            }

            UserCard card = obj as UserCard;
            if (card == null)
                return false;

            if (Object.ReferenceEquals(this, obj))
                return true;

            // Evolution, WashTime, Exp and UserCardId are not very important to check. If the CardId, Level and SkillNew are the same, the cards are practically the same.
            return this.CardId == card.CardId && this.Level == card.Level && this.SkillNew == card.SkillNew;
        }

        /* // from GetUserCards
        {
			"Uid": 314173,
			"CardId": 31,
			"Level": 0,
			"Evolution": 0,
			"WashTime": 0,
			"SkillNew": 0,
			"Exp": "0",
			"UserCardId": 21568014
		},
        */
        /* // from battle
        {
				"UUID": "atk_1",
				"CardId": "31",
				"UserCardId": 21568014,
				"Attack": 85,
				"HP": 300,
				"Wait": "2",
				"Level": 0,
				"SkillNew": 0,
				"Evolution": 0,
				"WashTime": null
			},*/
    }
}
