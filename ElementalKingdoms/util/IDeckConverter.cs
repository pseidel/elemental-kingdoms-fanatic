﻿using ElementalKingdoms.core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElementalKingdoms.util
{
    public interface IDeckConverter
    {
        string GetDeckString(User u);
        User GetUserFromString(string s);
    }
}
