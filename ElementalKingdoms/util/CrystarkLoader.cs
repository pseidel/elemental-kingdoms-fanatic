﻿using ElementalKingdoms.core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ElementalKingdoms.util
{
    public class CrystarkLoader : IDeckConverter
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private List<User> users;

        private User u;
        private Deck d;

        public CrystarkLoader()
        {
            users = new List<User>();
        }

        public List<User> Load(string filename)
        {
            if (File.Exists(filename))
            {
                users.Clear();
                //ParseDeckFile(File.ReadAllText(filename));
                users.Add(GetUserFromString(File.ReadAllText(filename)));
            }

            return users;
        }
        
        public User GetUserFromString(string s)
        {
            string[] lines = s.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            //if (log.IsDebugEnabled) log.DebugFormat("Split: {0}", String.Join("\r\n-----\r\n", parts));
            u = new User();
            d = u.UserDeck;
            foreach (var line in lines)
            {
                ParseDeckLine(line);
            }

            return u;
        }

        private void ParseDeckFile(string fileContents)
        {
            if (log.IsDebugEnabled) log.DebugFormat("File contents: {0}", fileContents);
            string[] lines = fileContents.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            //if (log.IsDebugEnabled) log.DebugFormat("Split: {0}", String.Join("\r\n-----\r\n", parts));
            u = new User();
            d = u.UserDeck;
            foreach (var line in lines)
            {
                ParseDeckLine(line);
            }
            users.Add(u);
        }

        private void ParseDeckSection(string part)
        {
            string[] lines = part.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (lines.Length < 3)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Not enough lines (found {0})", string.Join(" | ", lines));
            }
            else
            {
                User u = new User();
                u.NickName = lines[0];
                u.Level = int.Parse(lines[1]);

                
                u.AllDecks.Add(d);
            }
        }

        private void ParseDeckLine(string line)
        {
            if (log.IsDebugEnabled) log.DebugFormat("Parsing deck line {0}", line);

            if (line.StartsWith("#"))
            {
                // comment
                return;
            }
            if (line.StartsWith("--"))
            {
                // sim option
                return;
            }

            if (line.StartsWith("Player name:", StringComparison.OrdinalIgnoreCase))
            {
                u.NickName = line.Substring(12).Trim();
            }
            else
            if (line.StartsWith("Player level:", StringComparison.OrdinalIgnoreCase))
            {
                u.Level = int.Parse(line.Substring(13));
            }
            else
            if (line.StartsWith("Deck name:", StringComparison.OrdinalIgnoreCase))
            {
                d.Name = line.Substring(10).Trim();
            }
            else
            {
                /// --- Card formats --- 
                /// Name
                /// Name, level[+evo]
                /// Name, level[+evo], new_ability_1[:value], ..., new_ability_N[:value]
                /// Name, Cost, Timer, Attack, Health, Type, ability_1[:value], ..., ability_N[:value]

                /// --- Rune formats --- 
                /// Name
                /// Name:Llevel
                /// Name:value

                // A line can contain either a card or a rune. First split on ',', then check the first token to see how to proceed

                string[] tokens = line.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length == 0) return;

                try
                {
                    d.Cards.Add(ParseCardLine(tokens));
                }
                catch (Exception x1)
                {
                    try
                    {
                        d.Runes.Add(ParseRuneLine(line));
                    }
                    catch (Exception x2)
                    {
                        log.ErrorFormat("Failed to parse line {0}", line);
                    }
                }
            }
        }
        
        public string GetDeckString(User u)
        {
            StringBuilder data = new StringBuilder();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string verstr = String.Format("# Exported by {0} version {1}", fvi.FileDescription, fvi.FileVersion);

            data.AppendLine(verstr).AppendLine();

            data.Append("Player name: ").AppendLine(u.NickName)
                .Append("Player level: ").AppendLine(u.Level.ToString());

            data.AppendLine();
            foreach (var card in u.UserDeck.Cards)
            {
                data.AppendFormat("{0},{1}", card.Name, card.Level);
                if (card.SkillNew != 0)
                {
                    data.AppendFormat(",{0}", GetCrystarkSkillName(card.SkillNew));
                }
                data.AppendLine();
            }
            data.AppendLine();
            foreach (var rune in u.UserDeck.Runes)
            {
                data.AppendFormat("{0}:L{1}", rune.Name, rune.Level).AppendLine();
            }

            return data.ToString();
        }

        public void Write(Dictionary<string, User> decks, string filepath)
        {
            StringBuilder data = new StringBuilder();
            
            foreach (var userDeck in decks)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Exporting deck {0}", userDeck.Key);

                u = userDeck.Value;
                data.Clear();
                data.Append(GetDeckString(u));

                // remove invalid characters from the potential file name
                Regex r = new Regex(string.Format("[{0}]", Regex.Escape(new string(Path.GetInvalidFileNameChars()))));
                string deckFileName = r.Replace(userDeck.Key, "");
                string add = "";

                string filename;
                int i = 0;
                do
                {
                    filename = String.Format("{0}deck.peppa.{1}{2}.txt", filepath, deckFileName, add);
                    i++;
                    add = "_" + i;
                } while (File.Exists(filename));

                File.WriteAllText(filename, data.ToString());
            }
        }

        private string GetCrystarkSkillName(int skillId)
        {
            if (skillId == 0) { return ""; }

            Skill s = Skill.Get(skillId);
            
            string skillName = 
                s.Name
                .Replace("Desperation:  ", "D_")
                .Replace("Desperation: ", "D_")
                .Replace("Quick Strike: ", "QS_")
                .Replace("Death Whisper: ", "DW_")
                .Replace("Preemptive Strike: ", "PS_");
            var digits = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            skillName = skillName.TrimEnd(digits).Trim();

            int value = s.AffectValue;
            if (skillName.StartsWith("D_") || skillName.StartsWith("QS") || skillName.StartsWith("DW") || skillName.StartsWith("PS"))
            {
                Skill actual = Skill.Get(s.AffectValue);
                value = actual.AffectValue;
            }

            if (value > 0)
            {
                return skillName + ":" + value;
            }

            return skillName;
        }

        private string RemoveTokens(string cardname)
        {
            /* An other feature are tokens. They allow to mark cards that have a specific behavior. Currently available tokens are:

    E: Marks a card as being an event (Hydra, EW) card and thus boosts it's attack and health depending on it's number of stars. This has no effect on a full card description.
    DI: Marks a card as being a DI Merit card. Each card with this token will add 8k merit to each fight.
    LG: Marks a card as being legendary. This token should be used on all cards of an EW defending deck to automatically apply the attack and health multipliers.
    ATK#multiplier: Applies the provided multiplier to the card's attack.
    HP#multiplier: Applies the provided multiplier to the card's hp.
*/
            string[] tokens = cardname.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (tokens[0].Equals("E", StringComparison.OrdinalIgnoreCase)
                || tokens[0].Equals("DI", StringComparison.OrdinalIgnoreCase)
                || tokens[0].Equals("LG", StringComparison.OrdinalIgnoreCase)
                || tokens[0].StartsWith("ATK", StringComparison.OrdinalIgnoreCase)
                || tokens[0].StartsWith("HP", StringComparison.OrdinalIgnoreCase))
            {
                return tokens[1];
            }
            return cardname;
        }

        private UserCard ParseCardLine(string[] tokens)
        {
            UserCard c = new UserCard();

            c.CardId = Card.GetIdFromName(RemoveTokens(tokens[0])); // throws exception if the name is not a valid card;
            c.Level = 10;

            /// Name
            /// Name, level[+evo]
            /// Name, level[+evo], new_ability_1[:value], ..., new_ability_N[:value]
            /// Name, Cost, Timer, Attack, Health, Type, ability_1[:value], ..., ability_N[:value]
            /// 
            int timer;
            bool fullDescriptionLine = false;

            // The 2nd token is either 'level[+evo]' or 'Cost' - in the last case, the 3th token is an integer and not a string
            if (tokens.Length > 2)
            {
                if (int.TryParse(tokens[2], out timer))
                {
                    fullDescriptionLine = true;
                }
            }

            if (fullDescriptionLine)
            {
                log.Warn("Crystark decks with full descriptions are not supported yet");
            }
            else
            {
                if (tokens.Length > 1)
                {
                    string[] token2 = tokens[1].Split(new[] { '+' }, StringSplitOptions.RemoveEmptyEntries);
                    
                    if (token2.Length == 0)
                    {
                        log.ErrorFormat("Unexpected parse error while reading line {0}", string.Join(",", tokens));
                    }
                    else
                    {
                        c.Level = int.Parse(token2[0]);
                        if (token2.Length == 2)
                        {
                            c.WashTime = int.Parse(token2[1]);
                        }
                    }

                    // Next up are the evo skills
                    for (int i = 2; i < tokens.Length; i++)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Parsing skill {0}", tokens[i]);
                        if (i > 3)
                        {
                            log.WarnFormat("Only 1 extra ability is currently supported, skipping {0}", tokens[i]);
                            continue;
                        }
                        
                        c.SkillNew = ParseEvolution(tokens[i]);
                    }
                }
            }

            return c;
        }

        private int ParseEvolution(string evo)
        {
            string[] parts = evo.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

            // Attention: Desperation skills have 2 spaces between the ':' and the skill name in the skills json file. 
            // Quick strike does not have this extra space.
            //                 Appending extra space here           v
            string skillName = parts[0].Replace("D_", "Desperation:  ").Replace("QS_", "Quick Strike: ").Replace("DW_", "Death Whisper: ").Replace("PS_", "Preemptive Strike: ").Trim();

            // For some reason, every desperation skill *except* Prayer and Destroy has the double space.
            if (skillName.IndexOf("prayer", StringComparison.OrdinalIgnoreCase) != -1
                || skillName.IndexOf("destroy", StringComparison.OrdinalIgnoreCase) != -1)
            {
                skillName = skillName.Replace("  ", " ");
            }

            bool hasSubSkill = false;
            // For desperation and quick strike, we might need to look at the value of the actual skill being executed. 
            if (skillName.StartsWith("Desperation:") || skillName.StartsWith("Quick Strike:") || skillName.StartsWith("Death Whisper:") || skillName.StartsWith("Preemptive Strike:"))
            {
                hasSubSkill = true;
            }


            var skills = Skill.Skills.FindAll(x => x.Name.StartsWith(skillName, StringComparison.OrdinalIgnoreCase));

            if (skills.Count == 1)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Matched {0} to skill {1}", evo, skills[0].Name);
                return skills[0].SkillId;
            }

            if (parts.Length == 2)
            {
                int value = int.Parse(parts[1]);
                if (log.IsDebugEnabled) log.DebugFormat("Found a set of potential skill matches. Going to find one that has value {0}", value);

                foreach (Skill s in skills)
                {
                    int compare = s.AffectValue;
                    if (hasSubSkill)
                    {
                        Skill actual = Skill.Get(s.AffectValue);
                        compare = actual.AffectValue;
                    }
                    if (log.IsDebugEnabled) log.DebugFormat("Comparing {0} to {1}", compare, value);
                    if (compare == value)
                    {
                        if (log.IsDebugEnabled) log.DebugFormat("Matched {0} to skill {1}", evo, s.Name);
                        return s.SkillId;
                    }
                }
            }

            log.ErrorFormat("Could not match skill {0} to a known skill", evo);
            return 0;
        }

        private UserRune ParseRuneLine(string line)
        {
            // Rune might contain :level or :value; split on that
            string[] parts = line.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

            UserRune r = new UserRune();
            r.RuneId = Rune.GetIdFromName(parts[0].Trim());
            r.Level = 4;

            if (parts.Length > 1)
            {
                if (parts[1].StartsWith("L", StringComparison.OrdinalIgnoreCase))
                {
                    r.Level = int.Parse(parts[1].Substring(1));
                }
                else
                {
                    log.WarnFormat("Rune with value instead of level is not supported yet (reading line {0})", line);
                }
            }

            return r;
        }
    }
}
