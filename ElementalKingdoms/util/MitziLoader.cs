﻿using ElementalKingdoms.core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ElementalKingdoms.util
{
    public class MitziLoader : IDeckConverter
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private List<User> users;

        /// <summary>
        /// Mitzi uses abbreviations for evo skills. Took this list from 2015.08.09 version Abilities.pde
        /// </summary>
        Dictionary<string, string> skillMap = new Dictionary<string, string>()
        {
            { "as", "Advanced Strike" },
            { "ag", "Arctic Guard" },
            { "ap", "Arctic Pollution" },
            { "stab", "Backstab" },
            { "bite", "Bite" },
            { "bli", "Blight" },
            { "blz", "Blitz" },
            { "bzd", "Blizzard" },
            { "bs", "Bloodsucker" },
            { "blt", "Bloodthirsty" },
            { "blb", "Bloody Battle" },
            { "ca", "Chain Attack" },
            { "cl", "Chain Lightning" },
            { "cs", "Clean Sweep" },
            { "cmb", "Combustion" },
            { "con", "Concentration" },
            { "cf", "Confusion" },
            { "cor", "Corruption" },
            { "cnt", "Counterattack" },
            { "cz", "Craze" },
            { "crs", "Curse" },
            { "d:a", "D: Annihilation" },
            { "d:bzd", "D: Blizzard" },
            { "d:crs", "D: Curse" },
            { "d:des", "D: Destroy" },
            { "d:es", "D: Electric Shock" },
            { "d:fgd", "D: Fire God" },
            { "d:fs", "D: Firestorm" },
            { "d:gw", "D: Group Weaken" },
            { "d:h", "D: Healing" },
            { "d:plg", "D: Plague" },
            { "d:reg", "D: Regeneration" },
            { "d:tc", "D: Toxic Clouds" },
            { "d:pyr", "D: Prayer" },
            { "d:trp", "D: Trap" },
            { "damn", "Damnation" },
            { "dm", "Death Marker" },
            { "des", "Destroy" },
            { "da", "Devil's Armor" },
            { "db", "Devil's Blade" },
            { "dc", "Devil's Curse" },
            { "dex", "Dexterity" },
            { "dp", "Divine Protection" },
            { "dge", "Dodge" },
            { "dr", "Dread Roar" },
            { "ds", "Dual Snipe" },
            { "es", "Electric Shock" },
            { "eva", "Evasion" },
            { "exi", "Exile" },
            { "fob", "Feast of Blood" },
            { "fw", "Fire Wall" },
            { "fb", "Fireball" },
            { "fgd", "Fire God" },
            { "fs", "Firestorm" },
            { "ffi", "Forest Fire" },
            { "ff", "Forest Force" },
            { "fg", "Forest Guard" },
            { "frs", "Frost Shock" },
            { "gu", "Gang Up!" },
            { "gb", "Glacial Barrier" },
            { "grd", "Guard" },
            { "gw", "Group Weaken" },
            { "h", "Healing" },
            { "hm", "Healing Mist" },
            { "hc", "Hot Chase" },
            { "ib", "Iceball" },
            { "ice", "Ice Shield" },
            { "imm", "Immunity" },
            { "imp", "Impede" },
            { "infl", "Infiltrator" },
            { "jb", "Jungle Barrier" },
            { "lct", "Laceration" },
            { "lc", "Last Chance" },
            { "ms", "Magic Shield" },
            { "mc", "Mana Corruption" },
            { "man", "Mania" },
            { "mb", "Marsh Barrier" },
            { "ma", "Mass Attrition" },
            { "mf", "Mountain Force" },
            { "mgr", "Mountain Glacier" },
            { "mg", "Mountain Guard" },
            { "nv", "Nova Frost" },
            { "obs", "Obstinacy" },
            { "og", "Origins Guard" },
            { "nf", "Northern Force" },
            { "pry", "Parry" },
            { "plg", "Plague" },
            { "pyr", "Prayer" },
            { "punc", "Puncture" },
            { "pur", "Purification" },
            { "ps", "Power Source" },
            { "q:bzd", "QS: Blizzard" },
            { "q:crs", "QS: Curse" },
            { "q:des", "QS: Destroy" },
            { "q:es", "QS: Electric Shock" },
            { "q:exl", "QS: Exile" },
            { "q:fgd", "QS: Fire God" },
            { "q:fs", "QS: Firestorm" },
            { "q:gw", "QS: Group Weaken" },
            { "q:h", "QS: Healing" },
            { "q:ma", "QS: Mass Attrition" },
            { "q:plg", "QS: Plague" },
            { "q:reg", "QS: Regeneration" },
            { "q:tel", "QS: Teleportation" },
            { "q:tc", "QS: Toxic Clouds" },
            { "q:pyr", "QS: Prayer" },
            { "q:pur", "QS: Purification" },
            { "q:trp", "QS: Trap" },
            { "rean", "Reanimation" },
            { "ref", "Reflection" },
            { "reg", "Regeneration" },
            { "rein", "Reincarnation" },
            { "rej", "Rejuvenation" },
            { "ret", "Retaliation" },
            { "rez", "Resurrection" },
            { "res", "Resistance" },
            { "sac", "Sacrifice" },
            { "sdf", "Sacred Flame" },
            { "seal", "Seal" },
            { "sd", "Self-Destruct" },
            { "soe", "Shield of Earth" },
            { "sil", "Silence" },
            { "slay", "Slayer" },
            { "smog", "Smog" },
            { "snp", "Snipe" },
            { "sumd", "Summon Dragon" },
            { "sf", "Swamp Force" },
            { "sg", "Swamp Guard" },
            { "sp", "Swamp Purity" },
            { "tbg", "The Don's Bondyguard" },
            { "tb", "Thunderbolt" },
            { "tc", "Toxic Clouds" },
            { "trp", "Trap" },
            { "vend", "Vendetta" },
            { "ven", "Venom" },
            { "vb", "Volcano Barrier" },
            { "wc", "Warcry" },
            { "wp", "Warpath" },
            { "weak", "Weaken" },
            { "wl", "Wicked Leech" },
        };

        public MitziLoader()
        {
        }


        public User GetUserFromString(string s)
        {
            string[] lines = s.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (lines.Length < 3)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Not enough lines (found {0})", string.Join(" | ", lines));
                return null;
            }
            else
            {
                User u = new User();
                u.NickName = lines[0];
                u.Level = int.Parse(lines[1]);

                Deck d = new Deck();

                for (int i = 2; i < lines.Length; i++)
                {
                    ParseDeckLine(d, lines[i]);
                }
                u.AllDecks.Add(d);
                return u;
            }
            
        }

        public List<User> Load(string filename)
        {
            var level = Util.SetLoglevel(log4net.Core.Level.Error);
            users = new List<User>();
            try
            {
                if (File.Exists(filename))
                {
                    ParseDeckFile(File.ReadAllText(filename));
                }
            }
            catch (Exception ex)
            {
                log.Error("Error while parsing deck " + filename, ex);
            }

            Util.SetLoglevel(level);

            return users;
        }

        private void ParseDeckFile(string fileContents)
        {
            //if (log.IsDebugEnabled) log.DebugFormat("File contents: {0}", fileContents);
            string[] parts = fileContents.Split(new string[] { "\r\n\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            //if (log.IsDebugEnabled) log.DebugFormat("Split: {0}", String.Join("\r\n-----\r\n", parts));
            foreach (var part in parts)
            {
                //ParseDeckSection(part);

                User u = GetUserFromString(part);
                if (u != null)
                {
                    users.Add(u);
                }
            }
            /* Mitzi deck format 
        60 // old stuff, can be ignored (MitziHai: They are for the default levels but aren't really used anymore)
        60

        EW Aranyani // Name
        0 // Level (0 = infinite hp)
        Aranyani [Legendary];15 // card; level; evo skill (with abbreviations)
        Serpent Shamanka;15;mc10
        Serpent Shamanka;15;mc10
        Orc Teacher;15;res1
        Orc Teacher;15;res1
        Black Thorn;15;res1
        Everglade Mermaid;15;res1
        Lightning Pumpkin;15;es8
        Christmas Treant;15;res1
        Prince of the Forest;15;res1

        EW Arctic Cephalid // Can be multiple decks in one file
        0
        Arctic Cephalid [Legendary];15
        Ice Knight;15;res1
        Sea Piercer;15;bzd8
        Steel Dragon;15;res1
        Tundra Rider;15;res1
        Sea Piercer;15;bzd8
        Crystal Emperor;15;res1
        Steel Dragon;15;res1
        Ice Knight;15;res1
        Tundra Rider;15;res1

        Hydra V Witch
        0
        Hydra V [Witch];10
        Behemoth;10
        Behemoth;10
        Behemoth;10
        Ice Dragon;10
        Ice Dragon;10
        Giant Mud Larva;10
        Taiga General;10
        Taiga General;10
        Grand Lich;10
        Rune: Red Valley;4 // runes are like this
        Rune: Arctic Freeze;4
        Rune: Clear Spring;4
        Rune: Flying Stone;4

        */
        }

        private void ParseDeckSection(string part)
        {
            string[] lines = part.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (lines.Length < 3)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Not enough lines (found {0})", string.Join(" | ", lines));
            }
            else
            {
                User u = new User();
                u.NickName = lines[0];
                u.Level = int.Parse(lines[1]);

                Deck d = new Deck();

                for (int i = 2; i < lines.Length; i++)
                {
                    ParseDeckLine(d, lines[i]);
                }
                u.AllDecks.Add(d);
                users.Add(u);
            }
        }

        private void ParseDeckLine(Deck d, string line)
        {
            if (log.IsDebugEnabled) log.DebugFormat("Parsing deck line {0}", line);

            if (line.StartsWith("Rune: "))
            {
                d.Runes.Add(ParseRuneLine(line));
            }
            else
            {
                d.Cards.Add(ParseCardLine(line));
            }
        }

        private UserCard ParseCardLine(string line)
        {
            string[] parts = line.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            UserCard c = new UserCard();

            c.CardId = Card.GetIdFromName(parts[0].Replace('[', '(').Replace(']', ')').Trim());
            if (parts.Length > 1)
            {
                c.Level = int.Parse(parts[1]);
            }
            else
            {
                c.Level = 10;
            }

            if (parts.Length > 2)
            {
                c.SkillNew = ParseEvolution(parts[2]);
            }

            return c;
        }

        private int ParseEvolution(string evo)
        {
            Match m = Regex.Match(evo, "([a-zA-Z:]+)([0-9]*)");
            if (m.Success)
            {
                string evoName = m.Groups[1].Value;
                string evoLevel = m.Groups[2].Value;
                
                if (skillMap.ContainsKey(evoName))
                {
                    string skillName = skillMap[evoName];

                    if (evoName == "res" || evoName == "eva" || evoName == "imm" || evoName == "sdf")
                    {
                        evoLevel = ""; // res1 in the file. Resistance does not have a level.
                    }
                        
                    if (evoLevel.Length > 0)
                    {
                        skillName += " " + evoLevel;
                    }

                    // Attention: Desperation skills have 2 spaces between the ':' and the skill name in the skills json file. 
                    // Quick strike does not have this extra space.
                    //                 Appending extra space here    v
                    skillName = skillName.Replace("D:", "Desperation: ").Replace("QS:", "Quick Strike:");

                    Skill s = Skill.Skills.Find(x => x.Name.Equals(skillName));

                    if (s != null)
                    {
                        return s.SkillId;
                    }

                    log.WarnFormat("Could not find skill {0}", skillName);
                }
                else
                {
                    log.WarnFormat("Unknown skill: {0}", evoName);
                }
            }
            else
            {
                log.WarnFormat("Failed to parse evolution ({0})", evo);
            }
            return 0;
        }

        private UserRune ParseRuneLine(string line)
        {
            string[] parts = line.Split(';');

            UserRune r = new UserRune();
            r.RuneId = Rune.GetIdFromName(parts[0].Substring(5).Trim());
            if (parts.Length > 1)
            {
                r.Level = int.Parse(parts[1]);
            }
            else
            {
                r.Level = 4;
            }

            return r;
        }

        public string GetDeckString(User u)
        {
            StringBuilder data = new StringBuilder();

            data.AppendLine(u.NickName).AppendLine(u.Level.ToString());

            foreach (var card in u.UserDeck.Cards)
            {
                data.AppendFormat("{0};{1};{2}", card.Name, card.Level, GetMitziSkillName(card.SkillNew)).AppendLine();
            }

            foreach (var rune in u.UserDeck.Runes)
            {
                data.AppendFormat("Rune: {0};{1}", rune.Name, rune.Level).AppendLine();
            }

            return data.ToString();
        }

        public bool Write(List<User> users, string filename)
        {
            StringBuilder data = new StringBuilder();
            data.AppendLine("60").AppendLine("60").AppendLine();

            foreach (User u in users)
            {
                data.AppendLine(GetDeckString(u));
            }
            /*
        EW Aranyani // Name
        0 // Level (0 = infinite hp)
        Aranyani [Legendary];15 // card; level; evo skill (with abbreviations)
        Serpent Shamanka;15;mc10
        Serpent Shamanka;15;mc10
        Orc Teacher;15;res1
        Orc Teacher;15;res1
        Black Thorn;15;res1
        Everglade Mermaid;15;res1
        Lightning Pumpkin;15;es8
        Christmas Treant;15;res1
        Prince of the Forest;15;res1
        
        Rune: Red Valley;4 // runes are like this
        Rune: Arctic Freeze;4
        Rune: Clear Spring;4
        Rune: Flying Stone;4
        */

            File.WriteAllText(filename, data.ToString());

            return true;
        }

        private string GetMitziSkillName(int skillId)
        {
            if (skillId == 0) { return ""; }

            Skill s = Skill.Get(skillId);

            string skillName = s.Name.Replace("Desperation: ", "D:").Replace("Quick Strike:", "QS:");


            Match m = Regex.Match(skillName, "([a-zA-Z :]+)([0-9]*)");
            if (m.Success)
            {
                string evoName = m.Groups[1].Value.Trim();
                string evoLevel = m.Groups[2].Value.Trim();

                if (evoLevel.Length == 0)
                {
                    evoLevel = "1";
                }

                string mitziEvoAbbr = skillMap.FirstOrDefault(x => x.Value == evoName).Key;
                return String.Format("{0}{1}", mitziEvoAbbr, evoLevel);
                if (skillMap.ContainsValue(evoName))
                {
                    if (evoName == "res" || evoName == "eva" || evoName == "imm")
                    {
                        evoLevel = ""; // res1 in the file. Resistance does not have a level.
                    }

                    if (evoLevel.Length > 0)
                    {
                        skillName += " " + evoLevel;
                    }
                    
                }
                else
                {
                    log.WarnFormat("Unknown skill: {0}", evoName);
                }
            }
            else
            {
                log.WarnFormat("Failed to parse evolution ({0})", skillName);
            }
            return "";
        }
    }
}
