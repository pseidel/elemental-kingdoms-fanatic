﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.IO;
using System.Threading;
using System.Net;
using ElementalKingdoms.core;
using ElementalKingdoms.server;

namespace ElementalKingdoms.util
{
    public class ImageLoader
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static ImageLoader myImageLoader = new ImageLoader();
        private Dictionary<string, Image> Images = new Dictionary<string, Image>();
        private List<string> loadQueue = new List<string>();

        private static readonly ManualResetEvent _event = new ManualResetEvent(false);

        /// <summary>
        /// TODO: Remove this - need to pass the servercommunicator properly...
        /// </summary>
        public static string DeviceId { get; set; }

        private ImageLoader()
        {}

        // Wrapper method for use with thread pool.
        private void ThreadPoolCallback(Object threadContext)
        {
            string cardName = (string)threadContext;
            lock (loadQueue)
            {
                loadQueue.Remove(cardName);
                //Console.WriteLine("Image {0} loaded. Queue size={1}", cardName, loadQueue.Count);
            }
            getImageCore(cardName);
        }

        private void getImageInBackground(string cardName)
        {
            if (Images.ContainsKey(cardName))
            {
                if (Images[cardName] != null)
                {
                    // No need to get this image.
                    return;
                }
            }
            lock (loadQueue)
            {
                if (loadQueue.Contains(cardName))
                {
                    //Console.WriteLine("Already going to load card with id {0}", multiverseId);
                    //Logger.Log(0, "ImageLoader", "Already going to load card {0} with id {1}", card.Name, card.MultiverseId);
                    return;
                }
                loadQueue.Add(cardName);
            }
            ThreadPool.QueueUserWorkItem(myImageLoader.ThreadPoolCallback, cardName);
        }
        
        private static Image GetImage(string cardName)
        {
            return myImageLoader.getImageCore(cardName);
        }

        private Image getImageCore(string cardName)
        {
            bool alreadyLoading = false;
            do
            {
                lock (loadQueue)
                {
                    alreadyLoading = loadQueue.Contains(cardName);
                }
                if (alreadyLoading)
                {
                    //Console.WriteLine("Already going to load card with id {0}", multiverseId);
                    if (log.IsDebugEnabled) log.DebugFormat("Already going to load card {0}", cardName);
                    Thread.Sleep(200);
                }
            } while (alreadyLoading);
            if (Images.ContainsKey(cardName))
            {
                return Images[cardName];
            }
            //Console.WriteLine("Going to load the image.");
            //Logger.Log(5, "ImageLoader", "Going to load the image.");



            string fileName = "cache/cards/" + cardName + ".jpg";

            Image ret = null;
            try
            {
                //FileStream _fileStreamReader;
                Assembly _assembly = Assembly.GetExecutingAssembly();
                Stream s = _assembly.GetManifestResourceStream("ElementalKingdoms.Resources." + fileName.Replace('/', '.'));
                if (s == null)
                {
                    if (File.Exists(fileName))
                    {
                        //Console.WriteLine("Loading image from file {0}", fileName);
                        if (log.IsDebugEnabled) log.DebugFormat("Loading image from file {0}", fileName);
                        using (FileStream fs = File.OpenRead(fileName))
                        {
                            ret = Image.FromStream(fs);
                        }
                    }
                    else
                    {
                        ret = GetRemoteImage(cardName);
                        //ret = Gatherer.GetImage(cardName);
                        if (ret == null)
                        {
                            Console.WriteLine("Could not find card {0} (file {1})", cardName, fileName);
                        }
                        else
                        {
                            Console.WriteLine("Writing downloaded file to disk");
                            Directory.CreateDirectory("cache/cards");
                            ret.Save(fileName);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Loading image from assembly");
                    //Logger.Log(4, "ImageLoader", "Loading image from assembly");
                    ret = Image.FromStream(s);
                    s.Close();
                }


            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading file {0} : {1}", fileName, e.Message);
                //Logger.Log(4, "ImageLoader", "Error loading file {0} : {1}", fileName, e.Message);
                //MessageBox.Show("Error accessing resources!");
            }
            if (ret != null)
            {
                if (Images.ContainsKey(cardName))
                {
                    Console.WriteLine("Image already present...");
                    if (ret != Images[cardName])
                    {
                        Console.WriteLine("Images are different!!!!");
                    }
                }
                else
                {
                    Images.Add(cardName, ret);
                }
            }

            return ret;
        }

        public static Image GetImageByName(string cardName)
        {
            int id = Card.GetIdFromName(cardName);
            return GetImageById(id, false);
        }

        public static Image GetSmallImageByName(string cardName)
        {
            int id = Card.GetIdFromName(cardName);
            return GetImageById(id, true);
        }

        public static Image GetImageById(int id, bool small)
        {
            string key = String.Format("{0}_{1}", id, small);
            if (myImageLoader.Images.ContainsKey(key))
            {
                return myImageLoader.Images[key];
            }

            ServerCommunicator com = ServerCommunicator.Create(DeviceId, false);
            Image img = com.DownloadCardImage(id, small);
            if (img != null)
            {
                lock (myImageLoader)
                {
                    if (myImageLoader.Images.ContainsKey(key))
                    {
                        img = myImageLoader.Images[key];
                    }
                    else
                    {
                        myImageLoader.Images.Add(key, img);
                    }
                }
            }
            return img;
        }

        public static Image GetRuneImageById(int id)
        {
            ServerCommunicator com = ServerCommunicator.Create(DeviceId, false);
            return com.DownloadRuneImage(id);
        }

        public static Image GetImageFromServer(int cardId, bool head)
        {
            // Not all [Elite] cards seem to have their own resource file. Just get the original card image, which always seems to be (number - 7000)
            if (cardId >= 7000 && cardId < 8000)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Card with id in elite range, obtaining base card instead...");
                cardId = cardId - 7000;
            }

            string fileName;
            if (head)
            {
                fileName = String.Format("img_photoCard_{0}.PNG", cardId);
            }
            else
            {
                fileName = String.Format("img_maxCard_{0}.PNG", cardId);
            }
            
            Uri url = new Uri("http://ek.peppa84.com/images/" + fileName);

            // Creates an HttpWebRequest with the specified URL. 
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            // Sends the HttpWebRequest and waits for the response.			
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            // Gets the stream associated with the response.
            Stream receiveStream = myHttpWebResponse.GetResponseStream();

            Image img = Image.FromStream(receiveStream);

            // Releases the resources of the response.
            myHttpWebResponse.Close();

            img.Save("cache/resources/" + fileName);

            return img;
        }

        public static Image GetRemoteImage(string cardName)
        {
            cardName = cardName.ToLower().Replace(' ', '_').Replace("'", "");
            Uri url = new Uri("http://ek.arcannis.com/wp-content/themes/twentythirteen-child/images/cards/" + cardName + ".jpg");

            // Creates an HttpWebRequest with the specified URL. 
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            // Sends the HttpWebRequest and waits for the response.			
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
            // Gets the stream associated with the response.
            Stream receiveStream = myHttpWebResponse.GetResponseStream();

            Image img = Image.FromStream(receiveStream);

            // Releases the resources of the response.
            myHttpWebResponse.Close();

            return img;
        }
    }
}

