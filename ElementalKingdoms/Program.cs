﻿using ElementalKingdoms.core;
using ElementalKingdoms.tools;
using System;
using System.Windows.Forms;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace ElementalKingdoms
{
    /*
    static class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string mode = args.Length > 0 ? args[0] : "gui"; //default to gui


            if (mode == "gui")
            {
#if DEBUG
                Util.SetLoglevel(log4net.Core.Level.All);
#else
                Util.SetLoglevel(log4net.Core.Level.Info);
#endif
            }
            else
            {
                Util.SetLoglevel(log4net.Core.Level.Off);
            }


            log.Info("Elemental Kingdom Fanatic started.");
            Game game = new Game();
            game.Load();
            MultiBattleExecutor.Load();

            if (mode == "gui")
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ReplayForm());
                //Application.Run(new MainForm());
                //Application.Run(new MapStagesForm());
                //Application.Run(new BattleForm());
                //Application.Run(new CounterdeckGeneratorForm());
            //Application.Run(new ServerLoginForm());
            }
            else 
            {
            }
        }

    }
    */
}
