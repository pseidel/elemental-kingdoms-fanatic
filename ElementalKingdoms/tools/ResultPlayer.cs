﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElementalKingdoms.core;

namespace ElementalKingdoms.tools
{

    public class ResultPlayer
    {
        public int Level { get; set; }
        public Deck Deck { get; set; }

        public ResultPlayer()
        {
            Deck = new Deck();
        }

        public override bool Equals(object obj)
        {
            ResultPlayer other = obj as ResultPlayer;
            if (other == null) return false;

            return Level == other.Level && Deck.Equals(other.Deck);
        }

        public override string ToString()
        {
            return String.Format("Lvl {0} {1}", Level, Deck.ToString());
        }
    }

}
