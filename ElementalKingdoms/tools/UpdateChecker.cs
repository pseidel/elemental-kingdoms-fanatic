﻿using ElementalKingdoms.util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ElementalKingdoms.tools
{
    public class UpdateChecker
    {
        public bool UpdateAvailable { get; set; }
        public Dictionary<string, UpdateResult> UpdateLog { get; private set; }
        About about;

        public UpdateChecker()
        {
            about = new About();
            UpdateLog = new Dictionary<string, UpdateResult>();
        }

        public void CheckForUpdates()
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://ekf.peppa84.com/index.php");
                request.Method = "POST";

                var formContent = "version=" + about.AssemblyVersion;

                byte[] byteArray = Encoding.UTF8.GetBytes(formContent);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse response = request.GetResponse())
                    {
                        using (Stream dataStream2 = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(dataStream2))
                            {
                                string responseFromServer = reader.ReadToEnd();

                                JObject o = JObject.Parse(responseFromServer);

                                Console.WriteLine(o.ToString(Formatting.Indented));
                                if (o["success"].Value<bool>())
                                {
                                    var updates = o["updates"];
                                    if (updates.Count() > 0)
                                    {
                                        UpdateAvailable = true;

                                        // fill the update log
                                        foreach (var update in updates)
                                        {
                                            UpdateResult u = new UpdateResult()
                                            {
                                                Version = update["version"].ToString(),
                                                Date = update["date"].ToString(),
                                                ChangeLog = update["changelog"].ToString(),
                                            };
                                            UpdateLog.Add(u.Version, u);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
            }

        }
    }

    public class UpdateResult
    {
        public string Version { get; set; }
        public string Date { get; set; }
        public string ChangeLog { get; set; }
    }
}
