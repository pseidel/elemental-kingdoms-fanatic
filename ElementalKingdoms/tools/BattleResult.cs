﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElementalKingdoms.core;
using Newtonsoft.Json;

namespace ElementalKingdoms.tools
{
    public class BattleResult
    {
        public int MinMerit { get; set; }
        public int MaxMerit { get; set; }
        public long SumMerit { get; set; }
        public List<int> Merit { get; set; }

        /// <summary>
        /// Kingdom war number of lives
        /// </summary>
        [JsonIgnore]
        public int MinFightsPerLive { get { return FightsPerLive.Min(); } }
        [JsonIgnore]
        public int MaxFightsPerLive { get { return FightsPerLive.Max(); } }
        [JsonIgnore]
        public long SumFightsPerLive { get { return FightsPerLive.Sum(); } }
        public List<int> FightsPerLive { get; set; }

        public int MinRounds { get; set; }
        public int MaxRounds { get; set; }
        public long SumRounds { get; set; }

        public int NumberOfTimesReachedRoundFifty { get; set; }
        public int NumberOfTimesReachedRoundHundred { get; set; }
        public int RoundsWon { get; set; }

        public int Iterations { get; set; }

        public int Cooldown { get { return 60 + 2 * Attacker.Deck.Cost; } }
        public long MPF { get { return (SumMerit / Iterations); } }             
        public long MPM { get { return MPF * 60 / Cooldown; } }

        public int PercentHittingRound50 { get { return NumberOfTimesReachedRoundFifty * 100 / Iterations; } }
        public int PercentHittingRound100 { get { return NumberOfTimesReachedRoundHundred * 100 / Iterations; } }

        public ResultPlayer Attacker { get; set; }
        public ResultPlayer Defender { get; set; }

        [JsonIgnore]
        public List<Battle> Battles { get; set; }

        [JsonIgnore]
        public List<core.history.Battle> HistoricBattles { get; set; }

        public BattleResult()
        {
            Battles = new List<Battle>();
            Merit = new List<int>();
            FightsPerLive = new List<int>();
            HistoricBattles = new List<core.history.Battle>();
        }

        public BattleResult(int iterations, User attacker, User defender) : this()
        {
            if (iterations <= 0) throw new ArgumentOutOfRangeException("iterations must be > 0");
            Iterations = iterations;
            MinMerit = int.MaxValue;
            MinRounds = int.MaxValue;

            Attacker = new ResultPlayer()
            {
                Level = attacker.Level
            };
            Attacker.Deck.Cards = attacker.UserDeck.Cards.ToList();
            Attacker.Deck.Runes = attacker.UserDeck.Runes.ToList();

            Defender = new ResultPlayer()
            {
                Level = defender.Level
            };
            Defender.Deck.Cards = defender.UserDeck.Cards.ToList();
            Defender.Deck.Runes = defender.UserDeck.Runes.ToList();
        }

        public bool IsSameFight(BattleResult other)
        {
            return Attacker.Equals(other.Attacker) && Defender.Equals(other.Defender);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Number of fights: {0}", Iterations);
            sb.AppendLine();
			if (SumMerit > 0 && RoundsWon > 0)
			{
				sb.AppendFormat("Merit: {0} - {1} (MPF: {2}, MPM: {3})", MinMerit, MaxMerit, MPF, MPM);
				sb.AppendLine();
				sb.AppendFormat("Times won: {0} ({1}%)", RoundsWon, RoundsWon * 100 / Iterations);
				sb.AppendLine();
			}
			else if (SumMerit > 0)
            {
                sb.AppendFormat("Merit: {0} - {1} (MPF: {2}, MPM: {3})", MinMerit, MaxMerit, MPF, MPM);
                sb.AppendLine();
                sb.AppendFormat("Cooldown: {0}:{1:00}", Cooldown / 60, Cooldown % 60);
                sb.AppendLine();
            }
            else if (FightsPerLive.Count > 0)
            {
                sb.AppendFormat("Fights per life : {0} - {1} (avg: {2})", MinFightsPerLive, MaxFightsPerLive, SumFightsPerLive / Iterations);
                sb.AppendLine();
            }
            else
            {
                sb.AppendFormat("Times won: {0} ({1}%)", RoundsWon, RoundsWon * 100 / Iterations);
                sb.AppendLine();
            }

            if (FightsPerLive.Count == 0)
            {
                sb.AppendFormat("Rounds: {0} - {1} (avg: {2})", MinRounds, MaxRounds, (SumRounds / Iterations));
                sb.AppendLine();
                sb.AppendFormat("Percent time hitting round 50: {0}, 100: {1}", PercentHittingRound50, PercentHittingRound100);
            }
            return sb.ToString();
        }
    }
}
