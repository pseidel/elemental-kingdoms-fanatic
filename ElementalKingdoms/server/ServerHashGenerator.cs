﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ElementalKingdoms.server
{
    public class ServerHashGenerator
    {

        public ServerHashGenerator()
        {
            SetCommonData();
        }

        public string Md5Sum()
        {
            if (string.IsNullOrEmpty(Common.AA))
            {
                return "0";
            }
            if (string.IsNullOrEmpty(Common.BB))
            {
                return "0";
            }
            if (string.IsNullOrEmpty(Common.CC))
            {
                return "0";
            }
            if (string.IsNullOrEmpty(Common.DD))
            {
                return "0";
            }
            if (string.IsNullOrEmpty(Common.EE))
            {
                return "0";
            }
            if (string.IsNullOrEmpty(Common.FF))
            {
                return "0";
            }

            object[] objArray1 = new object[] { User.UID, Common.AuthType, "nj3dy&hf3h#jui$5lyf!s54", Common.AuthSerial };
            string s = string.Concat(objArray1);
            switch (Common.AuthType)
            {
                case 1:
                    s = s + Common.AA;
                    break;

                case 2:
                    s = s + Common.BB;
                    break;

                case 3:
                    s = s + Common.CC;
                    break;

                case 4:
                    s = s + Common.DD;
                    break;

                case 5:
                    s = s + Common.EE;
                    break;

                case 6:
                    s = s + Common.FF;
                    break;
            }
            byte[] bytes = new UTF8Encoding().GetBytes(s);
            byte[] buffer2 = new MD5CryptoServiceProvider().ComputeHash(bytes);
            string str2 = string.Empty;
            for (int i = 0; i < buffer2.Length; i++)
            {
                str2 = str2 + Convert.ToString(buffer2[i], 0x10).PadLeft(2, '0');
            }
            return str2.PadLeft(0x20, '0');
        }


        public static string GetMd5(string str)
        {
            byte[] bytes = new UTF8Encoding().GetBytes(str);
            byte[] buffer2 = new MD5CryptoServiceProvider().ComputeHash(bytes);
            string str2 = string.Empty;
            for (int i = 0; i < buffer2.Length; i++)
            {
                str2 = str2 + Convert.ToString(buffer2[i], 0x10).PadLeft(2, '0');
            }
            return str2.PadLeft(8, '0');
        }

        private void SetCommonData()
        {
            //Common.AA = GetMd5(Camera.main.transform.worldToLocalMatrix.m00.ToString()).Substring(0x19);
            Common.AA = GetMd5("320").Substring(0x19);

            //Common.BB = data["packages"][0][0].ToString();
            Common.BB = "package_AchievementTex_1";

            //Common.CC = GetMd5(Singleton<CardDataBase>.Instance.GetCardForID("11").m_n32MasterPacket).Substring(0x17);
            Common.CC = GetMd5("127").Substring(0x17);

            Common.DD = "keytest1234";// Singleton<LanguageSystem>.Instance.IDTable.GetUIString("keytest");

            int y = 1;// (int)this.m_pobjBackground.transform.localScale.y;
            Common.EE = GetMd5(y.ToString()).Substring(20);

            int x = 1;// (int)this.ActivenessBtn.transform.localPosition.x;
            Common.FF = GetMd5(x.ToString()).Substring(20);
        }

        public static void TestAASerial(int serial, string expectedHash)
        {
            var generator = new ServerHashGenerator();
            Common.AuthType = 1;

            // There is some more uncertain data
            Common.AuthSerial = serial; // maybe -1 or -2 or something...

            // UID
            User.UID = "107020";
            
            Console.WriteLine("Expecting {0} for serial {1}", expectedHash, serial);

            for (int i = -10000; i <= 10000; i++)
            {
                Common.AA = GetMd5(i.ToString()).Substring(0x19);
                if (generator.Md5Sum().Equals(expectedHash))
                {
                    Console.WriteLine("FOUND: {0} {1}", i, Common.AA);
                    break;
                }
            }
        }

        public static void Test()
        {
            Dictionary<int, string> testCases = new Dictionary<int, string>
            {
                { 189836, "890cc7433983386de83127b00f320288" },
                { 276554777, "a10a7a49bda2ab20100d4bdd3668c270" },
                { 276554781, "19cdba1851a565c1c57565dec739a272"},
                { 276554783, "890cc7433983386de83127b00f320288"},
                { 276554784, "1299cd22e961991f3f71923ed9c81437"},
                { 276554786, "4cda6248d642f4659a142441c62b9652"},
                { 584422614, "c3b91216827dd894756fa694222e2e83"},
                { 584422615, "2ebfd1f88bd7b9732ba508ea8ccc6974"},
                { 584422617, "1682cbdda40919f437b10fa1c13f191f"},
            };

            foreach (var item in testCases)
            {
                TestAASerial(item.Key, item.Value);
            }

        }

        public class Common
        {
            public static String AA { get; set; }
            public static String BB { get; set; }
            public static String CC { get; set; }
            public static String DD { get; set; }
            public static String EE { get; set; }
            public static String FF { get; set; }

            public static int AuthType { get; set; }
            public static int AuthSerial { get; set; }
        }

        public class User
        {
            public static string UID { get; set; }
        }
    }
}
