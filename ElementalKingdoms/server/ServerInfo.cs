﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ElementalKingdoms.server
{
    public class ServerInfo
    {
        [JsonProperty("GS_ID")]
        public int Id { get; set; }
        [JsonProperty("GS_STATUS")]
        public int Status { get; set; }
        [JsonProperty("GS_CHAT_IP")]
        public string ChatIP { get; set; }
        [JsonProperty("GS_CHAT_PORT")]
        public int ChatPort { get; set; }
        [JsonProperty("GS_NAME")]
        public string Name { get; set; }
        [JsonProperty("GS_IP")]
        public string Ip { get; set; }
    }
}
