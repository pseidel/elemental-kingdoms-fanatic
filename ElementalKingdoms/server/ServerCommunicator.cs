﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Drawing;
using ElementalKingdoms.util;
using HtmlAgilityPack;

namespace ElementalKingdoms.server
{
    public class ServerCommunicator
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
		//Old EK servers
		const string ARK_URL = "http://mobile.arcgames.com/";
		//const string EK_MASTER_URL = "http://master.ek.ifreeteam.com/";
		const string EK_MASTER_URL = "http://s1.ekbb.ifreeteam.com/";
		//New MR Servers
		//const string ARK_URL = "http://mobile.arcgames.com/";
		//const string EK_MASTER_URL = "http://74.72.1732.ip4.static.sl-reverse.com/";

		private const string GAME_PLATFORM = "ANDROID";
        private const string GAME_LANG = "EN";
        private const string GAME_VERSION = "1.8.2";
        private static string GAME_BUILD_DATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm");//"2015-08-07+18%3a55";

        public List<ServerInfo> Servers;
        private static Dictionary<string, bool> ServerConnections;

        private static ServerCommunicator com = null;
        public static ServerCommunicator Create(string deviceId, bool connectToServer)
        {
            if (com == null)
            {
                if (string.IsNullOrEmpty(deviceId))
                {
                    throw new ArgumentException("deviceId cannot be empty");
                }
                com = new ServerCommunicator(deviceId);
                ServerConnections = new Dictionary<string, bool>();
            }

            if (connectToServer)
            {
                if (!com.Connected)
                {
                    com.Connect();
                    //com.ArcLogin("g1095766@trbvm.com", "g1095766@trbvm");
                    com.PlayAsGuest();

                    com.EKLogin();
                }
            }
            return com;
        }

        private ServerHashGenerator HashGenerator;

        public ServerInfo Server { get; private set; }
        //string server { get { return "http://cache.ifreecdn.com/sgzj/public/wp8/"; } } //Server.Ip;
		string server { get { return "http://s1.ekbb.ifreeteam.com/"; } } //Server.Ip;

		string cdn_url;

        enum GameId {
            ElementalKingdoms = 51
        }

        public string DeviceId { get; set;}        
        public string Uin { get; set; }

        private string DeviceToken;
        private int LostPoint;

        private CookieAwareWebClient wc;

        public static string GenerateDeviceId()
        {
            string deviceId = ServerHashGenerator.GetMd5(ThreadSafeRandom.ThisThreadsRandom.Next().ToString());
            log.InfoFormat("Generated new DeviceId {0}", deviceId);

            return deviceId;
        }

        private ServerCommunicator(string deviceId)
        {
            //DeviceId = "a094d12945403af496d6657cba85b078";
            //DeviceId = "9d6303797585c8a1fe3986d1652d3122"; // HenkD
            if (String.IsNullOrEmpty(deviceId))
            {
            }

            DeviceId = deviceId;

            log.InfoFormat("Using DeviceId {0}", DeviceId);

            cdn_url = "";
			//server = "http://cache.ifreecdn.com/sgzj/public/wp8/";
			wc = new CookieAwareWebClient();
            Directory.CreateDirectory("cache");
            Directory.CreateDirectory("cache/league");
            Directory.CreateDirectory("cache/resources/android");

            Servers = new List<ServerInfo>();
            HashGenerator = new ServerHashGenerator();

            counter = ThreadSafeRandom.ThisThreadsRandom.Next(0x3e8, 0x270f);
            serial = 0;
        }

        private int counter;
        private int serial;

        public bool Connected
        {
            get
            {
                if (Server == null) return false;
                return ServerConnections.ContainsKey(com.server) && ServerConnections[com.server] == true;
            }
        }

        private string GetServerURL(string url)
        {
            if (Server == null)
            {
                // Need to do some minimal server connection now to get the address of one of the game servers.
                Connect();
                PlayAsGuest();
                EKLogin();
            }
            return String.Format("{0}{1}{2}", server, url, GetUrlParameters());
        }

        private string GetUrlParameters()
        {
            //&v=7797&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            //&v=8760&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.0&phpk=1ea261823dd60f0b951f9c0bf468ddb4&phps=276554788&pvb=2014-11-06+10%3a25
            //&v=6911&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.3&phpk=9431653b0e57d61aa2b6d0a4fb134e5a&phps=316470278&pvb=2015-01-26+15%3a13
            ServerHashGenerator.Common.AuthSerial = serial;

            string retVal = String.Format("&v={0}&phpp={1}&phpl={2}&pvc={3}&phpk={4}&phps={5}&pvb={6}", counter, GAME_PLATFORM, GAME_LANG, GAME_VERSION, HashGenerator.Md5Sum(), serial, GAME_BUILD_DATE);

            counter++;
            if (serial != 0) serial++;

            return retVal;
        }

        private JObject GetRequest(string url, NameValueCollection data = null)
        {
            string response = wc.GetRequest(GetServerURL(url), data);
            return ParseResponse(response);
        }

        private JObject PostRequest(string url,NameValueCollection data = null)
        {
            string response = wc.PostRequest(GetServerURL(url), data);
            return ParseResponse(response);
        }

        private JObject ParseResponse(string response)
        {
            if (string.IsNullOrEmpty(response))
            {
                log.WarnFormat("Empty response");
                return null;
            }
            JObject o = JObject.Parse(response);
            return o;
        }

        public bool Connect()
        {
			// 1. GET http://mobile.arcgames.com/user/login?gameid=51&deviceid=9d6303797585c8a1fe3986d1652d1286&platform=android&sdkvcode=2.0&androidos=15
			//string url = ARK_URL + "user/login";
			string url = ARK_URL + "user/login";
			var data = new NameValueCollection();
            data["gameid"] = ((int)GameId.ElementalKingdoms).ToString();
            data["deviceid"] = DeviceId;
            data["platform"] = "android";
            data["sdkvcode"] = "2.0";
            data["androidos"] = "15";
			//Nharzhool
            wc.GetRequest(url, data);

            return true;
        }

        public void PlayAsGuest()
        {
            // 2. GET http://mobile.arcgames.com/user/playasguest?gameid=51
            string url = ARK_URL + "user/playasguest";
            var data = new NameValueCollection();
            data["gameid"] = ((int)GameId.ElementalKingdoms).ToString();

            var response = wc.GetRequest(url, data);
            JObject o = JObject.Parse(response);
            if (log.IsDebugEnabled) log.Debug(o.ToString());
            var q = o.GetValue("loginstatus");
            var qq = q.ToString();
            var qqq = qq.Split(':');
            Uin = qqq[1];
            DeviceToken = qqq[3];


            // 3. GET http://mobile.arcgames.com/user/playasguest?gameid=51&deviceid=9d6303797585c8a1fe3986d1652d1286&platform=android&sdkvcode=2.0?
            // RETURNS {"result":true,"loginstatus":"guest:53d7eb97323a3:0:5lja7ejcgh759lqnpes4162f74:0"}
            data["deviceid"] = DeviceId;
            data["platform"] = "android";
            data["sdkvcode"] = "2.0";

            response = wc.GetRequest(url, data);
            // should be same as #2 afaik

            // 4. GET http://mobile.arcgames.com/index/getnotice?gameid=51&deviceid=9d6303797585c8a1fe3986d1652d1286&platform=android&sdkvcode=2.0?
            // RETURNS {"notif_total":0}
            url = ARK_URL + "index/getnotice";

            response = wc.GetRequest(url, data);
        }
		
        public void ArcLogin(string email, string password)
        {
			
            //1. Get login form http://mobile.arcgames.com/user/login?gameid=51
            string url = ARK_URL + "user/login";
            var data = new NameValueCollection();
            data["gameid"] = ((int)GameId.ElementalKingdoms).ToString();

            try
            {
                wc.GetRequest(ARK_URL + "user/logout", data);
            }
            catch (Exception ex)
            {
                // We might get an exception here because they redirect us to /user/login, but they currently (2015-07-15) have an SSL certificate with weak encryption for it, causing 
                // an exception. Just swallow it and move on.
                log.WarnFormat("user/logout caused exception: {0}", ex);
            }

            var response = wc.GetRequest(url, data);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(response);

            string emailName = doc.DocumentNode.SelectSingleNode("//input[@id='un']").Attributes["name"].Value;
            string passwordName = doc.DocumentNode.SelectSingleNode("//input[@id='pw']").Attributes["name"].Value;
            string captchaName = doc.DocumentNode.SelectSingleNode("//input[@id='captcha_login']").Attributes["name"].Value;
			
            /*
				<div class="js-captcha-wrap" >
					<img src='/captcha' style="margin: 0 0; width: 100%; height: auto;" />
					<input type="text" id="captcha_login" name="lAALBSHn" placeholder="Captcha Code" data-theme="a" />
				</div>
             */
            try
            {
                var captchaWrap = doc.DocumentNode.SelectSingleNode("//div[@class='js-captcha-wrap']");
                if (!captchaWrap.Attributes.Contains("style") || captchaWrap.Attributes["style"].Value != "display:none")
                {
                    string captcha = doc.DocumentNode.SelectSingleNode("//div[@class='js-captcha-wrap']/img").Attributes["src"].Value;
                    wc.DownloadFile(ARK_URL + captcha.Substring(1), "cache/captcha.png");
                }
            }
            catch (Exception ex)
            {
                log.WarnFormat("Something went wrong checking for captcha: {0}", ex.Message);
            }

            data[emailName] = email;
            data[passwordName] = password;

            data[captchaName] = "uWfDp";
			//TD
			/*
            response = wc.PostRequest(url, data);
            JObject o = JObject.Parse(response);
            if (log.IsDebugEnabled) log.Debug(o.ToString());
            if (o["result"].Value<bool>() == true)
            {
                var loginStatus = o["loginstatus"].Value<string>();
                var qq = loginStatus.Split(':');
                Uin = qq[1];
                DeviceToken = qq[3];
            }
            else
            {
                throw new Exception("Login failed: " + o["msg"]);
            }*/
        }

        private bool isNew;
        private string MUid;
        private string sign;
        private string ppsign;
        private string time;

        public void EKLogin(int preferedServerId = 0)
        {
			
            string url = EK_MASTER_URL + @"mpassport.php?do=plogin";

            // 5. POST http://master.ek.ifreeteam.com/mpassport.php?do=plogin 
            // plat=pwe&uin=53d7eb97323a3&nickName=53d7eb97323a3&Devicetoken=5lja7ejcgh759lqnpes4162f74&userType=2

            var data = new NameValueCollection();
            data["plat"] = "pwe";
            data["uin"] = Uin;
            data["nickName"] = Uin;
            if (!string.IsNullOrEmpty(DeviceToken))
            {
                data["Devicetoken"] = DeviceToken;
            }
            data["userType"] = "2"; // TODO Determine other userTypes. Maybe this is a value relating to Guest / Ark / Facebook login.
			//TD
            var response = wc.PostRequest(url, data);
            JObject o = JObject.Parse(response);
            if (log.IsDebugEnabled) log.Debug(o.ToString());
            var dataPrt = o.GetValue("data");
            isNew = dataPrt["new"].Value<bool>();
            var uinfo = dataPrt["uinfo"];

            MUid = uinfo["MUid"].ToString();
            sign = uinfo["sign"].ToString();
            ppsign = uinfo["ppsign"].ToString();
            time = uinfo["time"].ToString();

            // Current server = 
            //Server = dataPrt["current"].ToObject<ServerInfo>();
            if (preferedServerId == 0)
            {
                preferedServerId = dataPrt["current"]["GS_ID"].Value<int>();
            }
            if (log.IsDebugEnabled) log.DebugFormat("currentServerId: {0}", preferedServerId);
            //server = Server.Ip;
            //string connectedServer = dataPrt["current"]["GS_IP"].ToString();

            // All servers = 
            Servers = dataPrt["list"].ToObject<List<ServerInfo>>();
            Server = Servers.Find(x => x.Id == preferedServerId);
			
            ConnectToServer(Server);

            //if (log.IsDebugEnabled) log.Debug(dataPrt["uinfo"].ToString());

            //if (log.IsDebugEnabled) log.Debug(dataPrt.ToString());
            
            // Contains list of servers and user login info

            
// {"status":1,"data":{"new":false,"current":{"GS_ID":1,"GS_STATUS":1,"GS_CHAT_IP":"50.23.109.185","GS_CHAT_PORT":8000,"GS_NAME":"Chaos","GS_IP":"http:\/\/s1.ek.ifreeteam.com\/"},"list":[{"GS_ID":1,"GS_STATUS":1,"GS_CHAT_IP":"50.23.109.185","GS_CHAT_PORT":8000,"GS_NAME":"Chaos","GS_IP":"http:\/\/s1.ek.ifreeteam.com\/"},{"GS_ID":2,"GS_STATUS":1,"GS_CHAT_IP":"50.23.87.119","GS_CHAT_PORT":8000,"GS_NAME":"Harmony","GS_IP":"http:\/\/s2.ek.ifreeteam.com\/"},{"GS_ID":3,"GS_STATUS":1,"GS_CHAT_IP":"50.23.114.104","GS_CHAT_PORT":8000,"GS_NAME":"Legacy","GS_IP":"http:\/\/s3.ek.ifreeteam.com\/"},{"GS_ID":4,"GS_STATUS":1,"GS_CHAT_IP":"50.23.114.105","GS_CHAT_PORT":8000,"GS_NAME":"Destiny","GS_IP":"http:\/\/s4.ek.ifreeteam.com\/"},{"GS_ID":5,"GS_STATUS":2,"GS_CHAT_IP":"50.23.109.174","GS_CHAT_PORT":8000,"GS_NAME":"Fury","GS_IP":"http:\/\/s5.ek.ifreeteam.com\/"}],
//  "uinfo":{
//	    "access_token":"",
//	    "uin":"53d7eb97323a3",
//	    "nick":"53d7eb97323a3",
//	    "MUid":"1537121",
//	    "time":1406667469,
//	    "sign":"4acd36599d6642fb12548fc0161db1a5",
//	    "ppsign":"b491bf293e53e1346a42ad1c5529f500"}}}
// Set-Cookie: _sid=6f8g6b25l3bpkgdlsv9jpo1r53; expires=Fri, 08-Aug-2014 19:57:49 GMT; path=/\r\n

        }

        public void ConnectToServer(ServerInfo server)
        {
            if (Server == server && Connected)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Already connected to {0}", server.Name);
                return;
            }

            Server = server;
            ServerLogin();
        }

        public void ServerLogin()
        {

            // 6. POST http://s1.ek.ifreeteam.com/login.php?do=mpLogin&v=7797&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // username=53d7eb97323a3&plat=pwe&MUid=1537121&ppsign=b491bf293e53e1346a42ad1c5529f500&sign=4acd36599d6642fb12548fc0161db1a5&Udid=00-C0-7B-9F-0A-01&uin=53d7eb97323a3&nick=53d7eb97323a3&nickName=53d7eb97323a3&Origin=IOS_PW&userType=2&time=1406667469
            // Udid=00-C0-7B-9F-0A-01
            //      00C07B9F0A01
            //      826707741185
            // Origin=IOS_PW

            //var q = 0x00C07B9F0A01;

            NameValueCollection data = new NameValueCollection();
            data["plat"] = "pwe";
            data["username"] = Uin;
            data["MUid"] = MUid;
            data["ppsign"] = ppsign;
            data["sign"] = sign;
            data["uin"] = Uin;
            data["nick"] = Uin;
            data["nickName"] = Uin;
            data["Devicetoken"] = DeviceToken;
            data["time"] = time;
            data["userType"] = "2"; // TODO Determine other userTypes. Maybe this is a value relating to Guest / Ark / Facebook login.
            data["Udid"] = "00-C0-7B-EF-00-F5";
            data["Origin"] = "IOS_PW";

            //url = @"http://s1.ek.ifreeteam.com/login.php?do=mpLogin&v=7797&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38";
            JObject o = PostRequest("login.php?do=mpLogin", data);
            if (log.IsDebugEnabled) log.Debug(o.ToString());

            ServerConnections[server] = true;

            ServerHashGenerator.Common.AuthType = o["data"]["AuthType"].Value<int>();
            if (ServerHashGenerator.Common.AuthType != 1)
            {
                log.ErrorFormat("AuthType {0} is not supported yet! Proceed at own risk!!!");
            }
            serial = o["data"]["AuthSerial"].Value<int>();

            if (o["data"]["isSetNick"].Value<int>() != 0)
            {
                int attempts = 0;
                int status = 0;
                do
                {
                    data.Clear();
                    data["NickName"] = Util.GenerateName();
                    data["InviteCode"] = "44vhwe";
                    data["Sex"] = "0"; // 0 Male 1 Female

                    log.InfoFormat("Registering nickname {0}", data["NickName"].ToString());

                    //url = @"http://s1.ek.ifreeteam.com/user.php?do=EditNickName&v=7799&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38";
                    o = PostRequest("user.php?do=EditNickName", data);
                    if (log.IsDebugEnabled) log.Debug(o.ToString());

                    status = o["status"].Value<int>();

                    if (status == 0)
                    {
                        log.WarnFormat("EditNickName failed: {0}", o["message"].ToString());
                        attempts++;
                        if (attempts > 10)
                        {
                            log.ErrorFormat("Failed to generate nickname!");
                            throw new Exception("Failed to generate a nickname!");
                        }
                    }

                } while (status == 0);

                EditLostPoint(2);
            }

            GetUserInfo();
        }

        private void EditLostPoint(int lostPoint)
        {
            if (LostPoint >= lostPoint) return;

            NameValueCollection data = new NameValueCollection();
            data["LostPoint"] = lostPoint.ToString();

            JObject o = PostRequest("user.php?do=EditLostPoint", data);
            if (log.IsDebugEnabled) log.Debug(o.ToString());
            // 10. POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7801&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // LostPoint=2
            // RETURNS
            // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}
        }

        private void EditFresh(string freshStep)
        {

            // AFter clicking "Auto":
            // 23-A POST http://s1.ek.ifreeteam.com/user.php?do=EditFresh&v=7843&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // FreshStep=1_21
            // RETURNS
            // {"status":1,"data":[],"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}
            NameValueCollection data = new NameValueCollection();
            data["FreshStep"] = freshStep;

            JObject o = PostRequest("user.php?do=EditFresh", data);
            if (log.IsDebugEnabled) log.Debug(o.ToString());
        }
		
        public void GetUserInfo()
        {
            //http://s1.ek.ifreeteam.com/user.php?do=GetUserinfo&v=8739&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.0&phpk=0&phps=276554767&pvb=2014-11-06+10%3a25
            JObject o = GetRequest("user.php?do=GetUserinfo");

            string uid = o["data"]["Uid"].Value<string>();
            LostPoint = o["data"]["LostPoint"].Value<int>();

            ServerHashGenerator.User.UID = uid;
            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/userinfo.json", o.ToString());
        }

        public JObject GetCardGroups()
        {
            // 13-A GET http://s1.ek.ifreeteam.com/card.php?do=GetCardGroup&v=7811&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS GetCardGroups.json
            JObject o = GetRequest("card.php?do=GetCardGroup");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/cardgroup.json", o.ToString());

            return o;
        }

        public void GetUserLegion()
        {
            // 13-B GET http://s1.ek.ifreeteam.com/legion.php?do=GetUserLegion&v=7810&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS
            // {"status":1,"data":{"LegionId":0,"LegionName":"","StrengExpAdd":0,"Duty":0,"CreateUserId":0},"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

            JObject o = GetRequest("legion.php?do=GetUserLegion");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/userlegion.json", o.ToString());
        }

        public void GetThieves()
        { 
            // 13-C GET http://s1.ek.ifreeteam.com/arena.php?do=GetThieves&v=7809&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS
            // {"status":1,"data":{"Thieves":[],"Countdown":-1406666345,"SalaryCount":0},"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}
            JObject o = GetRequest("arena.php?do=GetThieves");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/thieves.json", o.ToString());
        }

        public void GetBackground()
        {
            // 13-D GET http://s1.ek.ifreeteam.com/user.php?do=GetBackGround&v=7815&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS 
            // {"status":1,"data":"http:\/\/s1.ek.ifreeteam.com\/public\/images\/activity\/background_1396423295.jpg","version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

            JObject o = GetRequest("user.php?do=GetBackGround");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/background.json", o.ToString());
        }

        public void GetActivityInfo()
        {
            // 13-E GET http://s1.ek.ifreeteam.com/activity.php?do=ActivityInfo&v=7814&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS
            // {"status":1,"data":[],"servertime":"2014-07-29-13-39-05","version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

            JObject o = GetRequest("activity.php?do=ActivityInfo");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/activityinfo.json", o.ToString());
        }

        public void GetUserRunes()
        {
            // 13-F GET http://s1.ek.ifreeteam.com/rune.php?do=GetUserRunes&v=7813&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS
            // {"status":1,"data":{"Runes":[]},"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

            JObject o = GetRequest("rune.php?do=GetUserRunes");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/userrunes.json", o.ToString());
        }

        public JObject GetUserCards()
        {
            // 13-G GET http://s1.ek.ifreeteam.com/card.php?do=GetUserCards&v=7812&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS GetUserCards.json

            JObject o = GetRequest("card.php?do=GetUserCards");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/usercards.json", o.ToString());

            return o;
        }

        public void GetUserMapStages()
        {
            // 17 GET http://s1.ek.ifreeteam.com/mapstage.php?do=GetUserMapStages&v=7832&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // RETURNS 
            // {"status":1,"data":null,"exploreOneKey":0,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}
            JObject o = GetRequest("mapstage.php?do=GetUserMapStages");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/usermapstages.json", o.ToString());
        }
		
        public void GetAllCard()
        {
            //http://s1.ek.ifreeteam.com/card.php?do=GetAllCard&v=7803&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            JObject o = GetRequest("card.php?do=GetAllCard");
            
            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/cards.json", o.ToString());

            JsonLoader.LoadCards();
        }

        public void GetAllSkill()
        {
            // http://s1.ek.ifreeteam.com/card.php?do=GetAllSkill&v=7805&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            JObject o = GetRequest("card.php?do=GetAllSkill");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/skills.json", o.ToString());

            JsonLoader.LoadSkills();
        }

        public void GetMapStages()
        {
            //http://s1.ek.ifreeteam.com/mapstage.php?do=GetMapStageALL&v=7802&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            JObject o = GetRequest("mapstage.php?do=GetMapStageALL");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/mapstages.json", o.ToString());

            JsonLoader.LoadMapstages();
        }

        public void GetAllRunes()
        {
            //http://s1.ek.ifreeteam.com/rune.php?do=GetAllRune&v=7804&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            JObject o = GetRequest("rune.php?do=GetAllRune");

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/allrunes.json", o.ToString());

            JsonLoader.LoadRunes();
        }

        public void GetLeagueInfo()
        {
            //GET http://s1.ek.ifreeteam.com/league.php?do=getLeagueInfo
            JObject o = GetRequest("league.php?do=getLeagueInfo");

            if (o == null)
            {
                // No access to league yet?
                return;
            }
            if (o["status"].Value<int>() == 0)
            {
                // Disconnected from server probably
                log.WarnFormat("Could not get league info: {0}", o["message"].Value<string>());
                return;
            }

            //if (log.IsDebugEnabled) log.Debug(o.ToString());
            System.IO.File.WriteAllText(@"cache/leagueinfo.json", o.ToString());

            JsonLoader.LoadLeague();

#if DEBUG
            string serverFolder = "cache/league/" + Server.Name + "/";
            Directory.CreateDirectory(serverFolder);

            int currentRound = ElementalKingdoms.core.League.CurrentLeague.RoundNow;
            String fileName = String.Format("s{0}r{1}.json", ElementalKingdoms.core.League.CurrentLeague.LeagueId, currentRound);
            System.IO.File.Copy("cache/leagueinfo.json", serverFolder + fileName, true);

            if (currentRound > 1)
            {
                foreach (var s in ElementalKingdoms.core.League.CurrentLeague.RoundResult[currentRound - 1])
                {
                    GetLeagueReplay(s.BattleId, ElementalKingdoms.core.League.CurrentLeague.LeagueId);
                }
            }
#endif
        }

        public void GetLeagueReplay(string battleId, int leagueId)
        {
            //POST http://s1.ek.ifreeteam.com/league.php?do=ShowRecord&v=8749&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.6&phpk=9b67bf9ae66db9a869397a681678f3d0&phps=471924400&pvb=2015-04-16+10%3a55

            NameValueCollection data = new NameValueCollection();
            data["BattleId"] = battleId;
            data["LeagueId"] = leagueId.ToString();
            try
            {
                JObject o = PostRequest("league.php?do=ShowRecord", data);
                String fileName = String.Format("cache/league/{0}_{1}.json", leagueId, battleId);
                System.IO.File.WriteAllText(fileName, o.ToString());
            }
            catch (Exception ex)
            {
                log.WarnFormat("Caught exception while getting league replay: {0}", ex.Message);
            }

            //JsonLoader.LoadBattle(fileName);
        }

        public void GetCDLUrl()
        {
			// 11-F GET http://s1.ek.ifreeteam.com/login.php?do=cdnurl&v=7807&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
			// Cookie: _sid=pm8eok49vb6jpgkfqci3ltf970; expires=Fri, 08-Aug-2014 19:57:49 GMT; path=/\r\n
			// RETURNS
			// {"status":1,"data":{"cdnurl":"http:\/\/d.muhecdn.com\/sgzj\/"}}
			//JObject o = GetRequest("login.php?do=cdnurl");
			//o["data"]["cdnurl"].ToString() + "public/wp8/";
			cdn_url = "http://cache.ifreecdn.com/sgzj/public/wp8/";
        }

        #region Image loading
        public void GetResources()
        {
            // TODO: Apparently, there is a new cdn url, http://cache.ifreecdn.com/sgzj/
            //       but this is using a different folder structure? Ah, I just needed to add public/wp8...
            //cdn_url = "http://d.muhecdn.com/sgzj/public/wp8/";

            // 1. Download (cdnurl) -> http://d.muhecdn.com/sgzj/public/wp8/registry_android.patch

            DownloadResourceList();


            // Using this, you can download the actual resource file by 
            
            //http://d.muhecdn.com/sgzj/public/wp8/android/package_8005_4.patch
            //DownloadResource(String.Format("android/{0}_{1}.patch", "package_8005", 4));
            //DownloadResource(String.Format("android/{0}_{1}.patch", "package_71", 4));

            //DownloadResource(String.Format("android/{0}_{1}.patch", "package_294", 5));
            //DownloadResource(String.Format("android/{0}_{1}.patch", "package_299", 3));
            
        }

        static Dictionary<string, PackageInfo> packages = new Dictionary<string, PackageInfo>();

        private void DownloadResourceList()
        {
            /*
                This is a unity resource file. After extraction using disunity, it contains a json file (registry.txt)
             *  { packages: [], assets: [] }
                This contains a list of packages, which have a number behind them. For example:
                [ "package_8005", 4 ]
                [ "package_AchievementTex_1301", 3 ]
                
                and matching assets entry
            
                [
                    "Assets/Resources/Imgs/Cards/Heads/img_photoCard_8005",
                    "UnityEngine.Texture2D",
                    "package_8005"
                ], 
                [
                    "Assets/Resources/Imgs/Cards/Img/img_maxCard_8005",
                    "UnityEngine.Texture2D",
                    "package_8005"
                ],
                [
                    "Assets/Resources/Imgs/AchievementTex/1301",
                    "UnityEngine.Texture2D",
                    "package_AchievementTex_1301"
                ]
             * */
            string packageFileName = "cache/resources/packages.json";
            FileInfo fi = null;
            if (File.Exists(packageFileName))
            {
                fi = new FileInfo(packageFileName);
            }
            else
            {
                DownloadResource("registry_android.patch");
                DirectoryInfo di = new DirectoryInfo("cache/resources/");

                var files = di.GetFiles("registry.txt", SearchOption.AllDirectories);
                if (files.Length != 1)
                {
                    log.WarnFormat("Multiple matches for registry.txt in {0}, using first", di.FullName);
                }
                fi = files[0];
                fi.CopyTo(packageFileName);
            }

            if (log.IsDebugEnabled) log.Debug(fi.FullName);
            if (log.IsDebugEnabled) log.Debug(fi.Name);
            string content = File.ReadAllText(fi.FullName);
            JObject o = JObject.Parse(content);
            var q = o["packages"].ToObject<List<PackageInfo>>();

            foreach (PackageInfo p in q)
            {
                //if (!Regex.IsMatch(p.PackageName, "^package_[0-9]+$"))
                //{
                //    continue;
                //}

                //int id = Int32.Parse(p.PackageName.Split('_').Last());

                packages.Add(p.PackageName, p);

                //if (log.IsDebugEnabled) log.DebugFormat("Added package {0} ({1})", id, p);
            }
        }

        public Image DownloadCardImage(int cardId, bool head)
        {
            // Not all [Elite] cards seem to have their own resource file. Just get the original card image, which always seems to be (number - 7000)
            if (cardId >= 7000 && cardId < 8000)
            {
                if (log.IsDebugEnabled) log.DebugFormat("Card with id in elite range, obtaining base card instead...");
                cardId = cardId - 7000;
            }

            string fileName;
            if (head)
            {
                fileName = String.Format("img_photoCard_{0}.PNG", cardId);
            }
            else
            {
                fileName = String.Format("img_maxCard_{0}.PNG", cardId);
            }

            string packageName = String.Format("package_{0}", cardId);

            Image img = GetImageFromFileOrPackage(fileName, packageName);
#if !DEBUG
            if (img == null)
            {
                try
                {
                    img = ImageLoader.GetImageFromServer(cardId, head);
                }
                catch (Exception)
                {
                    // just ignore exceptions
                }
            }
#endif
            return img;
        }

        public Image DownloadRuneImage(int runeId)
        {
            string fileName = String.Format("rune_{0}.png", runeId);
            string packageName = String.Format("package_shadeRunes_rune_{0}", runeId);

            return GetImageFromFileOrPackage(fileName, packageName);
        }

        private Image GetImageFromFileOrPackage(string fileName, string packageName)
        {
            if (log.IsDebugEnabled) log.DebugFormat("Obtaining image {0} (package {1})", fileName, packageName);
            bool first = true;
            do
            {
                // asume the images are now available in cache/resources
                var files = Directory.GetFiles("cache/resources", fileName, SearchOption.AllDirectories);
                if (files.Length > 1)
                {
                    if (log.IsDebugEnabled) log.DebugFormat("Found multiple files matching pattern {0}??? {1}", fileName, String.Join("\r\n", files));
                    return null;
                }
                else if (files.Length == 1)
                {
                    return Image.FromFile(files[0]);
                }
                else if (files.Length == 0)
                {
                    log.ErrorFormat("Could not find image {0}", fileName);
#if !DEBUG
                    return null;
#endif
                    if (!first)
                    {
                        log.ErrorFormat("Could not download resources");
                        return null;
                    }
                    if (packages.Count == 0)
                    {
                        DownloadResourceList();
                    }
                    if (!packages.ContainsKey(packageName))
                    {
                        log.WarnFormat("Could not find package {0} - trying to download it manually...", packageName);
						for (int i = 1; i <= 4; i++)
                        {
                            try
                            {
                                PackageInfo p = new PackageInfo() { packageName, i.ToString() };
                                DownloadResource(p.GetPackageUrl());
                                packages.Add(packageName, p);
                                break;
                            }
                            catch (Exception)
                            {
                                if (log.IsDebugEnabled) log.DebugFormat("Not found with package number {0}...", i);
                            }
                        }
                    }
                    else
                    {
                        PackageInfo pi = packages[packageName];
                        DownloadResource(pi.GetPackageUrl());
                    }
                    first = false;
                }
            } while (true);
        }

        class PackageInfo : List<string>
        {
            public string PackageName { get { return this[0]; } }
            public string Num { get { return this[1]; } }

            public string GetPackageUrl()
            {
                return String.Format("android/{0}_{1}.patch", PackageName, Num);
            }
        }

        private void DownloadResource(string resource_file)
        {
            if (cdn_url.Length == 0)
            {
                GetCDLUrl();
            }
            string remoteurl = String.Format("{0}{1}", cdn_url, resource_file);
            string localurl = String.Format("{0}{1}", "cache/resources/", resource_file);

            try
            {
                wc.DownloadFile(remoteurl, localurl);

                ExtractResource(localurl);
            }
            catch (Exception ex)
            {
                log.Error("Error downloading file", ex);
            }
        }

        private void ExtractResource(string file)
        {
            // Using disunity (https://github.com/ata4/disunity) to extract the unity asset bundle file to its original resources.
            // Since this is a java application,I have added a launch4j wrapper (disunity.exe) which should take care of JRE requirements.
            // TODO: Since it is an open source application, it should be trivial to convert the necessary java code to C#.
            if (!File.Exists("disunity.bat"))
            {
                // I have added all the required files into a single zip which I have added as a resource. 
                // Otherwise, I would probably be crying a lot to put the exe + jar + lib/*.jar into the project, and put it in the output directory properly.
                // Since the executable is not present, extract the zip from the resources.

                /*Disabled for now - needs .NET 4.5 and was only in debug build anyway 
                File.WriteAllBytes("disunity.zip", Properties.Resources.disunity_v0_3_4);
                ZipFile.ExtractToDirectory("disunity.zip", "."); // TODO: This throws an exception if one of the files already exists.
                File.Delete("disunity.zip");
                */
                return;
            }
            var startInfo = new System.Diagnostics.ProcessStartInfo("disunity.bat", String.Format("extract {0}", file))
            {
                CreateNoWindow = true,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
            };

            var proc = System.Diagnostics.Process.Start(startInfo);
            proc.WaitForExit();
            if (log.IsDebugEnabled) log.DebugFormat("disunity exited with code {0}", proc.ExitCode);

            string resourceDir = file;
            if (resourceDir.EndsWith(".patch"))
            {
                resourceDir = resourceDir.Remove(resourceDir.Length - 6);
            }

            if (log.IsDebugEnabled) log.DebugFormat("Resources extracted to {0}", resourceDir);

            DirectoryInfo di = new DirectoryInfo(resourceDir);

            foreach (var f in di.GetFiles("*.dds", SearchOption.AllDirectories))
            {
                if (log.IsDebugEnabled) log.Debug(f.FullName);
                if (log.IsDebugEnabled) log.Debug(f.Name);
                ConvertDDSFile(f);
            }

            foreach (var f in di.GetFiles("*.tga", SearchOption.AllDirectories))
            {
                if (log.IsDebugEnabled) log.Debug(f.FullName);
                if (log.IsDebugEnabled) log.Debug(f.Name);
                ConvertTGAFile(f);
            }
        }

        private void ConvertDDSFile(FileInfo f)
        {
            // Using texconv.exe (http://directxtex.codeplex.com/releases/view/150826) to convert the .dds file to .png, which is a lot prettier for us.
            // In the future, I might want to write a conversion tool myself to get rid of this exe. 
            // The dds file format description is available on http://msdn.microsoft.com/en-us/library/bb943991.aspx
            if (!File.Exists("texconv.exe"))
            {
                File.WriteAllBytes("texconv.exe", Properties.Resources.texconv);
            }

            var startInfo = new System.Diagnostics.ProcessStartInfo("texconv.exe", String.Format("-nologo -ft png -vflip -o {1} \"{0}\"", f.FullName, "cache/resources"))
            {
                CreateNoWindow = true,
                WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
            };

            var proc = System.Diagnostics.Process.Start(startInfo);
            proc.WaitForExit();

            if (log.IsDebugEnabled) log.DebugFormat("texconv exited with code {0}", proc.ExitCode);
        }

        private void ConvertTGAFile(FileInfo f)
        {
            Paloma.TargaImage ti = new Paloma.TargaImage(f.FullName);
            //var img = Paloma.TargaImage.LoadTargaImage(f.FullName);
            
            var b = new Bitmap(ti.Image.Width, ti.Image.Height, ti.Image.PixelFormat);
            Graphics g = Graphics.FromImage(b);
            g.DrawImage(ti.Image, 0, 0, new Rectangle(0, 0, b.Width, b.Height), GraphicsUnit.Pixel);
            g.Dispose();

            string newName = "cache/resources/" + f.Name.ToLower().Replace(".tga", ".png");
            b.Save(newName);
        }
#endregion Image loading

        public JObject UserMapBattle(int mapStageDetailId, bool isManual)
        {
            // 20 POST http://s1.ek.ifreeteam.com/mapstage.php?do=EditUserMapStages&v=7835&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // MapStageDetailId=1&isManual=1
            // RETURNS EditUserMapStages.json
            NameValueCollection data = new NameValueCollection();
            data["MapStageDetailId"] = mapStageDetailId.ToString();
            data["isManual"] = Convert.ToInt32(isManual).ToString();

            JObject o = PostRequest("mapstage.php?do=EditUserMapStages", data);

            string filename = String.Format("cache/{0}_{1}_userMapBattle.json", Util.GetFilenameFriendlyCurrentTime(), mapStageDetailId);
            System.IO.File.WriteAllText(filename, o.ToString());

            return o;
        }

        public JObject MapStageBattle(int mapStageDetailId, bool isManual, string battleId, params string[] selectedCards)
        {
            // After selecting card(s) and clicking "Attack":
            // 22 POST http://s1.ek.ifreeteam.com/mapstage.php?do=Battle&v=7838&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // MapStageDetailId=1&isManual=1&battleid=f3ab92f4322bb5851b9d46dd1f6667bc53d8190da453f&stage=atk_1%2c
            // RETURNS
            // Battle.json

            NameValueCollection data = new NameValueCollection();
            data["MapStageDetailId"] = mapStageDetailId.ToString();
            data["isManual"] = Convert.ToInt32(isManual).ToString();
            data["battleid"] = battleId;
            StringBuilder stage = new StringBuilder();
            foreach (string s in selectedCards)
            {
                stage.Append(s);
                stage.Append("%2c"); // html escaped ','
            }

            JObject o = PostRequest("mapstage.php?do=Battle", data);

            string filename = String.Format("cache/{0}_{1}_userMapBattle.json", Util.GetFilenameFriendlyCurrentTime(), mapStageDetailId);
            System.IO.File.WriteAllText(filename, o.ToString());

            return o;
        }

        public JObject SelectTower(int mapStageId)
        {
            //POST /maze.php?do=Show&v=4133&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.0&phpk=0608cdd7770e6655b37feb65af324dea&phps=876668596&pvb=2014-11-06+10%3a25 HTTP/1.1
            //MapStageId=8

            NameValueCollection data = new NameValueCollection();
            data["MapStageId"] = mapStageId.ToString();

            JObject o = PostRequest("maze.php?do=Show", data);

            return o;
        }

        public void ResetTower(int mapStageId)
        {
            //POST /maze.php?do=Reset&v=4134&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.0&phpk=bf94e50b493f5020e2e0fd640ed42c27&phps=876668597&pvb=2014-11-06+10%3a25 HTTP/1.1
            //MapStageId=8
            NameValueCollection data = new NameValueCollection();
            data["MapStageId"] = mapStageId.ToString();

            JObject o = PostRequest("maze.php?do=Reset", data);
        }

        public JObject EnterTowerFloor(int mapStageId, int layer)
        {
            //POST http://s1.ek.ifreeteam.com/maze.php?do=Info&v=10033&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.3&phpk=966932cdcd361767322d584102917115&phps=872567727&pvb=2015-01-26+15%3a13
            //MapStageId=7&Layer=1

            NameValueCollection data = new NameValueCollection();
            data["MapStageId"] = mapStageId.ToString();
            data["Layer"] = layer.ToString();


            JObject o = PostRequest("maze.php?do=Info", data);

            return o;
        }

        public JObject FightTowerBattle(int mapStageId, int layer, int targetIndex)
        {
            //POST http://s1.ek.ifreeteam.com/maze.php?do=Battle&v=10034&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.3&phpk=2fcddee5ca1a43f646f9de38b814be43&phps=872567728&pvb=2015-01-26+15%3a13
            //MapStageId=7&manual=1&Layer=1&ItemIndex=15

            NameValueCollection data = new NameValueCollection();
            data["MapStageId"] = mapStageId.ToString();
            data["Layer"] = layer.ToString();
            data["ItemIndex"] = targetIndex.ToString();
            data["manual"] = "0";

            JObject o = PostRequest("maze.php?do=Battle", data);

            return o;
        }

        public void SetDefaultCardGroup(int groupId)
        {

            // 29 POST http://s1.ek.ifreeteam.com/card.php?do=SetDefalutGroup&v=7852&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // GroupId=44912
            // RETURNS
            // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

            NameValueCollection data = new NameValueCollection();
            data["GroupId"] = groupId.ToString();

            JObject o = PostRequest("card.php?do=SetDefalutGroup", data);
        }

        private void checkIds(List<int> ids)
        {
            var duplicateKeys = ids.GroupBy(x => x)
                           .Where(group => group.Count() > 1)
                           .Select(group => group.Key);
            if (duplicateKeys.Count() > 0)
            {
                throw new ArgumentException("Duplicate ids found");
            }

            var zeroKeys = ids.Where(x => x == 0);
            if (zeroKeys.Count() > 0)
            {
                throw new ArgumentException("Invalid id found");
            }
        }
        
        public void SetCardGroup(int groupId, List<int> userCardIds, List<int> userRuneIds)
        {
            checkIds(userCardIds);
            checkIds(userRuneIds);
            // 32 POST http://s1.ek.ifreeteam.com/card.php?do=SetCardGroup&v=7856&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // Cards=21577506_21568015_21568016&GroupId=44912&Runes=
            // RETURNS
            // {"status":1,"data":{"GroupId":44912},"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}
            NameValueCollection data = new NameValueCollection();
            data["GroupId"] = groupId.ToString();
            data["Cards"] = String.Join("_", userCardIds);
            data["Runes"] = String.Join("_", userRuneIds);

            JObject o = PostRequest("card.php?do=SetCardGroup", data);
        }

        public JObject GetShopGoods()
        {
            //http://s1.ek.ifreeteam.com/shopnew.php?do=GetGoods&v=2883&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.2&phpk=195b9def0a8b814b25f4d7d5c81e5063&phps=829294530&pvb=2015-01-21+14%3a39

            JObject o = GetRequest("shopnew.php?do=GetGoods");
            
            return o;
        }

        public void GetFreeCards()
        {
            
            // POST
            // http://s1.ek.ifreeteam.com/shopnew.php?do=FreeBuy&v=4053&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.0&phpk=52d7c661c3a159b53d9ac51f2ed81768&phps=876668516&pvb=2014-11-06+10%3a25
            // GoodsId = 1
            // {"status":1,"data":{"CardIds":"34_34_5_1_31","CoinsAfter":73810885,"CashAfter":4836,"TicketAfter":313},"version":{"http":"20130702","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

            JObject shopStatus = GetShopGoods();
            
            NameValueCollection data = new NameValueCollection();

            if (shopStatus["data"]["oldgood"]["3"]["RemainTime"].Value<int>() == 0)
            {
                // Free gold pack (5 per day, 15 minute cooldown)
                data["GoodsId"] = "1";
                JObject o = PostRequest("shopnew.php?do=FreeBuy", data);
            }

            if (shopStatus["data"]["oldgood"]["2"]["RemainTime"].Value<int>() == 0)
            {
                // 1 per 24 hours
                data["GoodsId"] = "7";
                JObject o = PostRequest("shopnew.php?do=FreeBuy", data);
            }

        }

        public void GetSalary()
        {
            // http://s1.ek.ifreeteam.com/user.php?do=GetUserSalary&v=2874&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.2&phpk=28cef256ab1bf963193b3847287472c3&phps=829294521&pvb=2015-01-21+14%3a39

            JObject o = GetRequest("user.php?do=GetUserSalary");
        }

        public void GetRewards()
        {
            //GET /user.php?do=AwardSalary&v=9425&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.2&phpk=20cd7adddcc0ebb7c08efb877a410074&phps=825204997&pvb=2015-01-21+14%3a39

            JObject o = GetRequest("user.php?do=AwardSalary");
        }

        /// <summary>
        /// Get the list of rewards you could claim (Events -> Newbie Gift / Level rewards / Shop rewards
        /// </summary>
        public void GetWelfare()
        {
            //GET http://s1.ek.ifreeteam.com/shop.php?do=GetWelfare&v=9420&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.2&phpk=0&phps=825204992&pvb=2015-01-21+14%3a39

            JObject o = GetRequest("shop.php?do=GetWelfare");
            System.IO.File.WriteAllText("cache/welfare.json", o.ToString());
        }

        public void GetFriendlyMatchPlayers()
        {
            //http://s1.ek.ifreeteam.com/arena.php?do=GetCompetitors&v=4998&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.4&phpk=9e716354e0c39d12cc768d7e1f5b6f0c&phps=909716183&pvb=2015-02-27+09%3a49

            JObject o = GetRequest("arena.php?do=GetCompetitors");
        }

        public void TutorialStore()
        {
            //http://s3.ek.ifreeteam.com/shopnew.php?do=GetGoodsNewbie&v=9964&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.3&phpk=b6cffe9a72c7a6ea0e97bc5f85b6cae8&phps=370721539&pvb=2015-01-26+15%3a13

            JObject o = GetRequest("shopnew.php?do=GetGoodsNewbie");

            EditFresh("11_5");

            // And buy the card pack
            //POST /shopnew.php?do=BuyNewbie&v=9969&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.3&phpk=46e44797b5d95aebd2f8c36a4d1fff5b&phps=370721544&pvb=2015-01-26+15%3a13 HTTP/1.1
            //GoodsId=4

            NameValueCollection data = new NameValueCollection();
            data["GoodsId"] = "4";
            o = PostRequest("shopnew.php?do=BuyNewbie", data);

            EditFresh("14_3");

            // You have obtained a 5* card, which should be added to the deck

            // GetUserCards
            // SetCardGroup

            EditFresh("12_5");

            // After going back to map with a dialog that your deck is so much stronger now and you should finish 1.3
            EditFresh("13_8");
        }

        public void Tutorial()
        {
            if (LostPoint < 4)
            {
                // Tutorial text. After clicking on 'Map':

                // 17 GET http://s1.ek.ifreeteam.com/mapstage.php?do=GetUserMapStages&v=7832&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // RETURNS 
                // {"status":1,"data":null,"exploreOneKey":0,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                GetUserMapStages();

                // 18 POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7833&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // LostPoint=4
                // RETURNS
                // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                EditLostPoint(4);
            }

            // After clicking on map stage
            // 19 POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7834&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
            // LostPoint=5
            // RETURNS
            // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

            EditLostPoint(5);


            if (LostPoint < 7)
            {
                // AFter clicking fight:

                // 20 POST http://s1.ek.ifreeteam.com/mapstage.php?do=EditUserMapStages&v=7835&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // MapStageDetailId=1&isManual=1
                // RETURNS EditUserMapStages.json

                JObject battleStep = UserMapBattle(1, true);
                string tutorialBattleId = battleStep["data"]["BattleId"].Value<string>();

                // 21 POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7836&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // LostPoint=6
                // RETURNS
                // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                EditLostPoint(6);

                // After selecting card(s) and clicking "Attack":
                // 22 POST http://s1.ek.ifreeteam.com/mapstage.php?do=Battle&v=7838&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // MapStageDetailId=1&isManual=1&battleid=f3ab92f4322bb5851b9d46dd1f6667bc53d8190da453f&stage=atk_1%2c
                // RETURNS
                // Battle.json

                MapStageBattle(1, true, tutorialBattleId, "atk_1");

                // AFter clicking "Auto":
                // 23-A POST http://s1.ek.ifreeteam.com/user.php?do=EditFresh&v=7843&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // FreshStep=1_21
                // RETURNS
                // {"status":1,"data":[],"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                EditFresh("1_21");

                // 23-B POST http://s1.ek.ifreeteam.com/mapstage.php?do=Battle&v=7841&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // MapStageDetailId=1&isManual=0&battleid=f3ab92f4322bb5851b9d46dd1f6667bc53d8190da453f&stage=
                // RETURNS Battle.json
                // 

                MapStageBattle(1, false, tutorialBattleId);

                // 23-C POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7842&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // LostPoint=7
                // RETURNS 
                // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                EditLostPoint(7);

                // AFter closing the You Won! dialog:

                // 24 GET http://s1.ek.ifreeteam.com/user.php?do=GetUserinfo&v=7845&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // RETURNS
                // Same as usual. 

                GetUserInfo();
            }

            if (LostPoint < 8)
            {

                // I now see the level up dialog...
                // clicking now continues the conversation. Says something about a pixie.

                // 26 POST http://s1.ek.ifreeteam.com/user.php?do=EditFresh&v=7848&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // FreshStep=2_10
                // RETURNS
                // {"status":1,"data":{"Cards":"21577506"},"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                EditFresh("2_10");

                // AFter clicking Deck

                // 27 GET http://s1.ek.ifreeteam.com/card.php?do=GetCardGroup&v=7850&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // RETURNS GetCardGroup.json

                JObject cardGroups = GetCardGroups();
                int groupId = cardGroups["data"]["Groups"][0]["GroupId"].Value<int>();


                // 28 POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7842&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // LostPoint=8
                // RETURNS 
                // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                EditLostPoint(8);

                // After clicking first deck:

                // 29 POST http://s1.ek.ifreeteam.com/card.php?do=SetDefalutGroup&v=7852&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // GroupId=44912
                // RETURNS
                // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                SetDefaultCardGroup(groupId);

                // 30 GET http://s1.ek.ifreeteam.com/card.php?do=GetUserCards&v=7853&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // RETURNS GetUserCards.json

                JObject userCards = GetUserCards();

                List<JToken> cards = userCards["data"]["Cards"].ToList();

                List<int> userCardIds = new List<int>();
                foreach (var card in cards)
                {
                    userCardIds.Add(card["UserCardId"].Value<int>());
                }

                List<int> userRuneIds = new List<int>();

                // 31 POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7842&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // LostPoint=9
                // RETURNS 
                // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                EditLostPoint(9);

                // After clicking Save

                // 32 POST http://s1.ek.ifreeteam.com/card.php?do=SetCardGroup&v=7856&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // Cards=21577506_21568015_21568016&GroupId=44912&Runes=
                // RETURNS
                // {"status":1,"data":{"GroupId":44912},"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}

                SetCardGroup(groupId, userCardIds, userRuneIds);

                // 33-A -> 27
                // 33-B -> 29
                // 34-A -> 31
                // LostPoint = 10
                EditLostPoint(10);
                // 34-B -> 27

                // After clicking level 1-2

                // 35 POST http://s1.ek.ifreeteam.com/user.php?do=EditLostPoint&v=7842&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // LostPoint=11
                // RETURNS 
                // {"status":1,"version":{"http":"20130546","stop":"","appversion":"version_1","appurl":"ios:\/\/xxx"}}
                EditLostPoint(12);

                //POST /user.php?do=EditFresh&v=9959&phpp=ANDROID_ARC&phpl=EN&pvc=1.6.3&phpk=0031d92ff22d4a30c8327c9bdaf16a56&phps=370721534&pvb=2015-01-26+15%3a13 HTTP/1.1
                //FreshStep=2_17
                EditFresh("2_17");

                // After clicking "Fight"

                // 36 POST http://s1.ek.ifreeteam.com/mapstage.php?do=EditUserMapStages&v=7863&phpp=ANDROID_ARC&phpl=EN&pvc=1.4.7&pvb=2014-07-02+15%3a38
                // MapStageDetailId=2&isManual=1
                // RETURNS EditUserMapStage.json

                JObject battlestep2 = UserMapBattle(2, true);
                string tutorial2BattleId = battlestep2["data"]["BattleId"].Value<string>();


                JObject mapStageBattleResult = MapStageBattle(2, false, tutorial2BattleId);

                GetUserInfo();

                EditFresh("15_2");

                // Go to special shop
                TutorialStore();
            }
        }

        public JObject GetForceMap()
        {
            JObject o = GetRequest("forcefight.php?do=ShowForceMap");

            return o;
        }
    }
}
