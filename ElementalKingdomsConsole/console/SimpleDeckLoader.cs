﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElementalKingdoms.core;

namespace ElementalKingdomsConsole.console
{
    class SimpleDeckLoader
    {
        enum ParseState
        {
            Player,
            Cards,
            Runes,
            Done
        }

        private ParseState currentState;
        private string[] lines;
        private User u;

        public SimpleDeckLoader(string deckFileName)
        {
            if (File.Exists(deckFileName))
            {
                lines = File.ReadAllLines(deckFileName);
            }
            else
            {
                lines = new string[0];
            }
            u = new User();
            u.Level = 90;
            u.NickName = "Player";
        }

        public User loadDeck()
        {
            foreach (var line in lines)
            {
                parseLine(line);
            }
            return u;
        }

        private void parseLine(string line)
        {
            if (string.IsNullOrWhiteSpace(line))
            {
                currentState++;
                return;
            }

            if (currentState == ParseState.Done || line.StartsWith("#") || line.StartsWith("//"))
            {
                return;
            }

            switch (currentState)
            {
                case ParseState.Player:
                    parsePlayerLine(line);
                    break;
                case ParseState.Cards:
                    parseCardLine(line);
                    break;
                case ParseState.Runes:
                    parseRuneLine(line);
                    break;
                default:
                    break;
            }
        }

        private void parsePlayerLine(string line)
        {
            var elements = line.Split(';');
            if (elements.Count() >= 1)
            {
                // first element is the level
                u.Level = int.Parse(elements[0]);
            }
            if (elements.Count() >= 2)
            {
                u.NickName = elements[1];
            }
        }

        private void parseCardLine(string line)
        {
            var elements = line.Split(';');
            UserCard c = new UserCard();
            c.Level = 10;
            
            if (elements.Count() >= 1)
            {
                // first element is the card name
                c.CardId = Card.GetIdFromName(elements[0]);
            }
            if (elements.Count() >= 2)
            {
                // level
                c.Level = int.Parse(elements[1]);
            }
            if (elements.Count() >= 3)
            {
                // Evolve skill
                Skill s = Skill.Skills.Find(x => x.Name.Equals(elements[2], StringComparison.InvariantCultureIgnoreCase));
                if (s != null)
                {
                    c.SkillNew = s.SkillId;
                }
            }
            
            u.UserDeck.Cards.Add(c);
        }

        private void parseRuneLine(string line)
        {
            var elements = line.Split(';');
            UserRune r = new UserRune();
            r.Level = 4;
            
            if (elements.Count() >= 1)
            {
                // first element is the card name
                r.RuneId = Rune.GetIdFromName(elements[0]);
            }
            if (elements.Count() >= 2)
            {
                // level
                r.Level = int.Parse(elements[1]);
            }

            u.UserDeck.Runes.Add(r);
        }
    }
}
