﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using ElementalKingdoms.core;
using ElementalKingdoms.server;
using ElementalKingdoms.tools;
using ElementalKingdomsConsole.console;
using ElementalKingdomsConsole.Properties;

namespace ElementalKingdomsConsole
{
    class ConsoleUI
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /*
    DEMO CODE ONLY: In general, this approach calls for re-thinking 
    your architecture!
    There are 4 possible ways this can run:
    1) User starts application from existing cmd window, and runs in GUI mode
    2) User double clicks to start application, and runs in GUI mode
    3) User starts applicaiton from existing cmd window, and runs in command mode
    4) User double clicks to start application, and runs in command mode.

    To run in console mode, start a cmd shell and enter:
        c:\path\to\Debug\dir\WindowsApplication.exe console
        To run in gui mode,  EITHER just double click the exe, OR start it from the cmd prompt with:
        c:\path\to\Debug\dir\WindowsApplication.exe (or pass the "gui" argument).
        To start in command mode from a double click, change the default below to "console".
    In practice, I'm not even sure how the console vs gui mode distinction would be made from a
    double click...
        string mode = args.Length > 0 ? args[0] : "console"; //default to console
    */

        //[DllImport("kernel32.dll", SetLastError = true)]
        //static extern bool AllocConsole();

        //[DllImport("kernel32.dll", SetLastError = true)]
        //static extern bool FreeConsole();

        //[DllImport("kernel32", SetLastError = true)]
        //static extern bool AttachConsole(int dwProcessId);

        //[DllImport("user32.dll")]
        //static extern IntPtr GetForegroundWindow();

        //[DllImport("user32.dll", SetLastError = true)]
        //static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        public About About = new About();

        private static readonly Dictionary<string, string> demonAbbreviations = new Dictionary<string, string>
        {
            { "de", "Deucalion" },
            { "sk", "Sea King" },
            { "dt", "Dark Titan" },
            { "ma", "Mars" },
            { "pa", "Pandarus" },
            { "ba", "Bahamut" },
            { "az", "Azathoth" },
            { "pz", "Pazuzu" },
            { "po", "Plague Ogryn" }
        };

        public ConsoleUI()
        {

        }

        public void RunLeague(Options options)
        {
            bool optionsOk = true;
            if (String.IsNullOrEmpty(options.Server))
            {
                Console.WriteLine("Please provide a server name");
                optionsOk = false;
            }
            if (optionsOk)
            {
                // Do the stuff. Should put that in a separate class though.
                log.InfoFormat("Initializing server connection...");
                ServerCommunicator com = ServerCommunicator.Create(Settings.Default.DeviceId, true);
                ServerInfo s = com.Servers.Find(x => x.Name.Equals(options.Server, StringComparison.OrdinalIgnoreCase));
                if (s == null)
                {
                    Console.WriteLine("Unknown server name {0}", options.Server);
                    return;
                }
                log.InfoFormat("Connecting to {0}", s.Name);
                com.ConnectToServer(s);
                log.InfoFormat("Downloading card data...");
                com.GetAllCard();
                log.InfoFormat("Downloading skill data...");
                com.GetAllSkill();
                log.InfoFormat("Downloading rune data...");
                com.GetAllRunes();

                log.InfoFormat("Downloading FoH data...");
                com.GetLeagueInfo();

                int numberOfBattles = options.Iterations;

                int currentLeague = Math.Min(League.CurrentLeague.RoundResult.Count, 3);


                Console.WriteLine("Running FoH ({1} season {2} round {3} - {0} iterations)\r\n", numberOfBattles, s.Name, League.CurrentLeague.LeagueId, League.CurrentLeague.RoundNow);

                List<League.LeagueRoundResult> currentBattles = League.CurrentLeague.RoundResult[currentLeague - 1];
                for (int i = 0; i < currentBattles.Count; i++)
                {
                    League.LeagueRoundResult result = currentBattles[i];
                    //if (log.IsDebugEnabled) log.DebugFormat("Found player {0} with odds {1} ({2} honor bet)", result.BattleInfo.User.NickName, result.BetOdds, result.BetTotal);


                    User attacker = result.BattleInfo.User;
                    attacker.UserDeck.Cards = result.BattleInfo.Cards;
                    attacker.UserDeck.Runes = result.BattleInfo.Runes;

                    int opponentIndex = -1;
                    if (i % 2 == 0)
                    {
                        // opponent is +1
                        opponentIndex = i + 1;
                    }
                    else
                    {
                        opponentIndex = i - 1;
                    }

                    League.LeagueRoundResult opponentResult = currentBattles[opponentIndex];
                    User defender = opponentResult.BattleInfo.User;
                    defender.UserDeck.Cards = opponentResult.BattleInfo.Cards;
                    defender.UserDeck.Runes = opponentResult.BattleInfo.Runes;

                    if (attacker.GetFohMagicNumber() < defender.GetFohMagicNumber())
                    {
                        log.InfoFormat("Skipping {0} vs {1} - {1} will be the attacker", attacker.NickName, defender.NickName);
                        continue;
                    }

                    //Console.Write("{0} vs {1}: ", attacker.NickName, defender.NickName);

                    MultiBattleExecutor mb = new MultiBattleExecutor(attacker, defender);
                    var simResult = mb.Execute(numberOfBattles);

                    int attackerWinChance = simResult.RoundsWon * 100 / simResult.Iterations;

                    if (attackerWinChance > 50)
                    {
                        Console.WriteLine("{0}% {1,-12} vs {2,-12} {3}%", attackerWinChance, attacker.NickName, defender.NickName, 100 - attackerWinChance);
                    }
                    else
                    {
                        Console.WriteLine("{3}% {2,-12} vs {1,-12} {0}%", attackerWinChance, attacker.NickName, defender.NickName, 100 - attackerWinChance);
                    }
                }
                Console.WriteLine("Finished!");
            }
            else
            {
                Console.WriteLine(options.Server);
                Console.WriteLine(options.Iterations);
                Console.WriteLine(options.SkipData);
                Console.WriteLine(options.GetUsage());
            }
        }

        public void RunDemon(Options options)
        {
            Game.TheGame.Load();

            Card demon;
            if (String.IsNullOrEmpty(options.Deck) || !System.IO.File.Exists(options.Deck))
            {
                Console.WriteLine("Please provide a valid dack file name");
            }
            else if (string.IsNullOrEmpty(options.Demon))
            {
                Console.WriteLine("Please provide a demon name");
            }
            else
            {
                string name = options.Demon;
                if (demonAbbreviations.ContainsKey(name))
                {
                    name = demonAbbreviations[name];
                }
                demon = Card.Cards.Find(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase) && x.Race == Race.Demon);
                if (demon == null)
                {
                    Console.WriteLine("{0} is not a demon name I know.", options.Demon);
                    Console.WriteLine("Valid demons: {0}", String.Join(",", Card.Cards.FindAll(x => x.Race == Race.Demon).Select(x => x.Name)));
                    Console.WriteLine("Or use one of these abbreviations: {0}", String.Join(",", demonAbbreviations.Keys));
                }
                else
                {
                    Console.WriteLine("Running DI sim vs {0}", demon.Name);
                    User defender = new User();
                    defender.Level = 0;
                    defender.UserDeck.Cards.Add(new UserCard() { CardId = demon.Id, Level = 15 });

                    if (log.IsDebugEnabled) log.DebugFormat("Selected demon {0}", demon.Name);

                    User attacker = new SimpleDeckLoader(options.Deck).loadDeck();
                    PrintPlayer(attacker);

                    MultiBattleExecutor mb = new MultiBattleExecutor(attacker, defender);
                    var simResult = mb.Execute(options.Iterations);
                    Console.WriteLine("Finished: {0}", simResult.ToString());
                }
            }
        }

        private void PrintPlayer(User player)
        {
            Console.WriteLine("{0} level {1}", player.NickName, player.Level);
            Console.WriteLine("-------- Cards -------");
            foreach (var c in player.UserDeck.Cards)
            {
                Console.WriteLine("{0} | level {1}{2}", c.Name, c.Level, c.SkillNew != 0 ? " | " + Skill.Get(c.SkillNew).Name : "");
            }
            Console.WriteLine("-------- Runes -------");
            foreach (var r in player.UserDeck.Runes)
            {
                Console.WriteLine("{0} | level {1}", r.Name, r.Level);
            }
            Console.WriteLine("----------------------");
        }
    }

    class Options
    {
        [Option("mode", Required = true)]
        public string Mode { get; set; }

        [Option('s', "server", DefaultValue = "", HelpText = "Server name")]
        public string Server { get; set; }

        [Option('n', "iterations", DefaultValue = 10000, HelpText = "Number of iterations")]
        public int Iterations { get; set; }

        [Option('v', DefaultValue = false, HelpText = "Print details during execution.")]
        public bool Verbose { get; set; }

        [Option('d', "deck", HelpText = "Filename of your deck")]
        public string Deck { get; set; }

        [Option("demon", HelpText = "Name of the demon")]
        public string Demon { get; set; }

        [Option("skip-data", DefaultValue = false)]
        public bool SkipData { get; set; }

        [HelpOption(HelpText = "Display this help screen.")]
        public string GetUsage()
        {
            About about = new About();

            var usage = new StringBuilder();
            usage.AppendLine("Usage: ");
            usage.AppendLine("--mode [foh | di]\tSelects the mode to operate in");
            usage.AppendLine().AppendLine("GLOBAL OPTIONS");
            usage.AppendLine("-n number-of-fights\tHow many battles (default: 10000)");

            usage.AppendLine().AppendLine("FIELD OF HONOR OPTIONS");
            usage.AppendLine("-s server-name\t\tServer you want to connect to");

            usage.AppendLine().AppendLine("DI OPTIONS");
            usage.AppendLine("-d deck-file\t\tFile containing a deck list. See sample_deck.txt");
            usage.AppendLine("--demon demon\t\tDemon name or abbreviation");
            //usage.AppendLine("--skip-data \t Don't download data files from the server when running");
            return usage.ToString();
        }
    }
}
